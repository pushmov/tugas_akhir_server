<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['num_links'] = 2;
$config['use_page_numbers'] = TRUE;

$config['full_tag_open'] = "<ul>";
$config['full_tag_close'] = '</ul>';

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';

$config['cur_tag_open'] = "<li class='active'><a href='#'>";
$config['cur_tag_close'] = '</a></li>';

$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';

$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';

$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

/* End of file pagination.php */
/* Location: ./application/config/pagination.php */
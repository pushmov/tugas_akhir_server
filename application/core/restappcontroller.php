<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RESTAppController extends REST_Controller {

	function __construct(){
		parent::__construct();	
		$this->write_log();
	}
	
	protected function write_log(){
	
			$current = '';
			$path = APPPATH . 'errors/';
			$file = 'log.txt';
			$current .= file_get_contents($path . $file) . "\n";
			$current .= "-----------\n";
			$current .= "Log trace request \n";
			$current .= date('Y-m-d H:i:s') . "\n";
			$current .= serialize($_REQUEST);
			$current .= "-----------\n\n\n";
				
			file_put_contents($path . $file, $current);
			
	}

}
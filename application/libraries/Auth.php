<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Auth
*/
class Auth
{
	
	function __construct()
	{
		$this->ci =& get_instance();
		
	}

	function is_logged_in()
	{	
		if ($this->ci->session->userdata('global_session')) {
			return true;
		} else {
			return false;
		}
	}

	function get_user_id()
	{
		$user_id = NULL;

		if ($this->is_logged_in()) {
			$sess = $this->ci->session->userdata('global_session');
			$user_id = $sess['userid'];
		}

		return $user_id;
	}

	function is_driver()
	{

		$sess = $this->ci->session->userdata('global_session');
		$type = $sess['type'];

		if ($type == 'driver') {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function user_bid($shipment_id)
	{
		$this->ci->load->model('Appmodel');

		$this->ci->Appmodel->set_table('notification');
		$where = array('shipment_id' => $shipment_id, 'status' => 'A', 'member_id' => $this->get_user_id());
		$bid = $this->ci->Appmodel->fetch_row(NULL, $where);
		
		return $bid;
	}

}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */
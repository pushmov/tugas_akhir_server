<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Shipment_lib
*/
class Shipment_lib
{
	
	function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('Appmodel');
	}

	function get_bid_count($shipment_id)
	{
		$where = array('shipment_id' => $shipment_id, 'status' => 'A');
		$table = 'notification';

		return $this->ci->Appmodel->count($table, $where);
	}

	function get_bids($shipment_id)
	{
		$this->ci->Appmodel->set_table('notification');

		$params = array('select' => '*, member.username as bidder',
						'tablejoin' => 'member',
						'where1' => array('shipment_id' => $shipment_id),
						'where2' => "member.member_id = notification.member_id AND notification.status = 'A'");

		return $this->ci->Appmodel->multi_table_rows($params);
	}
	
	function get_applied_bids($shipmentid){
		return $this->ci->db->where(array('shipment_id' => $shipmentid, 'status' => 'A'))
			->from('notification')
			->count_all_results();
	}

	function is_closed($shipment_id)
	{	
		$this->ci->Appmodel->set_table('notification');
		$where = array('shipment_id' => $shipment_id);

		$bids = $this->ci->Appmodel->fetch_rows(NULL, $where);

		foreach ($bids as $row) {
			if ($row->status == 'R' || $row->status == 'ACC') {
				return TRUE;
			}
		}

		return FALSE;
	}

	function get_type_name($type)
	{
		$this->ci->Appmodel->set_table('shipment_type');
		$where = array('type' => $type);
		
		return $this->ci->Appmodel->fetch_row(NULL, $where);
	}
	
	function get_winner($shipmentid){
		return $this->ci->db->where(array('status' => 'ACC', 'shipment_id' => $shipmentid))
			->join('member', 'notification.member_id = member.member_id')
			->get('notification')
			->row();
	}
	
	function get_sub_type_name($sub_id){
		return $this->ci->db->where('sub_id', $sub_id)
			->get('shipment_sub_type')
			->row();
	}
}

/* End of file Shipment_lib.php */
/* Location: ./application/libraries/Shipment_lib.php */
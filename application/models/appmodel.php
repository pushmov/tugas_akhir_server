<?php

Class Appmodel extends CI_Model
{

	var $table = NULL;

	function set_table($table){
	
			$this->table = $table;
	}

	function update($data,$where=NULL){
	
			if(isset($where)){
			
					$this->db->where($where);
					
			}
			return $this->db->update($this->table,$data);
	}
	
	function insert($data){
	
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
	}
	
	function delete($where){
	
			$this->db->where($where)
					->delete($this->table);
					
	}

	function fetch_rows($select=NULL,$where=NULL, $limit=NULL, $offset=NULL, $order=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where);
			}

			if (isset($order)) {
					$this->db->order_by($order);
			}

			return $this->db->get($this->table, $limit, $offset)
					->result();
	}

	function fetch_row($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where);
			}

			return $this->db->get($this->table)
					->row();
	}

	function custom_rows($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where,NULL,FALSE);
			}

			return $this->db->get($this->table)
					->result();
	}

	function custom_row($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where);
			}

			return $this->db->get($this->table)
					->row();
	}
	
	function multi_table_rows($params, $limit = NULL, $offset = NULL){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}

	function count($table, $where)
	{
		if(isset($where)){
			$this->db->where($where);
		}
		return $this->db->count_all_results($table);
	}

}
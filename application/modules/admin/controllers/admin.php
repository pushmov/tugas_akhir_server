<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {
	
	var $base_view;
	
	function __construct(){
		parent::__construct();
		$this->load->model('admin_model');
		$this->base_view = $this->config->item('base_admin');
		
	}
	
	function index(){
		if(!$this->session->userdata('admin')){
			redirect('/admin/login');
		}
		
		redirect('admin/request');
	}
	
	function login(){
		
		
		$data = array( 'page_content' => 'login', 'active' => 'login');
		$this->load->view($this->base_view, $data);
		
	}
	
	function verify(){
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if(!$username || !$password){
			redirect('/admin/login');
		}
		
		$verify = $this->admin_model->verify($username, $password);
		if(empty($verify)){
			redirect('/admin/login');
		}
		
		$this->session->set_userdata('admin', $verify);
		redirect('/admin/request');
		
	}
	
	function request(){
		
		if(!$this->session->userdata('admin')){
			redirect('/admin/login');
		}
		
		$shipments = $this->admin_model->get_list_request();
		$data = array( 'page_content' => 'list', 'active' => 'request', 'shipments' => $shipments );

		$this->load->view($this->base_view, $data);
		
	}
	
	function report(){
		
		if(!$this->session->userdata('admin')){
			redirect('/admin/login');
		}
		
		
		$reports = $this->admin_model->get_list_reports();
		$data = array( 'page_content' => 'reports', 'active' => 'report', 'reports' => $reports );

		$this->load->view($this->base_view, $data);
		
	}
	
	
	function logout(){
		$this->session->set_userdata('admin', null);
		redirect('admin/login');
	}
	
	function acc_shipment($paymentid){
		$this->db->where('id', $paymentid)
			->set('is_accepted', 1)
			->set('invoice_status', '')
			->update('shipper_invoice');
		redirect('admin/request');
	}
	
	function reject_shipment(){
		
		$paymentid = $this->input->post('id');
		
		$this->db->where('id', $paymentid)
			->set('is_accepted', 0)
			->set('invoice_status', 'R')
			->set('invoice_date_paid', NULL)
			->set('status_message', $this->input->post('pesan'))
			->update('shipper_invoice');
		
		redirect('admin/request');
	}
	
	function forward($id){
		
		$this->db->where('id', $id)
			->set('is_completed', 1)
			->update('shipper_invoice');
		
		redirect('admin/report');
	}
	
	function subtype(){
		
		$kategori = $this->admin_model->get_parent_category();
		
		foreach($kategori as $k){
			$k->sub = $this->admin_model->count_child_category($k->type);
		}
		
		$data = array( 'page_content' => 'subtype', 'active' => 'sub', 'kategori' => $kategori );

		$this->load->view($this->base_view, $data);
		
	}
	
	function add_subtype($type=NULL){
		
		if(!isset($type)){
			redirect('/admin/subtype');
		}
		
		$kategori_detail = $this->admin_model->kategori_detail($type);
		
		if(empty($kategori_detail)){
			redirect('/admin/subtype');
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$nama = $this->input->post('name');
			
			$request = array(
				'parent_id' => $type,
				'sub_name' => $nama
			);
			
			$this->db->insert('shipment_sub_type', $request);
			redirect('/admin/add_subtype/' . $type);
			
		}
		
		$all = $this->admin_model->get_parent_category();
		
		$subs = $this->admin_model->get_subs($type);
		
		$data = array( 'page_content' => 'addsub', 'active' => 'sub', 'kategori' => $kategori_detail, 'all_kategori' => $all, 'subs' => $subs );

		$this->load->view($this->base_view, $data);
		
	}
	
	function delete_sub($id=NULL){
		
		if(!isset($id)){
			redirect('/admin/subtype');
		}
		
		$kategori_detail = $this->admin_model->kategori_sub_detail($id);
		
		
		if(empty($kategori_detail)){
			redirect('/admin/subtype');
		}
		
		$this->db->where(array('sub_id' => $kategori_detail->sub_id))
			->delete('shipment_sub_type');
		
		redirect('admin/add_subtype/' . $kategori_detail->sub_id);
		
	}
	
	function add_parent(){
		
		$nama = $this->input->post('nama');
		
		$check = $this->admin_model->check_exist($nama);
		
		if(!empty($check)){
			
			$this->session->set_flashdata('info', "Nama kategori sudah digunakan");
			redirect('admin/subtype');
			
		}
		
		$request = array(
			'shipment_type_name' => $nama
		);
		
		$this->db->insert('shipment_type', $request);
		redirect('admin/subtype');
		
	}
}
<?php

class Admin_model extends AppModel {
	
	
	function get_list_request(){
		return $this->db->where('invoice_date_paid IS NOT NULL', NULL, FALSE)
			->join('shipment', "shipment.shipment_id = shipper_invoice.shipment_id")
			->join('member', "member.member_id = shipper_invoice.driver_id")
			->join('notification', "notification.shipment_id = shipment.shipment_id AND notification.status = 'ACC'")
			->get('shipper_invoice')
			->result();
	}
	
	function get_list_reports(){
		return $this->db->where('shipper_invoice.is_accepted', 1)
			->join('shipment', "shipment.shipment_id = shipper_invoice.shipment_id")
			->join('notification', "shipper_invoice.shipment_id = notification.shipment_id AND shipper_invoice.driver_id = notification.member_id")
			->join('member', "member.member_id = shipper_invoice.driver_id")
			->order_by('driver_report_date DESC')
			->get('shipper_invoice')
			->result();
	}
	
	function verify($username, $password){
		return $this->db->where(array('username' => $username, 'password' => $password, 'is_admin' => 1))
			->get('member')
			->row();
	}
	
	function get_parent_category(){
		return $this->db->order_by('type', 'ASC')
			->get('shipment_type')
			->result();
	}
	
	function count_child_category($id){
		return $this->db->where('parent_id', $id)
			->from('shipment_sub_type')
			->count_all_results();
	}
	
	function kategori_detail($id){
		return $this->db->where('type', $id)
			->get('shipment_type')
			->row();
	}
	
	function kategori_sub_detail($id){
		return $this->db->where('sub_id', $id)
			->get('shipment_sub_type')
			->row();
	}
	
	
	
	function get_subs($id){
		return $this->db->where('parent_id', $id)
			->get('shipment_sub_type')
			->result();
	}
	
	function check_exist($nama){
		$query = $this->db->where('shipment_type_name', $nama)
			->get('shipment_type')
			->row();
			
		return $query;
	}

}
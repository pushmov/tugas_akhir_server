<section class="ct-find ct-base-admin ct-admin-request">
			
			<h5 style="margin-top:15px"><a href="<?php echo site_url('admin/subtype'); ?>"><i class="fa fa-reply fa-lg" style="margin-right:10px;"></i>Kembali</a></h5>
			<h3>Tambah Sub Kategori untuk <?php echo $kategori->shipment_type_name; ?></h3>
			
			<p>List sub kategori sekarang : </p>
			<?php if(!empty($subs)) : ?>
				<ul>
				<?php foreach($subs as $s) : ?>
					<li><a href="<?php echo site_url('admin/delete_sub/' . $s->sub_id); ?>" title="delete" onclick="return confirm('Apakah anda yakin?')" style="margin-right:10px;"><i style="color:#ff0000" class="fa fa-times fa-lg"></i></a><?php echo $s->sub_name; ?></li>	
				<?php endforeach; ?>
				</ul>
			<?php else : ?>
			-
			<?php endif; ?>
			
			<fieldset style="padding-top:20px;">
				<form action="" method="POST" >
						<div class="form-group clearfix">
				    	<label class="col-sm-2 control-label">Nama Sub Kategori : </label>
				    	<div class="col-sm-10">
				    		<input type="text" name="name" id="name" class="form-control required">
				    	</div>
				  	</div>
						
						<div class="form-group clearfix">
							<button type="submit" id="done" class="btn btn-primary">TAMBAH</button>
						</div>
						
				</form>
			</fieldset>
															
</section>

<script>
	$('#done').click(function(){
		
		var is_valid = true;
		
		$('.required').each(function(){
			
			if($(this).val().length <= 0){
				is_valid = false;
				return false;
			}
			
			return is_valid;
			
		});
		
	});
</script>
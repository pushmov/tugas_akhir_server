<section class="ct-find ct-base-admin ct-admin-request">
			
			<h3>Daftar Paket Request Acc :</h3>
			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nomor Transaksi</th>
						<th>Tanggal Request</th>
						<th>Nama Paket</th>
						<th>Asal</th>
						<th>Tujuan</th>
						<th>Pemenang</th>
						<th>Driver Bid</th>
						<th>Shipper Offered</th>
						<th>Dana Dikirim</th>
						<th>No. Rekening</th>
						<th>Nama Rekening</th>
						<th>Metode Pembayaran</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>

					<?php foreach ($shipments as $row):?>
						<tr>
							<td><?php echo $row->shipment_id ?></td>
							<td><?php echo $row->invoice_number ?></td>
							<td><?php echo date('d/M/Y',strtotime($row->invoice_created)) ?></td>
							<td><?php echo $row->name; ?></td>
							<td><?php echo $row->pickup_city; ?></td>
							<td><?php echo $row->deliver_city; ?></td>
							<td><?php echo $row->first_name; ?></td>
							<td><?php echo 'Rp. '.number_format($row->bid_price); ?></td>
							<td><?php echo ($row->price_offered == '') ? 'Fleksibel' : 'Rp. '.number_format($row->price_offered); ?></td>
							<td><?php echo 'Rp. '.number_format($row->invoice_transferred); ?></td>
							<td><?php echo $row->invoice_account_number; ?></td>
							<td><?php echo $row->invoice_account_name; ?></td>
							<td><?php echo $row->invoice_method; ?></td>
							<td>
								<?php if($row->is_accepted == 1) : ?>
								<button class="btn btn-success">Telah di-ACC</button>
								
								<?php elseif($row->invoice_status == 'R') : ?>
								<button class="btn btn-danger btn-reject">REJECTED</button>
								<?php else : ?>
								
								<button class="btn btn-primary" onclick="window.location='<?php echo site_url('admin/acc_shipment/' . $row->id); ?>'">ACC</button>
								<button class="btn btn-danger btn-reject" alt="<?php echo $row->id; ?>" data-toggle="modal" data-target="#myModal">REJECT</button>
								
								<?php endif; ?>
							</td>
							
						</tr>
					<?php endforeach ?>
					
				</tbody>
			</table>
			
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Pasang Bid</h4>
						</div>
						<div class="modal-body">
						<h5>Masukkan pesan</h5>

							<form class="form-inline" role="form" action="<?php echo site_url('admin/reject_shipment/') ?>" method="post">
								<input type="hidden" name="id" id="id" value="">
								<div class="form-group">
									<label class="sr-only" for="pesan">Email address</label>
									<input type="text" name="pesan" class="form-control" id="pesan" placeholder="Pesan">
								</div>
								<button type="submit" class="btn btn-danger">Reject</button>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						</div>
					</div>
				</div>
			</div>
															
</section>

<script>
	$('.btn-reject').click(function(){
		$('#id').val($(this).attr('alt'));
	});
</script>
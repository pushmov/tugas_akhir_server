<section class="ct-find ct-base-admin ct-admin-login">
	<div class="container">
				<fieldset>
					<h3>Login Area</h3>
						<form name="form-login" method="POST" action="<?php echo site_url('admin/verify'); ?>" role="form" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="inputEmail3">Username</label>
								<div class="col-sm-4">
									<input type="text" placeholder="Username" name="username" id="inputEmail3" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="inputPassword3">Password</label>
								<div class="col-sm-4">
									<input type="password" placeholder="Password" name="password" id="inputPassword3" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<div class="checkbox">
										<label>
											<input type="checkbox"> Remember me
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button class="btn btn-primary" type="submit">Login</button>
								</div>
							</div>
						</form>

				</fieldset>										
	</div>
</section>
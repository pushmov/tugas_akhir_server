<section class="ct-find ct-base-admin">
	<div class="container">
			
			<h3>Daftar Paket Request Acc :</h3>
			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nomor Transaksi</th>
						<th>Nama Paket</th>
						<th>Asal</th>
						<th>Tujuan</th>
						<th>Nama Driver</th>
						<th>Driver Report</th>
						<th>Tanggal Driver Report</th>
						<th>Shipper Report</th>
						<th>Tanggal Shipper Report</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>

					<?php foreach ($reports as $row):?>
						<tr>
							<td><?php echo $row->shipment_id ?></td>
							<td><?php echo $row->invoice_number ?></td>
							<td><?php echo $row->name; ?></td>
							<td><?php echo $row->pickup_city; ?></td>
							<td><?php echo $row->deliver_city; ?></td>
							<td><?php echo $row->first_name; ?></td>
							<td><?php echo $row->driver_report; ?></td>
							<td><?php echo $row->driver_report_date; ?></td>
							<td><?php echo $row->shipper_report; ?></td>
							<td><?php echo $row->shipper_report_date; ?></td>
							<td>
								<?php if($row->is_completed == 1) : ?>
								<button class="btn btn-success">COMPLETED</button>
								<?php else : ?>
								<button class="btn btn-primary" onclick="window.location='<?php echo site_url('admin/forward/' . $row->id); ?>'">Teruskan Dana ke Driver</button>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach ?>
					
				</tbody>
			</table>
															
	</div>
</section>
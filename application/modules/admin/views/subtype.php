<section class="ct-find ct-base-admin ct-admin-request">
			
			<?php if($this->session->flashdata('info')) : ?>
			<span style="color:#ff0000;font-size:12px;"><?php echo $this->session->flashdata('info'); ?></span>
			<?php endif; ?>
			
			<h3>Daftar Kategori Shipment</h3>
			<button  data-toggle="modal" data-target="#myModal" id="addnew" class="btn btn-primary">TAMBAH BARU</button>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nama Kategori</th>
						<th>Sub Kategori</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>

					<?php foreach ($kategori as $row):?>
						<tr>
							<td><?php echo $row->type ?></td>
							<td><?php echo $row->shipment_type_name ?></td>
							<td><?php echo $row->sub ?></td>
							<td>
								<button class="btn btn-primary" onclick="window.location='<?php echo site_url('admin/add_subtype/' . $row->type); ?>'">TAMBAH SUB</button>
							</td>
							
						</tr>
					<?php endforeach ?>
					
				</tbody>
			</table>
			
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Tambah Parent Kategori Baru</h4>
						</div>
						<div class="modal-body">

							<form class="form-inline" role="form" action="<?php echo site_url('admin/add_parent/') ?>" method="post">
								<div class="form-group">
									<label class="sr-only" for="pesan">Masukkan Nama Kategori</label>
									<input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Kategori">
								</div>
								<button type="submit" class="btn btn-danger">Tambah</button>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						</div>
					</div>
				</div>
			</div>
															
</section>

<script>
	$('.btn-reject').click(function(){
		$('#id').val($(this).attr('alt'));
	});
</script>
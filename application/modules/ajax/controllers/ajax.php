<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MX_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('ajax_model');
	}
	
	public function autosuggest($city=NULL, $provinsi){
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-type: text/xml");
		
		if(is_null($city))
		{
			echo NULL;
		}
		else
		{
			$this->ajax_model->get_kota_kabupaten(urldecode($city), urldecode($provinsi));
		}
	}
	
	public function autosuggest_kecamatan($search, $region1, $region2){
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-type: text/xml");
		
		if(is_null($search))
		{
			echo NULL;
		}
		else
		{
			$this->ajax_model->get_kecamatan(urldecode($search), urldecode($region1), urldecode($region2));
		}
	}
	
	public function autosuggest_kelurahan($search, $region1, $region2, $region3){
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-type: text/xml");
		
		if(is_null($search))
		{
			echo NULL;
		}
		else
		{
			$this->ajax_model->get_kelurahan(urldecode($search), urldecode($region1), urldecode($region2), urldecode($region3));
		}
	}
	
	public function checkemail($email){
		
		$email = $this->ajax_model->check_email(urldecode($email));
		
		$success = (empty($email)) ? '' : 'success';
		
		$response = array('success' => $success);
		
		echo json_encode($response);
		
	}
	
	public function chain(){
		
		$type = $this->input->get('type');
		
		$child_category = $this->ajax_model->get_child_category($type);
		
		echo json_encode($child_category);
		
	}


}
<?php

class Ajax_model extends AppModel {
	
	function get_kota_kabupaten($city,$statecode)
	{
	
		echo '<response>';
		
		$resources = $this->db->where('province_code', $statecode)
			->get('province_code')
			->row();
		
		$province_name_en = $resources->province_name_en;
		
		$result = $this->db->select('DISTINCT(region2)')
			->where('region1',$province_name_en)
			->like('region2', $city)
			->get('postal_code')
			->result();
		
			
		foreach($result as $row)
		{
			echo '<keywords>'.$row->region2.'</keywords>';
		}
		
		echo '</response>';
	}
	
	function get_kecamatan($search, $region1, $region2)
	{
		
		echo '<response>';
		
		$resources = $this->db->where('province_code', $region1)
			->get('province_code')
			->row();
		
		
		$province_name_en = $resources->province_name_en;
		
		
		$result = $this->db->select('DISTINCT(region3)')
			->where('region1',$province_name_en)
			->where('region2', $region2)
			->like('region3', $search)
			->get('postal_code')
			->result();
		
		foreach($result as $row)
		{
			echo '<keywords>'.$row->region3.'</keywords>';
		}
		
		echo '</response>';
		
	}
	
	function get_kelurahan($search, $region1, $region2, $region3)
	{
		
		echo '<response>';
		
		$resources = $this->db->where('province_code', $region1)
			->get('province_code')
			->row();
		
		
		$province_name_en = $resources->province_name_en;
		
		
		$result = $this->db->select('locality, latitude, longitude,postcode ')
			->where(array('region1' => $province_name_en, 'region2' => $region2, 'region3' => $region3))
			->like('locality', $search)
			->get('postal_code')
			->result();
		
		foreach($result as $row)
		{
			echo '<keywords>'.$row->locality. ', '. $row->postcode . '</keywords>';
		}
		
		echo '</response>';
		
	}
	
	function check_email($email){
		return $this->db->where('email', $email)
			->get('member')
			->row();
	}
	
	function get_child_category($type){
		
		$return = array();
		
		$query = $this->db->select('sub_id, sub_name')
			->where('parent_id', $type)
			->get('shipment_sub_type')
			->result();
		
		$return[0] = array("", "--");
		
		if(!empty($query)){
			foreach($query as $q){
				
				$return[] = array($q->sub_id, $q->sub_name);
				
			}
		}
		
		return $return;
		
		
	}
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends REST_Controller
{

	function __construct(){
	
		parent::__construct();
		$this->load->model('Api_dashboard_model');
		$this->load->model('Api_shipment_model');
		
	}
	
	function _shipment_total(){
			$this->Api_shipment_model->set_table('shipment');
			$model_params = array(
					'select' => "shipment.shipment_id,shipment.name,shipment.detail,shipment.date_added,shipment_type.shipment_type_name,shipment_type.icon_name",
					'tablejoin' => "shipment_type",
					'where2' => "shipment.type = shipment_type.type",
					'order_by' => "shipment.date_added ASC"
			);
			
			$all_shipments = $this->Api_shipment_model->multi_table_rows($model_params);
			return sizeof($all_shipments);
	}
	
	
	
	function shipperdashboard_get($uid){
		
		$this->Api_dashboard_model->set_table('member');
		$log_detail = $this->Api_dashboard_model->fetch_row(NULL, array('member_id' => $uid));
		if(empty($log_detail)){
			$this->response(array('status' => 'EMPTY', 'message' => 'Record empty'), 200);
		}
		
		//profpic fetch
		$log_detail->profpic = ($log_detail->profpic == '') ? 'anonymous.png' : $log_detail->profpic;
		
		//calculate main menu counter
		$active_shipment = sizeof($this->Api_dashboard_model->shipper_active_shipment($uid));
		$new_message = sizeof($this->Api_dashboard_model->new_message($uid));
		$my_feedback = sizeof($this->Api_dashboard_model->my_feedback($uid));
		$current_contracts = $this->Api_dashboard_model->shipper_current_contracts($uid);
		$finished_contracts = sizeof($this->Api_dashboard_model->shipper_finished_contracts($uid));
		
		$this->response(array('status' => 'OK', 'message' => 'OK', 
			'user' => $log_detail, 
			'active_shipment' => $active_shipment, 
			'new_message' => $new_message, 
			'my_feedback' => $my_feedback, 
			'current_contracts' => $current_contracts,
			'finished' => $finished_contracts), 
		200);
		
	}
	
	function driverdashboard_get($userid){
		
		$this->Api_dashboard_model->set_table('member');
		$login_detail = $this->Api_dashboard_model->fetch_row(NULL, array('member_id' => $userid));
		
		if(empty($login_detail)){
			$this->response(array('status' => 'EMPTY', 'message' => 'Record not found') , 200);
		}
		
		//profpic fetch
		$login_detail->profpic = ($login_detail->profpic == '') ? base_url() . 'public/images/uploads/memberprofile/anonymous.png' : base_url() . 'public/images/uploads/memberprofile/' . $login_detail->profpic;
		
		
		$earnings = $this->Api_dashboard_model->driver_earnings($userid);
		
		$this->response(array('status' => 'OK', 'message' => 'OK',
			'user' => $login_detail,
			'new_jobs' => sizeof($this->Api_dashboard_model->driver_new_jobs_available($userid)),
			'new_message' => sizeof($this->Api_dashboard_model->new_message($userid)),
			'feedback' => sizeof($this->Api_dashboard_model->my_feedback($userid)),
			'active_bids' => sizeof($this->Api_dashboard_model->driver_active_bids($userid)),
			'current_jobs' => sizeof($this->Api_dashboard_model->driver_current_jobs($userid)),
			'completed_jobs' => sizeof($this->Api_dashboard_model->driver_completed_jobs($userid)),
			'earnings' => 'Rp. '.number_format($earnings->earnings, 2)
		),200);
		
	}
	
	function driverslider_get($userid){
		
		$response = array(
			'find_shipments' => (string) ($this->_shipment_total() > 0) ? "+" . $this->_shipment_total() : 0,
			'my_bids' => (string) (3 > 0) ? "+" . 3 : 0,
			'message' => (string) (sizeof($this->Api_dashboard_model->new_message($userid)) > 0) ? "+" . sizeof($this->Api_dashboard_model->new_message($userid)) : 0,
			'feedback' => (string) (sizeof($this->Api_dashboard_model->my_feedback($userid)) > 0) ? "+" . sizeof($this->Api_dashboard_model->my_feedback($userid)) : 0,
			'my_routes' => (string) (4 > 0) ? "+" . 4 : 0,
			'current_contracts' => (string) (sizeof($this->Api_dashboard_model->driver_current_jobs($userid)) > 0) ? "+" . sizeof($this->Api_dashboard_model->driver_current_jobs($userid)) : 0
		);
		
		$this->response($response, 200);
		
	}


}
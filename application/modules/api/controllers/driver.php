<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_driver_model');
	}
	
	function index_get(){}

	function check_get($email=NULL){
		
		if(!isset($email)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'Field email is empty';
			$this->response($response, 200);
			exit();
		}

		$this->Api_driver_model->set_table('member');
		$checkemail = $this->Api_driver_model->fetch_row(NULL,array('email' => urldecode($email)));
		if(!empty($checkemail)){
			$response['status'] = 'EXISTS';
			$response['message'] = 'Email already registered';
			$this->response($response, 200);
			exit();

		} else {
			$response['status'] = 'OK';
			$response['message'] = 'Email can be used';
			$this->response($response, 200);
			exit();

		}
	}

	function register_post(){

		$response = array();
		if(!$this->input->post('driver_email')){
			$response['status'] = 'EMPTY';
			$response['message'] = 'Field email is empty';
			$this->response($response, 200);
			exit();
		}

		$email = $this->input->post('driver_email');

		$this->Api_driver_model->set_table('member');
		$checkemail = $this->Api_driver_model->fetch_row(NULL,array('email' => $email));
		if(!empty($checkemail)){
			$response['status'] = 'EXISTS';
			$response['message'] = 'Email already registered';
			$this->response($response, 200);
			exit();
		}

		
		$address = $this->input->post('driver_address');
		$city = $this->input->post('driver_city');
		$state_code_name = $this->input->post('driver_province');
		
		$state_code = $this->Api_driver_model->get_state_name($state_code_name);
		
		$zip_code = $this->input->post('driver_zip');
		

		$username = $this->input->post('driver_username');

		$fields = array(
			'email' => $email,
			'username' => $username,
			'password' => $this->input->post('driver_password'),
			'phone' => $this->input->post('driver_phone'),
			'address' => $address,
			'city' => $city,
			'state_code' => $state_code,
			'zip_code' => $zip_code,
			'ip_address' => $this->input->ip_address(),
			'is_driver' => 1,
			'activation_code' => sha1($email),
			'activation_status' => 1,
			'subscribe_newsletter' => $this->input->post('newsletter'),
			'subscribe_push_notification' => $this->input->post('notification')
		);

		$member_id = $this->Api_driver_model->insert($fields);

		//route defaul insertion
		$route_fields = array(
			'member_id' => $member_id,
			'route_name' => 'state',
			'state_list' => $state_code
		);

		$this->Api_driver_model->set_table('member_route');
		$this->Api_driver_model->insert($route_fields);
		
		//driver insert bank
		$this->Api_driver_model->set_table('member_profile');
		$profile_data = array(
			'member_id' => $member_id,
			'bank' => $this->input->post('driver_bank'),
			'bank_account_number' => $this->input->post('driver_bank_number')
		);
		$this->Api_driver_model->insert($profile_data);

		$response['status'] = 'OK';
		$response['message']  = 'Success';
		$this->response($response, 200);

	}

	function profile_get($id=NULL){

		$response = array();
		if(!isset($id)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'Empty field';
			$response['profile'] = '';
			$response['photo'] = '';
			$response['detail'] = '';
			$this->response($response, 200);
			exit();
		}


		$this->Api_driver_model->set_table('member');
		$row_driver = $this->Api_driver_model->fetch_row(NULL,array('member_id' => $id));
		if(empty($row_driver)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'No record found';
			$response['profile'] = '';
			$response['photo'] = '';
			$response['detail'] = '';
			$this->response($response, 200);
			exit();
		}

		$response['status'] = 'OK';
		$response['message'] = 'Profile Found';
		$response['profile'] = $row_driver;

		$this->Api_driver_model->set_table('member_photo');
		$row_photo = $this->Api_driver_model->fetch_rows(NULL,array('member_id' => $id));
		$response['photo'] = (empty($row_photo)) ? '' : $row_photo;

		$this->Api_driver_model->set_table('member_profile');
		$row_detail = $this->Api_driver_model->fetch_row(NULL,array('member_id' => $id));
		$response['detail'] = (empty($row_detail)) ? '' : $row_detail;
		$this->response($response, 200);
		
	}
	
	function profile_put($id){
		
		$email = $this->input->put('email');
		$username = $this->input->put('username');
		
		$fname = $this->input->put('first_name');
		$mname = $this->input->put('middle_name');
		$lname = $this->input->put('last_name');
		
		$dob = $this->input->put('dob');
		$address = $this->input->put('address');
		$city = $this->input->put('city');
		$state_code = $this->input->put('state_code');
		$zip_code = $this->input->put('zip_code');
		$phone = $this->input->put('phone');
		
		
		$this->Api_driver_model->set_table('member');
		$this->Api_driver_model->update($data, array('member_id' => $id));
		
	}
	
	function makebid_post(){
		$response = array();
		
		if(!$this->input->post('bid_price')){
			$response['message'] = 'Please input bid value';
			$this->response($response, 200);
		}
		
		$request = array(
			'bid_price' => $this->input->post('bid_price'),
			'bid_submit_date' => date('Y-m-d H:i:s'),
			'shipment_id' => $this->input->post('shipment_id'),
			'member_id' => $this->input->post('member_id'),
			'status' => $this->input->post('status')
		);
		
		$this->Api_driver_model->set_table('notification');
		$check_row = $this->Api_driver_model->fetch_row(NULL, array('shipment_id' => $this->input->post('shipment_id'), 'member_id' => $this->input->post('member_id')));
		
		$request['notification_read_date'] = date('Y-m-d H:i:s');
		if(empty($check_row))
		{
			$notification_id = $this->Api_driver_model->insert($request);
		}
		else 
		{
			$this->Api_driver_model->update($request, array('notification_id' => $check_row->notification_id));
			$notification_id = $check_row->notification_id;
		}
		
		//reduce bid quota by 1
		$this->Api_driver_model->set_table('member');
		$row_member = $this->Api_driver_model->fetch_row('member_id, bid_quota', array('member_id' => $request['member_id']));
		$this->Api_driver_model->update(array('bid_quota' => $row_member->bid_quota - 1), array('member_id' => $request['member_id']));
		
		$response['message'] = 'Bid placed successfully. Thanks';
		$response['status'] = 'success';
		$response['notification_id'] = $notification_id;
		$this->response($response, 200);
		
		//send notification to shipper about bid price
		
		
	}
	
	function quota_get($id, $shipmentid){
		
		$this->Api_driver_model->set_table('member');
		$row = $this->Api_driver_model->fetch_row('member_id,bid_quota', array('member_id' => $id));
		
		$this->Api_driver_model->set_table('notification');
		$check_status = $this->Api_driver_model->fetch_row('notification_id', array('shipment_id' => $shipmentid, 'member_id' => $id, 'status' => 'A'));
		$row->already_bid = (!empty($check_status)) ? "Y" : "N";
		
		$this->response($row, 200);
		
	}
	
	function mybids_get($userid){
		
		$this->Api_driver_model->set_table('notification');
		$notifications = $this->Api_driver_model->get_notification_list($userid);
		
		foreach($notifications as $n){
			$n->bid = "Rp. " . number_format($n->bid_price, 2);
			
			$this->Api_driver_model->set_table('shipment_picture');
			$picture = $this->Api_driver_model->fetch_row('shipment_picture', array('shipment_id' => $n->shipment_id));
			
			$this->Api_driver_model->set_table('shipment_type');
			$icon = $this->Api_driver_model->fetch_row('icon_name', array('type' => $n->type));
			
			$n->picture = (empty($picture)) ? base_url() . 'public/images/icon/' . $icon->icon_name : base_url() . 'public/images/uploads/shipmentpicture/' . $picture->shipment_picture;
		}
		
		$response = array(
			'success' => 'success',
			'bids' => $notifications
		);
		
		$this->response($response, 200);
		
	}
	
	function cancelbid_put($notificationid){
		
		$this->Api_driver_model->set_table('notification');
		$row = $this->Api_driver_model->fetch_row(NULL, array('notification_id' => $notificationid) );
		
		if(empty($row)) return;
		
		$update_data = array(
			'status' => 'O',
			'bid_price' => NULL,
			'bid_submit_date'=> NULL
		);
		
		$this->Api_driver_model->update($update_data, array('notification_id' => $notificationid));
		
		$response = array(
			'UID' => $row->member_id,
			'success' => 'success'
		);
		
		$this->response($response, 200);
	}
	
	function report_get($userid){
		
		$current_jobs = $this->Api_driver_model->get_current_jobs($userid);
		
		foreach($current_jobs as $c){
			$c->bid = "Rp. " . number_format($c->bid_price, 2);
			
			$this->Api_driver_model->set_table('shipment_picture');
			$picture = $this->Api_driver_model->fetch_row('shipment_picture', array('shipment_id' => $c->shipment_id));
			
			$this->Api_driver_model->set_table('shipment_type');
			$icon = $this->Api_driver_model->fetch_row('icon_name', array('type' => $c->type));
			
			$c->picture = (empty($picture)) ? base_url() . 'public/images/icon/' . $icon->icon_name : base_url() . 'public/images/uploads/shipmentpicture/' . $picture->shipment_picture;
			
			$c->due_date = date('d/M/Y',strtotime($c->deliver_max_date));
			
		}
		
		$response = array(
			'success' => 'success',
			'current_jobs' => $current_jobs
		);
		
		$this->response($response, 200);
	}
	
	function report_put($notification_id){
		
		$report = $this->input->put('driver_report');
		if($report == ''){
			
			$response = array('success' => 'fail', 'message' => 'no input');
			$this->response($response, 200);
			exit();
		}
		
		$this->Api_driver_model->set_table('notification');
		$row_notification = $this->Api_driver_model->fetch_row(NULL, array('notification_id' => $notification_id));
		
		if(empty($row_notification)){
			$response = array('success' => 'fail', 'message' => 'no record');
			$this->response($response, 200);
			exit();
		}
		
		$update_data = array(
			'status' => 'C',
			'driver_report' => $report,
			'driver_report_date' => date('Y-m-d H:i:s')
		);
		$this->Api_driver_model->update($update_data , array('notification_id' => $notification_id));
		
		$response = array(
			'UID' => $row_notification->member_id,
			'success' => 'success'
		);
		
		$this->response($response, 200);
		
	}
	
	
	function completedjobs_get($id){
		
		$result = $this->Api_driver_model->completed_jobs($id);
		
		
		
		if(!empty($result))
		{
			foreach($result as $c){
				
				$c->bid_price = "Rp. " . number_format($c->bid_price, 2);
				
				$this->Api_driver_model->set_table('shipment_picture');
				$picture = $this->Api_driver_model->fetch_row('shipment_picture', array('shipment_id' => $c->shipment_id));
				
				$this->Api_driver_model->set_table('shipment_type');
				$icon = $this->Api_driver_model->fetch_row('icon_name', array('type' => $c->type));
				
				$c->shipment_picture = (empty($picture)) ? base_url() . 'public/images/icon/' . $icon->icon_name : base_url() . 'public/images/uploads/shipmentpicture/' . $picture->shipment_picture;
				
				$c->shipper_report_date = date('d/M/Y',strtotime($c->driver_report_date));
				
				$this->Api_driver_model->set_table('feedback');
				$check_feedback = $this->Api_driver_model->fetch_row(NULL, array('from_member_id' => $id, 'shipment_id' => $c->shipment_id));
				
				$c->feedback_sent = (empty($check_feedback)) ? 'N' : 'Y';
				
				$fund_status = $this->Api_driver_model->get_fund_status($c->shipment_id, $id);
				$c->fund_status = ($fund_status->is_completed == 1) ? 'Y' : 'N';
				
			}
			
		}
		
		$response = array(
			'success' => 'success',
			'jobs' => $result
		);
		
		$this->response($response, 200);
		
	}
	
	
	
}
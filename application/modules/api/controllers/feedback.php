<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_feedback_model');
		$this->Api_feedback_model->set_table('feedback');
	}
	
	function all_get(){

		$response = array();

		$params = array(
			'tablejoin' => "member",
			'order_by' => "date_added DESC",
			'where2' => "feedback.recipient_id = member.member_id"
		);

		$feedbacks = $this->Api_feedback_model->multi_table_rows($params);
		if(empty($feedbacks)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'No record found';
			$response['feedback'] = '';
			$this->response($response, 200);
		}

		$response['status'] = 'OK';
		$response['message'] = '';
		$response['feedback'] = $feedbacks;
		$this->response($response, 200);
		
	}
	
	function index_get($id){
		
		$params = array(
			'tablejoin' => "member",
			'order_by' => "date_added DESC",
			'where2' => "feedback.recipient_id = member.member_id AND member.member_id = '$id'"
		);
		
		$feedbacks = $this->Api_feedback_model->multi_table_rows($params);
		if(empty($feedbacks)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'No record found';
			$response['feedback'] = '';
			$this->response($response, 200);
		}

		$response['status'] = 'OK';
		$response['message'] = '';
		$response['feedback'] = $feedbacks;
		$this->response($response, 200);
		
	}
	
	//create new record
	function index_post(){
		
		$title = $this->input->post('title');
		$text = $this->input->post('text');
		$rating = $this->input->post('rating');
		$member_id = $this->input->post('posted_by');
		$recpt_id = $this->input->post('recipient_id');
		
		$active_record = array(
			'title' => $title,
			'text' => $text,
			'rating' => $rating,
			'member_id' => $member_id,
			'recipient_id' => $recpt_id
		);
		
		$insert_id = $this->Api_feedback_model->insert($active_record);
		$this->response(array('status' => 'OK', 'message' => 'Feedback inserted', 'data' => $insert_id), 200);
		
	}
	
	//update existing record
	function index_put($id){
		
		$title = $this->input->put('title');
		$text = $this->input->put('text');
		$rating = $this->input->put('rating');
		$member_id = $this->input->put('posted_by');
		$recpt_id = $this->input->put('recipient_id');
		
		$active_record = array(
			'title' => $title,
			'feedback_text' => $text,
			'rating' => $rating,
			'member_id' => $member_id,
			'recipient_id' => $recpt_id,
		);
		
		$this->Api_feedback_model->update($active_record, array($this->pk => $id));
		$this->response(array('status' => 'OK', 'message' => 'Feedback updated', 'data' => $id), 200);
		
	}
	
	//create or update reply for existing record
	function reply_put($id){
		
		$recpt_reply = $this->input->put('recipient_reply');
		
		$active_record = array(
			'recipient_reply' => $recpt_reply,
			'recipient_reply_date' => date('Y-m-d H:i:s')
		);
		
		$this->Api_feedback_model->update($active_record, array($this->pk => $id));
		$this->response(array('status' => 'OK', 'message' => 'Feedback updated', 'data' => $id), 200);
		
	}
	
	function index_delete($id){
	
		$this->Api_feedback_model->delete(array($this->pk => $id));
		$this->response(array('status' => 'OK', 'message' => 'Feedback deleted'), 200);
		
	}
	
	function shipper_get($notification_id){
		
		$this->Api_feedback_model->set_table('notification');
		$row_notification = $this->Api_feedback_model->fetch_row('notification_id, shipment_id, member_id, bid_price, shipper_report_date', array('notification_id' => $notification_id));
		$row_notification->bid_price = "Rp. ".number_format($row_notification->bid_price,2);
		$row_notification->shipper_report_date = date('d/M/Y',strtotime($row_notification->shipper_report_date));
		
		$this->Api_feedback_model->set_table('shipment');
		$column = 'shipment_id, name, date_added, type';
		$row_shipment = $this->Api_feedback_model->fetch_row($column, array('shipment_id' => $row_notification->shipment_id));
		$row_shipment->added = date('d/M/Y',strtotime($row_shipment->date_added));
		
		$this->Api_feedback_model->set_table('shipment_picture');
		$column = 'shipment_picture';
		$row_picture = $this->Api_feedback_model->fetch_row($column, array('shipment_id' => $row_notification->shipment_id));
		
		$this->Api_feedback_model->set_table('shipment_type');
		$row_type = $this->Api_feedback_model->fetch_row(NULL, array('type' => $row_shipment->type));
		
		$picture = (empty($row_picture)) ? base_url() . 'public/images/icon/' . $row_type->icon_name : base_url() . 'public/images/uploads/' . $row_picture->shipment_picture;
		
		
		$this->Api_feedback_model->set_table('member');
		$row_member = $this->Api_feedback_model->fetch_row('username, profpic', array('member_id' => $row_notification->member_id));
		$row_member->profpic = ($row_member->profpic == '') ? base_url() . 'public/images/uploads/memberprofile/anonymous.png' : base_url() . 'public/images/uploads/memberprofile/' . $row_member->profpic;
		
		$this->response(array(
			'success' => 'success',
			'shipment' => $row_shipment,
			'notification' => $row_notification,
			'picture' => $picture,
			'winner' => $row_member
		),200);
	}
	
	function shipper_post(){
		
		$header = $this->input->post('feedback_header');
		$content = $this->input->post('feedback_content');
		$notification_id = $this->input->post('notification_id');
		$rating = $this->input->post('rating');
		
		$this->Api_feedback_model->set_table('notification');
		$row_notification = $this->Api_feedback_model->fetch_row(NULL, array('notification_id' => $notification_id));
		
		$this->Api_feedback_model->set_table('shipment');
		$row_shipment = $this->Api_feedback_model->fetch_row(NULL, array('shipment_id' => $row_notification->shipment_id));
		
		$request = array(
			'title' => $header,
			'feedback_text' => $content,
			'rating' => $rating,
			'from_member_id' => $row_shipment->member_id,
			'recipient_id' => $row_notification->member_id,
			'shipment_id' => $row_shipment->shipment_id
		);
		
		
		$this->Api_feedback_model->set_table('feedback');
		$this->Api_feedback_model->insert($request);
		
		$this->response(array(
			'UID' => $row_shipment->member_id,
			'success' => 'success'
		),200);
		
	}
	
	function driver_get($id){
		
		$result = $this->Api_feedback_model->my_feedbacks($id);
		
		if(!empty($result))
		{
		
			foreach($result as $c){
				
				$this->Api_feedback_model->set_table('shipment_picture');
				$picture = $this->Api_feedback_model->fetch_row('shipment_picture', array('shipment_id' => $c->shipment_id));
				
				$this->Api_feedback_model->set_table('shipment_type');
				$icon = $this->Api_feedback_model->fetch_row('icon_name', array('type' => $c->type));
				
				$c->picture = (empty($picture)) ? base_url() . 'public/images/icon/' . $icon->icon_name : base_url() . 'public/images/uploads/shipmentpicture/' . $picture->shipment_picture;
				
				$c->date_added = date('d/M/Y',strtotime($c->date_added));
				
				$string = strip_tags($c->feedback_text);
				if (strlen($string) > 100) {
					$stringCut = substr($string, 0, 100);
					$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
				}
				
				$c->feedback_text = $string;
				
			}
		
		}
		
		$this->response(array(
			'feedbacks' => $result,
			'success' => 'success'
		));
		
	}
	
	function driver_put($fid){
		
		$rating = $this->input->put('rating');
		$header = $this->input->put('feedback_title');
		$content = $this->input->put('feedback_content');
		
		$this->Api_feedback_model->set_table('feedback');
		$detail = $this->Api_feedback_model->fetch_row(NULL, array('feedback_id' => $fid));
		
		$request = array(
			'recipient_title' => $header,
			'recipient_rating' => $rating,
			'recipient_reply' => $content,
			'recipient_reply_date' => date('Y-m-d H:i:s')
		);
		
		$this->Api_feedback_model->update($request, array('feedback_id' => $fid));
		
		$this->response(array(
			'success' => 'success',
			'UID' => $detail->recipient_id
		), 200);
		
	}
	
	function driver_post($nid){
		
		$this->Api_feedback_model->set_table('notification');
		$row_notification = $this->Api_feedback_model->fetch_row(NULL, array('notification_id' => $nid));
		
		$driver_id = $row_notification->member_id; //sender
		$shipment_id = $row_notification->shipment_id;
		
		$this->Api_feedback_model->set_table('shipment');
		$row_shipment = $this->Api_feedback_model->fetch_row('shipment_id, name, member_id', array('shipment_id' => $shipment_id));
		
		$insert_data = array(
			'title' => $this->input->post('feedback_title'),
			'feedback_text' => $this->input->post('feedback_content'),
			'rating' => $this->input->post('rating'),
			'from_member_id' => $driver_id,
			'recipient_id' => $row_shipment->member_id,
			'shipment_id' => $row_shipment->shipment_id
		);
		
		$this->Api_feedback_model->set_table('feedback');
		$this->Api_feedback_model->insert($insert_data);
		
		$this->response(array(
			'success' => 'success',
			'UID' => $driver_id
		));
		
	}
	
	function detail_get($id){
		
		$this->Api_feedback_model->set_table('notification');
		$row_notification = $this->Api_feedback_model->fetch_row(NULL, array('notification_id' => $id));
		
		$driver_id = $row_notification->member_id; //sender
		$shipment_id = $row_notification->shipment_id;
		
		$this->Api_feedback_model->set_table('shipment');
		$row_shipment = $this->Api_feedback_model->fetch_row('shipment_id, name', array('shipment_id' => $shipment_id));
		
		$this->Api_feedback_model->set_table('feedback');
		$detail = $this->Api_feedback_model->fetch_row(NULL, array('feedback_id' => $id));
		
		$response = array(
			'success' => 'success',
			'shipment' => $row_shipment,
			'feedback' => $detail
		);
		
		$this->response($response, 200);
	
	}
	
	function calculate_rating($memberid){
		
		$result = $this->Api_feedback_model->my_feedbacks($memberid);
		
		$rating = 0;
		$total = 0;
		
		if(!empty($result))
		{
		
			foreach($result as $row)
			{
				$rating += (int) $row->rating;
				$total++;
			}
		
		}
		
		return number_format(round(($rating / $total),1) , 1);
		
	}
	
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Find extends REST_Controller
{
	
	const EARTH_RADIUS_IN_MILE = 3959;
	
	const MILES_TO_KM = 1.60934;
	const MILES_TO_M = 1609.34;

	function __construct(){
		parent::__construct();
		$this->load->model('Api_find_model', 'Model');
		$this->load->model('Appmodel');
	}
	
	function index_get($latitude, $longitude, $distance, $limit, $mode='pickup', $unit = 'KM'){
		//find shipment nearby
		
		// $test_lat = -6.954440;
		// $test_lon = 107.636940;
		// $radius = 5;
		// $mode = 'pickup';
		
		if($unit == 'KM')
		{
			$earth_radius = self::EARTH_RADIUS_IN_MILE * self::MILES_TO_KM;
		}
		else
		{
			$earth_radius = self::EARTH_RADIUS_IN_MILE * self::MILES_TO_M;
		}
		
		if($mode == 'pickup'){
			$result = $this->Model->geo_search_pickup($latitude, $longitude, $distance, (int) $limit, $earth_radius);
		} else {
			$result = $this->Model->geo_search_deliver($latitude, $longitude, $distance, (int) $limit, $earth_radius);
		}
		
		
		foreach($result as $row){
		
			$row->human_distance = ($row->distance > 1) ? round($row->distance, 2) . " " . strtolower($unit) : round($row->distance, 2) . " " . strtolower($unit);
			
			$this->Appmodel->set_table('shipment_picture');
			$icon = $this->Appmodel->fetch_row('shipment_picture', array('shipment_id' => $row->shipment_id));
			
			$arr_icon = $this->_get_icon($row->type);
			
			$row->icon = (empty($icon)) ? $arr_icon['icon'] : base_url() . 'public/images/uploads/shipmentpicture/' . $icon->shipment_picture;
			$row->icon_name = $arr_icon['name'];
			
			
			$string = strip_tags($row->detail);
			if (strlen($string) > 100) {
				$stringCut = substr($string, 0, 100);
				$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
			}
			
			$row->small_description = $string;
			
			$row->human_date_added = date("d/M/Y", strtotime($row->date_added));
			
		}
		
		$response['shipments'] = $result;
		$response['total'] = sizeof($result);
		
		$this->response($response, 200);
		
		
	}
	
	private function _get_icon($type){
		
		$this->Appmodel->set_table('shipment_type');
		$icon = $this->Appmodel->fetch_row(NULL, array('type' => $type));
		
		$return['icon'] = base_url() . 'public/images/icon/' . $icon->icon_name;
		$return['name'] = $icon->shipment_type_name;
		
		return $return;
	}
	
	function index_post(){}
	
	function index_put(){}
	
	function index_delete(){}
	
}
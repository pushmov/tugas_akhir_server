<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Geografi extends REST_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Appmodel');
	}
	
	function kota_get($kode_provinsi){
	
		$this->Appmodel->set_table('province_code');
		$name = $this->Appmodel->fetch_row('province_name_en', array('province_code' => $kode_provinsi));
		
		$this->Appmodel->set_table('postal_code');
		$result = $this->Appmodel->fetch_rows('DISTINCT(region2)', array('region1' => $name->province_name_en));
		
		$response = array();
		foreach($result as $row){
			$response[$row->region2] = $row->region2;
		}
		
		$this->response($response, 200);
		
	}
	
	function provinsi_get(){
		
		$this->Appmodel->set_table('province_code');
		$provinces = $this->Appmodel->fetch_rows('province_code, province_name');
		
		$this->response($provinces, 200);
		
	}
	
	function position_post(){
		
		$userid = $this->input->post('userid');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
		
		$request = array(
			'user_id' => $userid,
			'latitude' => $latitude,
			'longitude' => $longitude
		);
		
		
		$this->Appmodel->set_table('tracking');
		$this->Appmodel->insert($request);
		
		$response = array(
			'success' => 'success'
		);
		
		$this->response($response, 200);
	}
	
	
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends REST_Controller
{
	
	var $name = 'login';
	
	function __construct(){
		parent::__construct();
		$this->load->model('Api_login_model');
		$this->Api_login_model->set_table('member');
	}
	
	function index_get(){
		echo 'assaa';
		exit();
	}
	
	function index_post(){
			
			$response = array();
			
			if ( $this->input->post('login') == 'login' ) {
					$email 		= $this->input->post('email');
					$password = $this->input->post('password'); //fix me : salt & hashed password
					
					$result = $this->Api_login_model->fetch_row('member_id,email,username,first_name,middle_name,last_name,activation_status,activation_code,is_driver',
							array(
									'email' => $email,
									'password' => $password
							)
					);
					
					if(empty($result)){
							$response['message'] 	= 'Invalid email or password';
							$response['success'] 	= NULL;
							$response['flag']			= FALSE;
							$this->response($response, 200);
							exit();
					}
					
					// if(!$result->activation_status){
							// $response['message'] 	= 'Account not yet activated';
							// $response['tag'] = 'activation';
							// $response['success'] 	= NULL;
							// $response['flag']			= FALSE;
							// $this->response($response, 200);
							// exit();
					// }
					
					$response['message'] = 'Login Successful';
					$response['success'] = 'success';
					$response['flag'] = TRUE;
					$response['user_detail'] = $result;
					$response[$this->name]['member_id'] = $result->member_id;
					$response[$this->name]['email'] = $result->email;
					$response[$this->name]['username'] = $result->username;
					$response[$this->name]['first_name'] = $result->first_name;
					$response[$this->name]['middle_name'] = $result->middle_name;
					$response[$this->name]['last_name'] = $result->last_name;
					$response[$this->name]['activation_code'] = $result->activation_code;
					$response[$this->name]['type'] = ($result->is_driver) ? 'driver' : 'shipper';
					$this->response($response, 200);
					exit();
			}
			
			exit;
	}
	
	function index_put(){}
	
	function index_delete(){}
	
}
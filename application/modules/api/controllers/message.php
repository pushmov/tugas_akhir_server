<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_message_model');
		
	}
	
	function index_get($userid){
		
		$thread_lists = $this->Api_message_model->get_thread_lists($userid);
		
		foreach($thread_lists as $row)
		{
			$row->is_recipient_has_read = ($row->recipient_read_date == '') ? 'N' : 'Y';
		}
		
		$this->response(array(
			'threads' => $thread_lists,
			'success' => 'success'
		),200);
		
	}
	
	function index_post(){
		
		$sender_id = $this->input->post('sender_id');
		$receiver_id = $this->input->post('receiver_id');
		$message = $this->input->post('message');
		
		$this->Api_message_model->set_table('message');
		$insert_message = array(
			'title' => '',
			'message_text' => $message,
			'member_id' => $sender_id
		);
		$insert_id = $this->Api_message_model->insert($insert_message);
		
		
		$this->Api_message_model->set_table('message_recipient');
		$insert_recipient = array( 
			'message_id' => $insert_id,
			'member_id' => $receiver_id 
		);
		$this->Api_message_model->insert($insert_recipient);
		
		$this->response(array(
			'success' => 'success'
		), 200);
	
	}
	
	function index_put(){}
	
	function index_delete(){}
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_notification_model');
	}
	
	//function index_get(){}
	
	function index_post(){

		$response = array();

		//listen curl post
		// $member_id = $this->input->post('memberid');
		$shipment_id = $this->input->post('shipmentid');

		//dummy

		$this->Api_notification_model->set_table('shipment');
		$shipment_detail = $this->Api_notification_model->fetch_row(NULL,array('shipment_id' => $shipment_id));

		if(empty($shipment_detail)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'Shipment not found';
			$this->response($response, 200);
			exit();
		}

		$start_state_code = $shipment_detail->pickup_province_code;
		$end_state_code = $shipment_detail->deliver_province_code;

		$start_zip_code = $shipment_detail->pickup_zip_code;
		$end_zip_code = $shipment_detail->deliver_zip_code;

		$this->Api_notification_model->set_table('member_route');
		$routes = $this->Api_notification_model->custom_rows(NULL,"FIND_IN_SET( '$start_state_code' , `state_list` ) 
			OR FIND_IN_SET('$end_state_code', `state_list`) 
			OR FIND_IN_SET('$start_zip_code', `start_zip_code`) 
			OR FIND_IN_SET('$end_zip_code', `end_zip_code`) 
			");

		$member_list = array(); 
		foreach($routes as $route){
			//member yg setting route nya cocok dengan shipment skrg
			$member_list[] = $route->member_id;
		}

		$this->Api_notification_model->set_table('notification');
		$uniq = array_unique($member_list);
		foreach($uniq as $key => $val){
			//push notification to every member
			//email notification 
			$notification_row = array(
				'status' => 'O',
				'member_id' => $val,
				'shipment_id' => $shipment_id
			);
			$this->Api_notification_model->insert($notification_row);
			
		}

		$this->load->library('Pushover');
		exit();

	}
	
	
}


<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends REST_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model('Api_payment_model');
	}
	
	function index_get($shipmentid){
		
		$row_payment = $this->Api_payment_model->confirm_payment($shipmentid);
		
		$row_payment->bid_agreed = 'Rp. '.number_format($row_payment->bid_price);
		
		$this->response(array('success' => 'success', 'result' => $row_payment ),200);
		
	}
	
	function index_put($shipmentid){
		
		$this->Api_payment_model->set_table('shipper_invoice');
		$row_invoice = $this->Api_payment_model->fetch_row(NULL, array('shipment_id' => $shipmentid));
		
		$date_paid = $this->input->put('yy') .'-'. $this->input->put('mm') .'-'. $this->input->put('dd');
		
		$request = array(
			'invoice_method' => $this->input->put('invoice_method'),
			'invoice_date_paid' => $date_paid,
			'invoice_transferred' => $this->input->put('transferred'),
			'invoice_account_name' => $this->input->put('account_name'),
			'invoice_account_number' => $this->input->put('account_number'),
			'invoice_status' => NULL,
			'status_message' => NULL
		);
		
		$this->Api_payment_model->update_method($shipmentid, $request);
		
		$this->Api_payment_model->set_table('shipment');
		$row = $this->Api_payment_model->fetch_row(NULL, array('shipment_id' => $shipmentid));
		
		$this->Api_payment_model->set_table('notification');
		$this->Api_payment_model->update(array('status' => 'ACC'), array('shipment_id' => $shipmentid, 'member_id' => $row_invoice->driver_id));
		
		$this->response(array('success' => 'success', 'UID' => $row->member_id), 200);
		
	}
}
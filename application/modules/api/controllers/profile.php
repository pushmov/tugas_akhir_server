<?php defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH . 'modules/api/controllers/feedback.php');

class Profile extends REST_Controller
{

	const FILE_ALLOWED_TYPES = 'jpg|JPG|jpeg|JPEG|png|PNG';
	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;

	function __construct(){
		parent::__construct();
		$this->load->model('Api_profile_model');
	}
	
	function _getrating($memberid){
		$this->Api_profile_model->set_table('feedback');
		$result = $this->Api_profile_model->fetch_rows('rating', array('recipient_id' => $memberid));
		$total = 0;
		foreach($result as $r){
			$total += $r->rating;
		}
		
		$rating = (empty($result)) ? 0 : round($total / sizeof($result),2);
		return $rating;
	}
	
	function index_get($id){
			
		$response = array();
		
		$this->Api_profile_model->set_table('member');
		$row = $this->Api_profile_model->fetch_row(NULL, array('member_id' => $id));
		
		if(empty($row)){
			$response['success'] = 'fail';
			$response['message'] = 'No Record Found.';
			$this->response($response, 200);
		}
		
		
		//fetch complete province name
		$this->Api_profile_model->set_table('province_code');
		$row_province = $this->Api_profile_model->fetch_row(NULL, array('province_code' => $row->state_code));
		$row->province_name = (!empty($row_province)) ? $row_province->province_name : 0;
		
		
		$feedback = new Feedback();
		$row->rating = $feedback->calculate_rating($id);
		
		$recent = array();
		
		$row->status = ((bool)$row->is_driver) ? 'Driver' : 'Shipper';
		if((bool) $row->is_driver)
		{
			
			$this->Api_profile_model->set_table('notification');
			$completed = $this->Api_profile_model->recent_shipment($id);
			$row->completed = sizeof($completed);
			
			foreach($completed as $c)
			{
				
				$this->Api_profile_model->set_table('shipment_picture');
				$get_pic = $this->Api_profile_model->fetch_row(NULL, array('shipment_id' => $c->shipment_id));
				
				
				if(empty($get_pic)){
					
					$this->Api_profile_model->set_table('shipment_type');
					$type_row = $this->Api_profile_model->fetch_row(NULL, array('type' => $c->type));
						
					$icon_path = base_url() . 'public/images/icon/' . $type_row->icon_name;
						
				} else {
					
					$icon_path = base_url() . 'public/images/uploads/shipmentpicture/' . $get_pic->shipment_picture;
				}
				
				$c->picture = $icon_path;
				
				
				$this->Api_profile_model->set_table('feedback');
				$feedback_given = $this->Api_profile_model->fetch_row('feedback_text', array('shipment_id' => $c->shipment_id, 'recipient_id' => $c->member_id));
				$c->feedback = (!empty($feedback_given)) ? $feedback_given->feedback_text : 'No feedback given yet';
				$c->date_completed = date('d-M-Y', strtotime($c->shipper_report_date));
				
			}
			
			$recent = $completed;
		}
		else
		{
			$row->completed = 0;
		}
		
		$row->profpic = ($row->profpic == '') ? base_url() . 'public/images/uploads/memberprofile/anonymous.png' : base_url() . 'public/images/uploads/memberprofile/' . $row->profpic;
		
		$response['success'] = 'success';
		$response['data'] = $row;
		$response['recent'] = $completed;
		$this->response($response, 200);
			
	}
	
	function index_post(){
		
	}
	
	function index_put($memberid){
		//update member profile
		
		$response = array();
		
		//province
		$this->Api_profile_model->set_table('province_code');
		$row_province = $this->Api_profile_model->fetch_row(NULL, array('province_name' => $this->input->put('province')));
		
		$this->Api_profile_model->set_table('member');
		$row_member = $this->Api_profile_model->fetch_row(NULL, array('member_id' => $memberid));
		
		if(empty($row_member)){
			$response['success'] = 'fail';
			$response['message'] = 'Record not found';
			$this->response($response, 200);
		}
		
		$put_data = array(
			'username' => $this->input->put('username'),
			'first_name' => $this->input->put('first_name'),
			'middle_name' => $this->input->put('middle_name'),
			'last_name' => $this->input->put('last_name'),
			'address' => $this->input->put('address'),
			'city' => $this->input->put('city'),
			'state_code' => $row_province->province_code,
			'zip_code' => $this->input->put('zipcode'),
			'phone' => $this->input->put('phone'),
			'subscribe_newsletter' => ($this->input->put('subscribe_newsletter') == '1') ? 1 : 0,
			'subscribe_push_notification' => ($this->input->put('subscribe_pushnotification') == '1')  ? 1 : 0
		);
		
		$this->Api_profile_model->update($put_data, array('member_id' => $memberid));
		
		$response['success'] = 'success';
		$response['message'] = 'Profile Updated';
		$this->response($response, 200);
		
	}
	
	function index_delete(){}
	
	function upload_post($memberid){
		
		$photo = (!empty($_FILES['picture']['tmp_name'])) ? time() .'-'.$memberid : '';
		
		/** upload configuration */
		$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
		$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
		$config['upload_path'] = './public/images/uploads/memberprofile/';
		
		if($photo != '')
		{
			
			$config['file_name'] = $photo;
			$this->load->library('upload',$config);
			
			if(!$this->upload->do_upload('picture'))
			{
				
				$response['success'] = 'fail';
				$response['message'] = $this->upload->display_errors('', '');
				$this->response($response,200);
				
			
			} else {
				
				
				$data = $this->upload->data();
				$photo = $data['orig_name'];
				
				$this->Api_profile_model->set_table('member_photo');
				$insert_photo = array(
					'member_id' => $memberid,
					'photo_file' => $photo
				);
				
				$this->Api_profile_model->insert($insert_photo);
				
				//update main profile
				$this->Api_profile_model->set_table('member');
				$update = array( 'profpic' => $photo );
				$this->Api_profile_model->update($update, array('member_id' => $memberid));
				
				$response['success'] = 'success';
				$response['message'] = 'Success. File uploaded to server.';
				$response['url'] = base_url() . 'public/images/uploads/memberprofile/' . $photo;
				$this->response($response,200);
				
			}
		
		}
		
	}
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Route extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->appmodel->set_table('member_route');
		$this->pk = 'route_id';
	}
	
	function index_get($id){
		//fetch records
		$result = $this->appmodel->fetch_rows(NULL,array($this->pk => $id));
		$this->response(array('status' => 'OK', 'message' => 'Record found', 'data' => $result), 200);
	}
	
	function index_post(){
		//create new record
		
	}
	
	function index_put(){
		//update record
		
		
		
	}
	
	function index_delete(){}
	
}
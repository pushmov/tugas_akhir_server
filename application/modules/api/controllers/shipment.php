<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . 'modules/api/controllers/feedback.php');

class Shipment extends REST_Controller
{
	
	const ITEM_PER_PAGE = 10;
	const COUNTRY = 'ID';

	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';
	
	function __construct(){
		parent::__construct();
		$this->load->model('Api_shipment_model');
		$this->Api_shipment_model->set_table('shipment');
	}

	public function curl_post_async($url, $params){
			foreach ($params as $key => &$val) {
				if (is_array($val)) $val = implode(',', $val);
					$post_params[] = $key.'='.urlencode($val);
			}
			$post_string = implode('&', $post_params);

			$parts=parse_url($url);
			$fp = fsockopen($parts['host'],
					isset($parts['port'])?$parts['port']:80,
					$errno, $errstr, 30);

			$out = "POST ".$parts['path']." HTTP/1.1\r\n";
			$out.= "Host: ".$parts['host']."\r\n";
			$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
			$out.= "Content-Length: ".strlen($post_string)."\r\n";
			$out.= "Connection: Close\r\n\r\n";
			if (isset($post_string)) $out.= $post_string;

			fwrite($fp, $out);
			fclose($fp);
	}
	
	function index_get(){}

	function _get_num_category($name){

		$this->Api_shipment_model->set_table('shipment_type');
		$row = $this->Api_shipment_model->fetch_row(NULL,array('shipment_type_name' => $name));
		return !empty($row) ? 
			$row->type :
			7;
	}

	function _position($params){
		$position = array();


		/** latlng position */
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($params['address'])."&components=country:".$params['country']."|city=".$params['city']."|postal_code:".$params['postal_code']."&sensor=false";
		$data = @file_get_contents($url);
		$jsondata = json_decode($data,true);

		if($jsondata['status'] != 'OK')
		{
			
			$position['latitude'] = '0.000000';
			$position['longitude'] = '0.000000';

		} else {

			$location = $jsondata['results'][0]['geometry']['location'];
			$position['latitude'] = $location['lat'];
			$position['longitude'] = $location['lng'];

		}

		return $position;
	}

	function _direction($params){

		$return = array();
		
		/** distance in meters */
		$origins = $params['pickup']['address'] . ',' . $params['pickup']['city'] . ',' . ','. $params['pickup']['postal_code'] .', '. $params['pickup']['country'];
		$destinations = $params['deliver']['address'] . ',' . $params['deliver']['city'] . ',' . ','. $params['deliver']['postal_code'] .','. $params['deliver']['country'];
		
		//Jalan+Soekarno+Hatta,+Bojongloa+Kaler,+,Bandung,+40233,+ID
		//Jalan+Sekelimus+Utara,+Bandung,+Bandung,+40266,+ID
		
		$json = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode($origins)."&destinations=".urlencode($destinations)."&language=en-EN&sensor=false");
		$data = json_decode($json,true);

		if($data['status'] == 'OK'){

			$distance = $data['rows'][0]['elements'][0]['distance']['value'];
			$duration = $data['rows'][0]['elements'][0]['duration']['value'];
			
			$return['distance'] = ($distance / 1000);  //divide by 1000 (meters to km)
			$return['duration'] = $duration;  //in minutes, unused;

		} else {

			$return['distance'] = 0;
			$return['duration'] = 0;

		}

		return $return;
	}
	
	function index_post(){
			
			// populate mandatory post data 
			$email = $this->input->post('email');
			$member = array(
				'email' => $email,
				'username' => strstr($this->input->post('email'), '@', true),
				'password' => $this->input->post('password'),
				'phone' => $this->input->post('phone'),
				'ip_address' => $this->input->ip_address(),
				'is_driver' => 0,
				'activation_status' => 1,
				'activation_code' => sha1($email),
				'subscribe_newsletter' => $this->input->post('subscribe_newsletter'),
				'subscribe_push_notification' => $this->input->post('subscribe_push_notification')
			);
			
			$listing_duration = ($this->input->post('listing_duration')) ? trim(str_replace("days", "", $this->input->post('listing_duration'))) : 90;
			//step1 : register shipper
			$this->Api_shipment_model->set_table('member');
			$memberid = $this->Api_shipment_model->insert($member);
			
			$shipment_name = $this->input->post('name');
			$shipment_type = $this->input->post('type');
			$shipment_detail = $this->input->post('detail');
			
			$pickup_addr = $this->input->post('pickup_addr');
			$pickup_city = $this->input->post('pickup_city');
			$pickup_prov_code = $this->input->post('pickup_prov_code');
			
			$this->Api_shipment_model->set_table('province_code');
			
			$row_code = $this->Api_shipment_model->fetch_row(NULL,array('province_name_en' => $pickup_prov_code));
			$pickup_prov_code = $row_code->province_code;
			
			$pickup_postal_code = $this->input->post('pickup_postal_code');
			$pickup_max_date = date('Y-m-d H:i:s',strtotime("+$listing_duration days"));
			
			$deliver_addr = $this->input->post('deliver_addr');
			$deliver_city = $this->input->post('deliver_city');
			$deliver_prov_code = $this->input->post('deliver_prov_code');
			
			$row_code = $this->Api_shipment_model->fetch_row(NULL,array('province_name_en' => $deliver_prov_code));
			$deliver_prov_code = $row_code->province_code;
			
			$deliver_postal_code = $this->input->post('deliver_postal_code');
			$deliver_max_date = date('Y-m-d H:i:s',strtotime("+$listing_duration days"));
			
			
			$price_offered = $this->input->post('price_offered');
			//geolocation

			$pickup_pos = $this->_position(
					array(
						'address' => $this->input->post('pickup_addr'),
						'country' => self::COUNTRY,
						'city' => $this->input->post('pickup_city'),
						'postal_code' => $this->input->post('pickup_postal_code')
					)
				);
			$deliver_pos = $this->_position(
					array(
						'address' => $this->input->post('deliver_addr'),
						'country' => self::COUNTRY,
						'city' => $this->input->post('deliver_city'),
						'postal_code' => $this->input->post('deliver_postal_code')
					)
				);

			$distance = $this->_direction(array(
				'pickup' => array(
					'address' => $this->input->post('pickup_addr'),
					'city' => $this->input->post('pickup_city'),
					'postal_code' => $this->input->post('pickup_postal_code'),
					'country' => self::COUNTRY
				),
				'deliver' => array(
					'address' => $this->input->post('deliver_addr'),
					'city' => $this->input->post('deliver_city'),
					'postal_code' => $this->input->post('deliver_postal_code'),
					'country' => self::COUNTRY
				)
			));

			$fields = array(
				'member_id' => $memberid,
				'name' => $shipment_name,
				'type' => $this->_get_num_category($shipment_type),
				'detail' => $shipment_detail,
				'pickup_address' => $pickup_addr,
				'pickup_city' => $pickup_city,
				'pickup_province_code' => $pickup_prov_code,
				'pickup_zip_code' => $pickup_postal_code,
				'pickup_max_date' => $pickup_max_date,
				'pickup_latitude' => $pickup_pos['latitude'],
				'pickup_longitude' => $pickup_pos['longitude'],
				'deliver_address' => $deliver_addr,
				'deliver_city' => $deliver_city,
				'deliver_province_code' => $deliver_prov_code,
				'deliver_zip_code' => $deliver_postal_code,
				'deliver_max_date' => $deliver_max_date,
				'deliver_latitude' => $deliver_pos['latitude'],
				'deliver_longitude' => $deliver_pos['longitude'],
				'price_offered' => $price_offered,
				'total_distance' => $distance['distance']
			);
			$this->Api_shipment_model->set_table('shipment');
			$shipmentid = $this->Api_shipment_model->insert($fields);
			$response['success'] = 'success';
			$response['shipmentid'] = $shipmentid;


			//call the curl
			$url = base_url() . 'api/notification/';
			$data['shipmentid'] = $shipmentid;
			$data['memberid'] = $memberid;
			$this->curl_post_async($url, $data);

			echo json_encode($response);
			exit();
	}
	
	function index_put($shipmentid){
		
		$response = array();
		
		$category = $this->_get_num_category($this->input->put('type'));
		
		$this->Api_shipment_model->set_table('province_code');
		$row_code = $this->Api_shipment_model->fetch_row(NULL,array('province_name' => $this->input->put('pickup_province')));
		$pickup_prov_code = $row_code->province_code;
		
		$row_code = $this->Api_shipment_model->fetch_row(NULL,array('province_name' => $this->input->put('deliver_province')));
		$deliver_prov_code = $row_code->province_code;
		
		$this->Api_shipment_model->set_table('shipment');
		$row = $this->Api_shipment_model->fetch_row(NULL, array('shipment_id' => $shipmentid));
		
		if(empty($row)){
			
			$response['success'] = 'fail';
			$response['message'] = 'Record not found';
			
		}
		
			$pickup_pos = $this->_position(
					array(
						'address' => $this->input->put('pickup_address'),
						'country' => self::COUNTRY,
						'city' => $this->input->put('pickup_city'),
						'postal_code' => $this->input->put('pickup_postal_code')
					)
				);
			$deliver_pos = $this->_position(
					array(
						'address' => $this->input->put('deliver_addr'),
						'country' => self::COUNTRY,
						'city' => $this->input->put('deliver_city'),
						'postal_code' => $this->input->put('deliver_postal_code')
					)
				);

			$distance = $this->_direction(array(
				'pickup' => array(
					'address' => $this->input->put('pickup_address'),
					'city' => $this->input->put('pickup_city'),
					'postal_code' => $this->input->put('pickup_postal_code'),
					'country' => self::COUNTRY
				),
				'deliver' => array(
					'address' => $this->input->put('deliver_addr'),
					'city' => $this->input->put('deliver_city'),
					'postal_code' => $this->input->put('deliver_postal_code'),
					'country' => self::COUNTRY
				)
			));
		
		$request = array(
			'name' => $this->input->put('shipment_name'),
			'type' => $category,
			'detail' => $this->input->put('shipment_detail'),
			'pickup_address' => $this->input->put('pickup_address'),
			'pickup_city' => $this->input->put('pickup_city'),
			'pickup_province_code' => $pickup_prov_code,
			'pickup_zip_code' => $this->input->put('pickup_postal_code'),
			'pickup_latitude' => $pickup_pos['latitude'],
			'pickup_longitude' => $pickup_pos['longitude'],
			'deliver_address' => $this->input->put('deliver_address'),
			'deliver_city' => $this->input->put('deliver_city'),
			'deliver_province_code' => $deliver_prov_code,
			'deliver_zip_code' => $this->input->put('deliver_postal_code'),
			'deliver_latitude' => $deliver_pos['latitude'],
			'deliver_longitude' => $deliver_pos['longitude'],
			'total_distance' => $distance['distance'],
			'date_updated' => date('Y-m-d H:i:s')
		);
		
		
		$this->Api_shipment_model->update($request, array('shipment_id' => $shipmentid));
		
		$response['success'] = 'success';
		$response['message'] = 'Shipment Updated';
		
		$response['UID'] = $row->member_id;
		
		$this->response($response, 200);
		
	}
	
	function index_delete($shipmentid){
		$response = array();
		
		$this->Api_shipment_model->set_table('shipment');
		$row = $this->Api_shipment_model->fetch_row(NULL, array('shipment_id' => $shipmentid));
		
		if(empty($row)){
			$response['success'] = 'fail';
			$response['message'] = 'Record not found';
			$this->response($response, 200);
		}
		
		$this->Api_shipment_model->delete(array('shipment_id' => $shipmentid));
		
		//delete notification table, keep the acc status for the analytic and feedback 
		$this->Api_shipment_model->set_table('notification');
		$this->Api_shipment_model->delete(array('shipment_id' => $shipmentid, 'status <>' => 'ACC'));
		
		//unlink the shipment picture
		$this->Api_shipment_model->set_table('shipment_picture');
		$pics = $this->Api_shipment_model->fetch_rows(NULL, array('shipment_id' => $shipmentid));
		foreach($pics as $p){
		
			unlink('./public/images/uploads/shipmentpicture/' . $p->shipment_picture);
			$this->Api_shipment_model->delete(array('shipment_id' => $p->picture_id));
		
		}
		
		$response['success'] = 'success';
		$response['message'] = 'Shipmet Deleted';
		$response['UID'] = $row->member_id;
		$this->response($response, 200);
	}
	
	function total_get(){
			$model_params = array(
					'select' => "shipment.shipment_id,shipment.name,shipment.detail,shipment.date_added,shipment_type.shipment_type_name,shipment_type.icon_name",
					'tablejoin' => "shipment_type",
					'where2' => "shipment.type = shipment_type.type AND shipment.is_accept_driver = 0",
					'order_by' => "shipment.date_added ASC"
			);
			
			$all_shipments = $this->Api_shipment_model->multi_table_rows($model_params);
			$total = sizeof($all_shipments);
			$response = array('total' => (string) $total);
			$this->response($response,200);
	}
	
	function all_get($page=0){
			
			$model_params = array(
					'select' => "shipment.shipment_id,shipment.name,shipment.detail,shipment.date_added,shipment_type.shipment_type_name,shipment_type.icon_name",
					'tablejoin' => "shipment_type",
					'where2' => "shipment.type = shipment_type.type AND shipment.pickup_max_date >= NOW() AND shipment.is_accept_driver = 0",
					'order_by' => "shipment.date_added DESC"
			);
			
			
			$response = array();
			
			$all_shipments = $this->Api_shipment_model->multi_table_rows($model_params);
			
			$this->load->library('pagination');
			$config['base_url'] = base_url() . 'api/shipment/all/';
			$config['total_rows'] = count($all_shipments);
			$config['per_page'] = self::ITEM_PER_PAGE;
			$config['use_page_numbers'] = TRUE;
			
			$limit = ($page > 0) ? self::ITEM_PER_PAGE * ($page) : self::ITEM_PER_PAGE;
			
			$result = $this->Api_shipment_model->multi_table_rows($model_params,$limit,0);
			
			$this->Api_shipment_model->set_table('shipment_picture');
			foreach($result as $row){
					$row->date_added = date('M/d/Y',strtotime($row->date_added));
					
					
					$get_pic = $this->Api_shipment_model->fetch_row(NULL, array('shipment_id' => $row->shipment_id));
					if(empty($get_pic)){
						
						$icon_path = 'public/images/icon/' . $row->icon_name;
						
					} else {
					
						$icon_path = 'public/images/uploads/shipmentpicture/' . $get_pic->shipment_picture;
					}
					
					$row->icon_path = stripslashes($icon_path);
			}
			
			$this->response($result, 200);
	}
	
	function detail_get($id=NULL){
			
			$response = array();
			if(!isset($id)){
					$this->response(array('flag' => 'notfound'), 200);
					exit();
			}
			
			$result = $this->Api_shipment_model->fetch_row(NULL,array('shipment_id' => $id));
			
			if(empty($result)){
					$this->response(array('flag' => 'notfound'), 200);
					exit();
			}
			
			$this->Api_shipment_model->set_table('shipment_type');
			$get_category = $this->Api_shipment_model->fetch_row('shipment_type_name,icon_name',array('type' => $result->type));
			
			//additional data
			$result->shipment_category = $get_category->shipment_type_name;
			$result->shipment_icon = $get_category->icon_name;
			$result->expired_date = date('M/d/Y',strtotime($result->deliver_max_date));
			
			
			$this->Api_shipment_model->set_table('shipment_picture');
			$row_picture = $this->Api_shipment_model->get_picture($id);
			$result->picture = (empty($row_picture)) ? base_url() . 'public/images/package.png' : base_url() . 'public/uploads/shipmentpicture/' . $row_picture->shipment_picture;
			
			$this->Api_shipment_model->set_table('province_code');
			$pickup_province_name = $this->Api_shipment_model->fetch_row('country_name,province_name',array('province_code' => $result->pickup_province_code));
			$deliver_province_name = $this->Api_shipment_model->fetch_row('country_name,province_name',array('province_code' => $result->deliver_province_code));
			
			$result->pickup_province_name = $pickup_province_name->province_name;
			$result->deliver_province_name = $deliver_province_name->province_name;
			$result->country = $pickup_province_name->country_name;
			
			$result->human_date_added = date('d/M/Y', strtotime($result->date_added));
			
			$result->listing_duration = round((strtotime($result->pickup_max_date) - strtotime($result->date_added)) / (24*60*60));
			
			
			//fetch picture
			$this->Api_shipment_model->set_table('shipment_picture');
			$row_pic = $this->Api_shipment_model->fetch_row(NULL, array('shipment_id' => $result->shipment_id));
			$result->picture = (empty($row_pic)) ? base_url() . 'public/images/package.png' : base_url() . 'public/images/uploads/shipmentpicture/' . $row_pic->shipment_picture;
			
			$result->expired_date = date('d/M/Y', strtotime($result->pickup_max_date));
			
			$result->from_address = $result->pickup_city . ', ' . $result->pickup_zip_code;
			$result->to_address = $result->deliver_city . ', ' . $result->deliver_zip_code;
			
			$get_bidders = $this->Api_shipment_model->get_bidder_list(array('shipment_id' => $id, 'status <> ' => 'O'));
			
			$feedback = new Feedback();
			
			foreach($get_bidders as $bidder){
				
				$path = base_url() . 'public/images/uploads/memberprofile/';
				$bidder->profile = ($bidder->profpic == '') ? $path . 'anonymous.png' :  $path . $bidder->profpic;
				$bidder->human_price = 'Rp. ' . number_format($bidder->bid_price, 2);
				$bidder->rating_value = $feedback->calculate_rating($bidder->member_id);
				
			}
			
			$response['success'] = 'success';
			$response['result'] = $result;
			$response['bid_status'] = (empty($get_bidders)) ? 'EMPTY' : 'OK';
			$response['bidders'] = $get_bidders;
			
			$this->response($response, 200);
			
	}
	
	function category_get(){
			
			$this->Api_shipment_model->set_table('shipment_type');
			$result = $this->Api_shipment_model->fetch_rows();
			
			$this->response($result,200);
	}
	
	function geocoding_get($lat=NULL,$lon=NULL){
			$response = array();
			if(!isset($lat) || !isset($lon)){
					$response['results'] = 'ZERO_RESULT';
					$this->response($response,200);
					exit();
			}
			
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lon.'&sensor=false';
			$data = @file_get_contents($url);
			
			$jsondata = json_decode($data,true);
			$response['street_number'] = '';
			$response['route'] = '';
			$response['locality'] = '';
			$response['administrative_area_level_3'] = '';
			$response['administrative_area_level_2'] = '';
			$response['administrative_area_level_1'] = '';
			$response['country'] = '';
			$response['postal_code'] = '';
			if($jsondata['status'] == 'OK'){
					
					$response['results'] = $jsondata['status'];
					
					foreach($jsondata['results'][0]['address_components'] as $arr_type){
							$response[$arr_type['types'][0]] = $arr_type['long_name'];
					}
					
					$this->response($response, 200);
					exit();
			} elseif($jsondata['status'] == 'ZERO_RESULTS'){
					$response['results'] = 'ZERO_RESULT';
					$this->response($response,200);
					exit();
			}
			
	}

	function gallery_post($shipmentid){

		$response = array();

		$request = array();
		$request['photo'] = (!empty($_FILES['picture']['tmp_name'])) ? time() .'-'.$shipmentid : '';
		$photo = $request['photo'];

		/** upload configuration */
		$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
		$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
		$config['upload_path'] = './public/images/uploads/shipmentpicture/';

		if($photo != '')
		{
			$config['file_name'] = $photo;
			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('picture'))
			{
				$response['status'] = 'ERROR';
				//$response['message'] = $photo . ' - max size : 5MB. File type allowed : ' . self::MAX_ALLOWED_SHIPMENT_PICTURE;
				$response['message'] = $this->upload->display_errors('', '');
				$this->response($response,200);
				exit();
			}
			else
			{
				$data = $this->upload->data();
				$photo = $data['orig_name'];

				$this->Api_shipment_model->set_table('shipment_picture');
				$this->Api_shipment_model->insert(array('shipment_id' => $shipmentid, 'shipment_picture' => $photo));
				
				$this->load->library('image_lib');
				$config['image_library'] = 'gd2';
				$config['source_image'] = './public/images/uploads/shipmentpicture/'.$photo;
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = FALSE;
				$config['width']     = 40;
				$config['height']   = 41;

				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

				$response['status'] = 'OK';
				$response['message'] = 'Success. File uploaded to server';
				$this->response($response, 200);
				exit();
			}
		}


	}
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH . 'modules/api/controllers/shipment.php');


class Shipper extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_shipper_model');
	}
	
	function index_get($shipmentid){
	
		$response = array();
		
		//shipper shipment bid
		$this->Api_shipper_model->set_table('shipment');
		$row_shipment = $this->Api_shipper_model->fetch_row(NULL, array('shipment_id' => $shipmentid));
		
		if(empty($row_shipment)){
			$response['success'] = 'fail';
			$response['message'] = 'Record not found';
			$this->response($response,200);
		}
		
		$this->Api_shipper_model->set_table('shipment_picture');
		$rowpic = $this->Api_shipper_model->fetch_row('shipment_picture', array('shipment_id' => $shipmentid));
		
		$row_shipment->picture = (empty($rowpic)) ? base_url() . 'public/images/package.png' : base_url() . 'public/images/uploads/shipmentpicture/' . $rowpic->shipment_picture;
		$response['shipment'] = $row_shipment;
		
		
		//get bidder list
		$this->Api_shipper_model->set_table('notification');
		$bidders = $this->Api_shipper_model->fetch_rows(NULL, array('shipment_id' => $shipmentid, 'status' => 'A'));
		
		$feedback = new Feedback();
		
		
		$this->Api_shipper_model->set_table('member');
		foreach($bidders as $b){
		
			$m = $this->Api_shipper_model->fetch_row('member_id, username, profpic', array('member_id' => $b->member_id));
			$b->member_id = $m->member_id;
			$b->username = $m->username;
			$b->profpic = ($m->profpic != '') ? base_url() . 'public/images/uploads/memberprofile/' . $m->profpic : base_url() . 'public/images/uploads/memberprofile/anonymous.png';
			$b->price = 'Rp. ' . number_format($b->bid_price, 2);
			$b->human_bid_date = date('d/M/Y',strtotime($b->bid_submit_date));
			
			$b->rating = $feedback->calculate_rating($b->member_id);
		}
		
		$response['bidders'] = $bidders;
		$response['success'] = 'success';
		$response['message'] = 'success';
		$response['total_bid'] = sizeof($bidders);
		
		$this->response($response, 200);
	
	}
	
	function index_post(){}
	
	function index_put(){}
	
	function index_delete(){}
	
	function myshipment_get($id){
		
		$this->Api_shipper_model->set_table('shipment');
		
		$select = 'shipment_id,date_added,name,detail, is_accept_driver';
		$result = $this->Api_shipper_model->fetch_rows($select, array('member_id' => $id, "pickup_max_date < " => 'NOW()'), NULL, NULL, "date_added DESC");
		
		$new_result = array();
		foreach($result as $row){
			$this->Api_shipper_model->set_table('notification');
			$check_acc = $this->Api_shipper_model->fetch_row(NULL, array('shipment_id' => $row->shipment_id, 'status' => 'ACC', 'status' => 'C', 'status' => 'F'));
			if(!empty($check_acc)) continue;
			
			$string = strip_tags($row->detail);
			if (strlen($string) > 100) {
				$stringCut = substr($string, 0, 100);
				$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
			}
			
			$row->small_description = $string;
			$row->human_date = date('d/M/Y', strtotime($row->date_added));
			
			//count current bid from notification table, where A=applied, ACC=accepted, R=rejected, O=open
			$row->total_bids = $this->Api_shipper_model->count('notification', array('status' => 'A', 'shipment_id' => $row->shipment_id));
			
			$new_result[] = $row;
			
			$confirmation = $this->Api_shipper_model->check_confirmation($row->shipment_id);
			$row->is_confirmation = (empty($confirmation)) ? '' : $confirmation->invoice_number;
			
		}
		
		$this->response(
			array('status' => 'OK', 'result' => $new_result)
		);
		
	}
	
	function newshipment_post(){
		
		$response = array();
		
		if(!$this->input->post('memberid')){
			$response['success'] = 'fail';
			$response['message'] = 'Member not found';
			$this->response($response, 200);
		}
		
		$member_id = $this->input->post('memberid');
		
		$shipment_obj = new Shipment();
		$category = $shipment_obj->_get_num_category($this->input->post('type'));
		
		$this->Api_shipment_model->set_table('province_code');
		$row_code = $this->Api_shipment_model->fetch_row(NULL,array('province_name' => $this->input->post('pickup_province')));
		$pickup_prov_code = $row_code->province_code;
		
		$row_code = $this->Api_shipment_model->fetch_row(NULL,array('province_name' => $this->input->post('deliver_province')));
		$deliver_prov_code = $row_code->province_code;
		
		
			$pickup_pos = $shipment_obj->_position(
					array(
						'address' => $this->input->post('pickup_address'),
						'country' => Shipment::COUNTRY,
						'city' => $this->input->post('pickup_city'),
						'postal_code' => $this->input->post('pickup_postal_code')
					)
				);
			$deliver_pos = $shipment_obj->_position(
					array(
						'address' => $this->input->post('deliver_addr'),
						'country' => Shipment::COUNTRY,
						'city' => $this->input->post('deliver_city'),
						'postal_code' => $this->input->post('deliver_postal_code')
					)
				);

			$distance = $shipment_obj->_direction(array(
				'pickup' => array(
					'address' => $this->input->post('pickup_address'),
					'city' => $this->input->post('pickup_city'),
					'postal_code' => $this->input->post('pickup_postal_code'),
					'country' => Shipment::COUNTRY
				),
				'deliver' => array(
					'address' => $this->input->post('deliver_addr'),
					'city' => $this->input->post('deliver_city'),
					'postal_code' => $this->input->post('deliver_postal_code'),
					'country' => Shipment::COUNTRY
				)
			));
			
		$listing_duration = ($this->input->post('listing_duration')) ? trim(str_replace("days", "", $this->input->post('listing_duration'))) : 90;
		
		$request = array(
			'member_id' => $member_id,
			'name' => $this->input->post('shipment_name'),
			'type' => $category,
			'detail' => $this->input->post('shipment_detail'),
			'pickup_address' => $this->input->post('pickup_address'),
			'pickup_city' => $this->input->post('pickup_city'),
			'pickup_province_code' => $pickup_prov_code,
			'pickup_zip_code' => $this->input->post('pickup_postal_code'),
			'pickup_latitude' => $pickup_pos['latitude'],
			'pickup_longitude' => $pickup_pos['longitude'],
			'pickup_max_date' => date('Y-m-d H:i:s', strtotime("+$listing_duration days")),
			'deliver_address' => $this->input->post('deliver_address'),
			'deliver_city' => $this->input->post('deliver_city'),
			'deliver_province_code' => $deliver_prov_code,
			'deliver_zip_code' => $this->input->post('deliver_postal_code'),
			'deliver_latitude' => $deliver_pos['latitude'],
			'deliver_longitude' => $deliver_pos['longitude'],
			'deliver_max_date' => date('Y-m-d H:i:s', strtotime("+$listing_duration days")),
			'total_distance' => $distance['distance'],
		);
		$this->Api_shipper_model->set_table('shipment');
		$insertid = $this->Api_shipper_model->insert($request);
		
		$response['shipment_id'] = $insertid;
		$response['member_id'] = $member_id;
		$response['success'] = 'success';
		$response['message'] = 'Data Saved';
		$this->response($response, 200);
		
		
	}
	
	function accept_put($notificationid){
		
		$this->Api_shipper_model->set_table('notification');
		$row_notification = $this->Api_shipper_model->fetch_row(NULL, array('notification_id' => $notificationid));
		
		$shipment_id = $row_notification->shipment_id;
		
		// $update_1 = $this->Api_shipper_model->update(array('status' => 'ACC'), array('notification_id' => $notificationid));
		
		//create invoice record for admin
		
		
		$this->Api_shipper_model->set_table('shipment');
		$row_shipment = $this->Api_shipper_model->fetch_row(NULL, array('shipment_id' => $shipment_id));
		
		$this->Api_shipper_model->create_invoice($shipment_id, $row_shipment->member_id, $row_notification->member_id);
		
		$this->Api_shipper_model->update_shipment($shipment_id);
		
		//reject remaining bids, close shipment
		$update_2 = $this->Api_shipper_model->reject_remaining($shipment_id);
		
		$update_1 = $this->Api_shipper_model->acc_notification($notificationid);
		
		//delete open bids
		$this->Api_shipper_model->set_table('notification');
		$this->Api_shipper_model->delete(array('status' => 'O', 'shipment_id' => $shipment_id));
		
		if($update_2){
			$response['success'] = 'success';
			$response['message'] = 'Data Updated';
			$response['shipperid'] = $row_shipment->member_id;
			$this->response($response, 200);
		} else {
			
			$response['success'] = 'failed';
			$response['message'] = 'Data Not Updated';
			
			$this->response($response, 200);
			
		}
		
	}
	
	function reject_put($notificationid){
	
		$this->Api_shipper_model->set_table('notification');
		$update = $this->Api_shipper_model->update(array('status' => 'R'), array('notification_id' => $notificationid));
		
		$row_notif = $this->Api_shipper_model->fetch_row(NULL, array('notification_id' => $notificationid));
		
		$this->Api_shipper_model->set_table('shipment');
		$row_shipment = $this->Api_shipper_model->fetch_row(NULL, array('shipment_id' => $row_notif->shipment_id));
		
		if($update){
			$response['success'] = 'success';
			$response['message'] = 'Data Updated';
			$response['shipperid'] = $row_shipment->member_id;
			
			$this->response($response, 200);
		} else {
			$response['success'] = 'failed';
			$response['message'] = 'Data Not Updated';
			
			$this->response($response, 200);
		}
	}
	
	function currentcontracts_get($shipperid, $param_status = 'C'){
		
		$results = $this->Api_shipper_model->get_current_contracts($shipperid, $param_status);
		
		if(!empty($results))
		{
			foreach($results as $c){
				
				$c->bid = "Rp. " . number_format($c->bid_price, 2);
				
				$this->Api_shipper_model->set_table('shipment_picture');
				$picture = $this->Api_shipper_model->fetch_row('shipment_picture', array('shipment_id' => $c->shipment_id));
				
				$this->Api_shipper_model->set_table('shipment_type');
				$icon = $this->Api_shipper_model->fetch_row('icon_name', array('type' => $c->type));
				
				$c->picture = (empty($picture)) ? base_url() . 'public/images/icon/' . $icon->icon_name : base_url() . 'public/images/uploads/shipmentpicture/' . $picture->shipment_picture;
				
				$c->notification_date = "";
				
				$status_payment = $this->Api_shipper_model->get_status_payment($c->shipment_id, $shipperid);
				$c->status_payment = (empty($status_payment)) ? 'N' : 'Y';
				
				$status_paid = $this->Api_shipper_model->get_status_paid($c->shipment_id, $shipperid);
				$c->status_paid = ($status_paid->invoice_date_paid == '') ? 'N' : 'Y';
				$c->status_rejected = ($status_paid->invoice_status == 'R') ? 'Y' : 'N';
				$c->status_rejected_msg = $status_paid->status_message;
				
				$c->is_driver_report = ($c->driver_report == '') ? 'N' : 'Y';
				
				$c->is_shipper_report = ($c->shipper_report == '') ? 'N' : 'Y';
				
			}
		}
		
		$this->response(array('success' => 'success', 'contracts' => $results), 200);
		
	}
	
	function report_put($notification_id){
		
		$report = $this->input->put('shipper_report');
		
		$update_data = array(
			'is_shipper_accept_report' => 1,
			'status' => 'F',
			'shipper_report' => $report,
			'shipper_report_date' => date('Y-m-d H:i:s')
		);
		
		$this->Api_shipper_model->set_table('notification');
		$row_notification = $this->Api_shipper_model->fetch_row(NULL, array('notification_id' => $notification_id));
		
		$this->Api_shipper_model->set_table('shipment');
		$row_shipment = $this->Api_shipper_model->fetch_row(NULL, array('shipment_id' => $row_notification->shipment_id));
		
		$this->Api_shipper_model->set_table('notification');
		$this->Api_shipper_model->update($update_data, array('notification_id' => $notification_id));
		
		$this->response(array('success' => 'success', 'UID' => $row_shipment->member_id), 200);
		
	}
	
}
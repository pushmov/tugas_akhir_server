<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_user_model');
		$this->Api_user_model->set_table('member');
	}
	
	function index_get($id=NULL){
		if(isset($id)){
			
			//get single id
			$result = $this->Api_user_model->fetch_row(NULL,array('member_id' => $id));
			if(empty($result)){
				$this->response(array('status' => 'failed', 'message' => 'record not found'), 200);
			}
			
			$this->response(array('status' => 'OK', 'message' => 'record found', 'data' => $result), 200);
			
		} else {
		
			//get all records
			$result = $this->Api_user_model->fetch_rows();
			$this->response(array('status' => 'OK', 'message' => 'record found', 'data' => $result), 200);
			
		}
		
	}
	
	function checkemail_get($email=NULL){
		
			$response = array();
			$response['response'] = 'true';
			
			if(!isset($email)){
				$response['response'] = 'false';
				$this->response($response, 200);
				exit();
			}
			
			$email = urldecode($email);
			$result = $this->Api_user_model->fetch_row(NULL, array('email' => $email));
			if(!empty($result))
			{
				$response['response'] = 'false';
				$this->response($response, 200);
				exit();
			}
			
			$this->response($response, 200);
			
	}
	
	function index_post(){
		
		//create user
		
		
	}
	
	function index_put(){}
	
	function index_delete(){}
	
}
<?php

class Api_dashboard_model extends AppModel {
	
	
	
	function shipper_active_shipment($userid){
		
		return $this->db->where(array("shipment.member_id" => $userid))
			->where("pickup_max_date < ", 'NOW()')
			->join("notification", "shipment.shipment_id = notification.shipment_id AND notification.status <> 'R' AND notification.status <> 'C' AND notification.status <> 'ACC' AND notification.status <> 'F'")
			->group_by("shipment.shipment_id")
			->get('shipment')
			->result();
		
	}
	
	function shipper_finished_contracts($userid){
		return $this->db->where(array("shipment.member_id" => $userid))
			->where("pickup_max_date < ", 'NOW()')
			->join("notification", "shipment.shipment_id = notification.shipment_id AND notification.status = 'F'")
			->group_by("shipment.shipment_id")
			->get('shipment')
			->result();
	}
	
	function my_shipments($shipperid){
		return $this->db->where('member_id', $shipperid)
			->where("pickup_max_date < NOW()")
			->join('notification', "notification.shipment_id = shipment.shipment_id AND notification.status <> 'C' AND notification.status <> 'ACC'")
			->order_by("date_added DESC")
			->get('shipment')
			->result();
			
	}
	
	function new_message($userid){
		
		return $this->db->where(array('member_id' => $userid, 'recipient_keep' => 1))
			->where('recipient_read_date IS NULL', NULL, FALSE)
			->get('message_recipient')
			->result();
		
	}
	
	function my_feedback($userid){
	
		return $this->db->where(array('recipient_id' => $userid))
			->get('feedback')
			->result();
		
	}
	
	function shipper_current_contracts($userid){
		
		return $this->db->where(array("shipment.member_id" => $userid))
			->join("notification", "shipment.shipment_id = notification.shipment_id AND (notification.status = 'C' OR notification.status = 'ACC')")
			->from('shipment')
			->count_all_results();
	
	}
	
	function driver_new_jobs_available($userid){
		return $this->db->where(array('member_id' => $userid))
			->where("notification_read_date IS NULL", NULL, FALSE)
			->group_by("shipment_id")
			->get('notification')
			->result();
	}
	
	function driver_active_bids($userid){
		return $this->db->select('notification.bid_price, notification.bid_submit_date, shipment.name, shipment.pickup_city, shipment.deliver_city, shipment.type, shipment.shipment_id, notification.notification_id')
			->where(array('notification.member_id' => $userid, 'status' => 'A'))
			->join('shipment', 'shipment.shipment_id = notification.shipment_id AND shipment.pickup_max_date >= NOW()')
			->get('notification')
			->result();
	}
	
	function driver_current_jobs($userid){
		return $this->db->where(array('member_id' => $userid, 'status' => 'ACC'))
			->group_by("shipment_id")
			->get('notification')
			->result();
	}
	
	function driver_completed_jobs($userid){
		return $this->db->where(array('member_id' => $userid))
			->where("(status = 'F' OR status = 'C')")
			->group_by("shipment_id")
			->get('notification')
			->result();
	}
	
	function driver_earnings($userid){
		return $this->db->select("SUM(bid_price) AS earnings")
			->where(array('member_id' => $userid, 'status' => 'F'))
			->limit(1)
			->get('notification')
			->row();
	}
	
	
	
	
}
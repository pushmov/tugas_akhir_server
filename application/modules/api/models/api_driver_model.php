<?php

class Api_driver_model extends AppModel {
	
	function multi_table_rows($params, $limit = NULL, $offset = NULL){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}
	
	function get_notification_list($userid){
		
		
		return $this->db->select('notification.bid_price, notification.bid_submit_date, shipment.name, shipment.pickup_city, shipment.deliver_city, shipment.type, shipment.shipment_id, notification.notification_id')
			->where(array('notification.member_id' => $userid, 'status' => 'A'))
			->join('shipment', 'shipment.shipment_id = notification.shipment_id AND shipment.pickup_max_date >= NOW()')
			->get('notification')
			->result();
			
		
	}
	
	
	function get_current_jobs($userid){
		
		return $this->db->select('notification.bid_price, notification.bid_submit_date, shipment.name, shipment.pickup_city, shipment.deliver_city, shipment.type, shipment.shipment_id, notification.notification_id, shipment.deliver_max_date, shipper_invoice.is_accepted')
			->where(array('status' => 'ACC', 'notification.member_id' => $userid))
			->join('shipment', 'shipment.shipment_id = notification.shipment_id AND shipment.deliver_max_date >= NOW()')
			->join('shipper_invoice', 'shipper_invoice.shipment_id = shipment.shipment_id')
			->get('notification')
			->result();
	}
	
	function completed_jobs($id){
		return $this->db->select('notification.bid_price, notification.is_shipper_accept_report, notification.shipper_report, notification.shipper_report_date, notification.bid_submit_date, shipment.name, shipment.pickup_city, shipment.deliver_city, shipment.type, shipment.shipment_id, notification.notification_id, shipment.deliver_max_date, notification.driver_report_date')
			->where("(status = 'F' OR status = 'C')", NULL, FALSE)
			->where(array('notification.member_id' => $id))
			->join('shipment', 'shipment.shipment_id = notification.shipment_id')
			->order_by('driver_report_date DESC')
			->get('notification')
			->result();
	}
	
	function get_fund_status($shipmentid, $driverid){
		return $this->db->select('is_completed')
			->where(array('shipment_id' => $shipmentid, 'driver_id' => $driverid))
			->get('shipper_invoice')
			->row();
	}
	
	function get_state_name($statename){
		$resource = $this->db->select('province_code')
			->where('province_name', $statename)
			->row();
			
		return $resource->province_code;
	}
	
}
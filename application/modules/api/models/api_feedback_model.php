<?php

class Api_feedback_model extends AppModel {

	var $pk = 'feedback_id';

	function multi_table_rows($params, $limit = NULL, $offset = NULL){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}

	function fetch_all(){

			$this->db->order_by('date_added DESC');

			return $this->db->get($this->table)
					->result();
	}
	
	function my_feedbacks($id){
		
		return $this->db->select('shipment.name, shipment.type, feedback.*')
			->where('recipient_id', $id)
			->join('shipment', "shipment.shipment_id = feedback.shipment_id")
			->get('feedback')
			->result();
	
	}

}
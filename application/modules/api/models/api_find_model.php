<?php

class Api_find_model extends AppModel {

	
	function geo_search_pickup($lat, $lon, $radius, $limit, $earth_radius=6371){
		//3959 : earth radius
		//note, this is in kilometer
		
		
		
		return $this->db->query("
			SELECT shipment_id,name,type,detail,pickup_city,deliver_city,date_added, ( $earth_radius * ACOS( COS( RADIANS($lat) ) * COS( RADIANS( pickup_latitude ) ) 
				* COS( RADIANS( pickup_longitude ) - RADIANS($lon) ) + SIN( RADIANS($lat) ) * SIN(RADIANS(pickup_latitude)) ) ) AS distance 
				FROM shipment WHERE is_accept_driver = 0
				HAVING distance < $radius 
				ORDER BY distance ASC;
		")->result();
		
	}
	
	function geo_search_deliver($lat, $lon, $radius, $limit, $earth_radius=6371){
		//3959 : earth radius
		
		return $this->db->query("
			SELECT shipment_id,name,type,detail,pickup_city,deliver_city,date_added, ( $earth_radius * ACOS( COS( RADIANS($lat) ) * COS( RADIANS( deliver_latitude ) ) 
				* COS( RADIANS( deliver_longitude ) - RADIANS($lon) ) + SIN( RADIANS($lat) ) * SIN(RADIANS(deliver_latitude)) ) ) AS distance 
				FROM shipment WHERE is_accept_driver = 0
				HAVING distance < $radius 
				ORDER BY distance ASC;
		")->result();
		
	}

}
<?php

class Api_message_model extends AppModel {

	function multi_table_rows($params, $limit = NULL, $offset = NULL){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}
	
	function get_thread_lists($userid){
		return $this->db->select('member.username, message.*, message_recipient.*')
			->where(array('message_recipient.member_id' => $userid, 'message_recipient.recipient_keep' => 1))
			->join('message', "message.message_id = message_recipient.message_id")
			->join('member', "message.member_id = member.member_id")
			->get('message_recipient')
			->result();
	}
	
}
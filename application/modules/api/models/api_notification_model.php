<?php

class Api_notification_model extends AppModel {
	
	function multi_table_rows($params, $limit = NULL, $offset = NULL){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}
	
}
<?php

class Api_payment_model extends AppModel {
	
	function confirm_payment($shipmentid){
		return $this->db->where('shipper_invoice.shipment_id', $shipmentid)
			->join('member_profile', "member_profile.member_id = shipper_invoice.driver_id")
			->join('member', "member.member_id = shipper_invoice.driver_id")
			->join('shipment', "shipment.shipment_id = shipper_invoice.shipment_id")
			->join('notification', "notification.shipment_id = shipment.shipment_id AND notification.status = 'ACC'")
			->get('shipper_invoice')
			->row();
	}
	
	function update_method($shipmentid, $request){
		$this->db->where('shipment_id', $shipmentid)
			->update('shipper_invoice', $request);
	}
	
}
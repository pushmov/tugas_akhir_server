<?php

class Api_profile_model extends AppModel {
	
	function recent_shipment($id){
		
		return $this->db->select('shipment.shipment_id, shipment.name, shipment.type, notification.member_id, notification.shipper_report_date')
			->where(array('status' => 'F', 'notification.member_id' => $id))
			->order_by('notification.shipper_report_date', 'DESC')
			->join('shipment', "notification.shipment_id = shipment.shipment_id")
			->get('notification')
			->result();
	}
	
}
<?php

class Api_shipment_model extends AppModel {
	
	function multi_table_rows($params, $limit = NULL, $offset = NULL){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}
	
	function get_bidder_list($param){
		
		return $this->db->select('member.member_id, member.username, member.profpic, notification.notification_id, notification.status, notification.bid_price')
			->where($param)
			->join('member', "notification.member_id = member.member_id")
			->get('notification')
			->result();
		
	}
	
	function get_picture($shipment_id){
		return $this->db->where(array('shipment_id' => $shipment_id))
			->order_by('date_uploaded DESC')
			->get('shipment_picture')
			->row();
	}
	
	
	
}
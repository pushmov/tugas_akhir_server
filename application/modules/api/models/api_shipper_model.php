<?php

class Api_shipper_model extends AppModel {

	
	function reject_remaining($shipmentid){
		return $this->db->where("status <> 'O'", NULL, FALSE)
			->where("status <> 'ACC'", NULL, FALSE)
			->where(array("shipment_id" => $shipmentid))
			->update("notification", array('status' => 'R'));
	}
	
	function acc_notification($id){
		return $this->db->where("notification_id", $id)
			->set('status', 'ACC')
			->update('notification');
	}
	
	function get_current_contracts($shipperid, $param_status){
		$this->db->select('name, notification.shipment_id, notification.member_id, shipper_report,status, is_shipper_accept_report, driver_report, driver_report_date, bid_price, notification_id, type');
		
		if($param_status == 'C')
		{
			$this->db->where("shipment.member_id = '$shipperid' AND (notification.status = 'ACC' OR notification.status = 'C')", NULL, FALSE);
		}
		else
		{
			$this->db->where("shipment.member_id = '$shipperid' AND notification.status = 'F'", NULL, FALSE);
		}
		
		return $this->db->join('shipment', "shipment.shipment_id = notification.shipment_id")
			->get('notification')
			->result();
	}
	
	function create_invoice($shipmentid, $shipperid, $driverid){
		$data = array(
			'shipper_id' => $shipperid,
			'shipment_id' => $shipmentid,
			'driver_id' => $driverid,
			'invoice_number' => $this->generateRandomString()
		);
		return $this->db->insert('shipper_invoice', $data);
	}
	
	function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
	}
	
	function check_confirmation($shipmentid){
		return $this->db->where(array('shipment_id' => $shipmentid, 'is_accepted' => 1))
			->get('shipper_invoice')
			->row();
	}
	
	function update_shipment($shipment_id){
		return $this->db->where('shipment_id', $shipment_id)
			->update('shipment', array('is_accept_driver' => 1));
	}
	
	function get_status_payment($shipmentid, $shipperid){
		return $this->db->where(array('shipment_id' => $shipmentid, 'shipper_id' => $shipperid, 'is_accepted' => 1))
			->get('shipper_invoice')
			->row();
	}
	
	function get_status_paid($shipmentid, $shipperid){
		return $this->db->where(array('shipment_id' => $shipmentid, 'shipper_id' => $shipperid))
			->get('shipper_invoice')
			->row();
	}
	
}
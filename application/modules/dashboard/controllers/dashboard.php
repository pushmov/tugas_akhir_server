<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	
	var $base_view;

	public function __construct(){
		parent::__construct();
		
		$this->load->model('Appmodel');
		$this->load->model('dashboard/dashboard_model');

		$this->base_view = $this->config->item('base_view');
		
		if(!$this->auth->is_logged_in()){
			redirect('/login');
		}
	}
	
	public function index(){
		// Get member data
		$this->Appmodel->set_table('member');
		$where = array('member_id' => $this->auth->get_user_id());
		$member = $this->Appmodel->fetch_row(NULL, $where);

		$data = array('member' => $member,
					  'page_content' => 'dashboard',
			  		  'active_nav' => 'dashboard',
			  		  'extra_js' => array('public/js/dashboard.js'));

		$this->load->view($this->base_view, $data);
		
	}

	public function my_shipments()
	{
		$this->Appmodel->set_table('shipment');
		$where = array('member_id' => $this->auth->get_user_id());
		$select = 'shipment_id, name, detail, date_added';

		$shipments = $this->Appmodel->fetch_rows($select, $where);
		
		foreach($shipments as $key=>$s)
		{
			
			if(!$this->dashboard_model->get_status($s->shipment_id)) {
				unset($shipments[$key]);
			}
		}

		$data = array('shipments' => $shipments);

		$this->load->view('tab/my_shipments', $data);
	}

	public function my_bids()
	{
		$this->Appmodel->set_table('notification');

		$params = array('select' => '*',
						'tablejoin' => 'shipment',
						'where1' => array('notification.member_id' => $this->auth->get_user_id(), 'notification.status' => '"A"'),
						'where2' => 'shipment.shipment_id = notification.shipment_id',
						'order_by' => "bid_submit_date DESC"
						);

		$bids = $this->Appmodel->multi_table_rows($params);

		$data = array('bids' => $bids);

		$this->load->view('tab/my_bids', $data);
	}

	public function notifications()
	{
		$this->Appmodel->set_table('notification');

		if ($this->auth->is_driver()) {
			$where = 'member_id = '.$this->auth->get_user_id().' and (status = "ACC" or status = "R")';

			$notifications = $this->Appmodel->fetch_rows(NULL, $where);
		} else {
			$params = array('select' => '*',
							'tablejoin' => 'shipment',
							'where1' => array('shipment.member_id' => $this->auth->get_user_id()),
							'where2' => 'shipment.shipment_id = notification.shipment_id');

			$notifications = $this->Appmodel->multi_table_rows($params);

			foreach ($notifications as $row) {
				if ($row->status == 'A') {
					$row->is_new = TRUE;

					// set this notification as open
					$where = array('notification_id' => $row->notification_id);
					$new_data = array('status' => 'O', 'notification_read_date' => date('Y-m-d H:i:s'));
					$this->Appmodel->update($new_data, $where);
				}
			}
		}

		$data = array('notifications' => $notifications);

		$this->load->view('tab/notifications', $data);
	}
	
	public function curr_contracts(){
		
		if($this->auth->is_driver())
		{
			$contracts = $this->dashboard_model->get_driver_current_contracts($this->auth->get_user_id());
			
			foreach($contracts as $contract)
			{
				
				$this->Appmodel->set_table('shipper_invoice');
				$row_payment = $this->Appmodel->fetch_row(NULL, array('shipment_id' => $contract->shipment_id, 'is_accepted' => 1));
				$contract->is_payment_accepted = (empty($row_payment)) ? FALSE : TRUE;
			}
			
			$data['contracts'] = $contracts;
			$this->load->view('tab/my_current_contracts_driver', $data);
		}
		else 
		{
		
			$contracts = $this->dashboard_model->get_current_contracts($this->auth->get_user_id());
		
			foreach($contracts as $row){
				$this->Appmodel->set_table('member');
				$row->driver = $this->Appmodel->fetch_row('member_id, username', array('member_id' => $row->member_id));
				
				
				$this->Appmodel->set_table('shipper_invoice');
				$row_shipper_bayar = $this->Appmodel->fetch_row(NULL, array('shipment_id' => $row->shipment_id));
				
				$row->is_shipper_confirmed = ($row_shipper_bayar->invoice_date_paid == '') ? FALSE : TRUE;
				$row->invoice_status = $row_shipper_bayar->invoice_status;
				$row->status_message = $row_shipper_bayar->status_message;
				
				
				$row_admin_acc = $this->Appmodel->fetch_row(NULL, array('shipment_id' => $row->shipment_id, 'is_accepted' => 1));
				$row->is_admin_accepted = (empty($row_admin_acc)) ? FALSE : TRUE;;
			}
			
			$data['contracts'] = $contracts;
			$this->load->view('tab/my_current_contracts', $data);
			
		}
		
	}
	
	public function finished_contracts(){
		
		if($this->auth->is_driver())
		{
			
			$finished = $this->dashboard_model->get_finished_contracts_driver($this->auth->get_user_id());
			
			foreach($finished as $row)
			{
				$this->Appmodel->set_table('shipment');
				$row->shipment = $this->Appmodel->fetch_row('shipment_id, member_id', array('shipment_id' => $row->shipment_id));
				
				$this->Appmodel->set_table('member');
				$row->shipper = $this->Appmodel->fetch_row('member_id', array('member_id' => $row->shipment->member_id));
				
				$this->Appmodel->set_table('feedback');
				$row_feedback = $this->Appmodel->fetch_row('feedback_id', array('from_member_id' => $this->auth->get_user_id(), 'recipient_id' => $row->shipper->member_id, 'shipment_id' => $row->shipment_id));
				
				$row->feedback_already_sent = (empty($row_feedback)) ? FALSE : TRUE;
				
				$this->Appmodel->set_table('shipper_invoice');
				$invoice = $this->Appmodel->fetch_row(NULL, array('shipment_id' => $row->shipment->shipment_id, 'shipper_id' => $row->shipment->member_id, 'driver_id' => $this->auth->get_user_id()));
				
				$row->is_completed = (!empty($invoice)) ? $invoice->is_completed : 0;
			}
			
			$data['contracts'] = $finished;
			$this->load->view('tab/my_finished_contracts_driver', $data);
			
		}
		else
		{
			$finished = $this->dashboard_model->get_finished_contracts($this->auth->get_user_id());
			
			foreach($finished as $row){
				$this->Appmodel->set_table('member');
				$row->driver = $this->Appmodel->fetch_row('member_id, username', array('member_id' => $row->member_id));
				
				$this->Appmodel->set_table('feedback');
				$row_feedback = $this->Appmodel->fetch_row('feedback_id', array('from_member_id' => $this->auth->get_user_id(), 'recipient_id' => $row->driver->member_id, 'shipment_id' => $row->shipment_id));
				$row->feedback_already_sent = (empty($row_feedback)) ? FALSE : TRUE;
			}
			
			
			$data['contracts'] = $finished;
			$this->load->view('tab/my_finished_contracts', $data);
		}
		
	}
	
	public function my_feedback(){
		
		$my_feedbacks = $this->dashboard_model->get_my_feedback($this->auth->get_user_id());
		
		foreach($my_feedbacks as $feedback)
		{
			if($this->auth->get_user_id() == $feedback->from_member_id)
			{
				$member_id = $feedback->recipient_id;
			}
			else
			{
				$member_id = $feedback->from_member_id;
			}
			
			$this->Appmodel->set_table('member');
			$feedback->member = $this->Appmodel->fetch_row('username', array('member_id' => $member_id));
			
			$this->Appmodel->set_table('shipment');
			$feedback->shipment = $this->Appmodel->fetch_row('shipment_id, name', array('shipment_id' => $feedback->shipment_id));
		}
		
		$data['feedbacks'] = $my_feedbacks;
		$this->load->view('tab/my_feedbacks', $data);
		
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends Appmodel {

	function __construct() {
		parent::__construct();
	}
	
	function get_status($shipmentid)
	{
		$resource = $this->db->where('shipment_id', $shipmentid)
			->get('notification')
			->result();
			
		foreach($resource as $s)
		{
			if($s->status == 'ACC' || $s->status == 'C' || $s->status == 'F') {
				return FALSE;
				break;
			}
		}
		
		return TRUE;
	}
	
	function get_current_contracts($userid)
	{
		return $this->db->where(array('shipment.member_id' => $userid))
			->where("notification.status = 'ACC' OR notification.status = 'C'")
			->join('notification', "notification.shipment_id = shipment.shipment_id")
			->order_by('shipment.date_added DESC')
			->get('shipment')
			->result();	
	}
	
	function get_driver_current_contracts($userid)
	{
		return $this->db->where(array('notification.status' => 'ACC', 'notification.member_id' => $userid))
			->join('notification', "notification.shipment_id = shipment.shipment_id")
			->get('shipment')
			->result();	
	}
	
	
	
	function get_finished_contracts($userid){
		return $this->db->where(array('notification.status' => 'F', 'shipment.member_id' => $userid))
			->join('notification', "notification.shipment_id = shipment.shipment_id")
			->get('shipment')
			->result();
	}
	
	function get_finished_contracts_driver($userid){
		return $this->db->where("notification.status = 'F' OR notification.status = 'C'", NULL, FALSE)
			->where(array('notification.member_id' => $userid))
			->join('notification', "notification.shipment_id = shipment.shipment_id")
			->order_by('notification.shipper_report_date', 'DESC')
			->get('shipment')
			->result();
	}
	
	function get_my_feedback($userid){
		return $this->db->where('recipient_id', $userid)
			->order_by('date_added', 'DESC')
			->get('feedback')
			->result();
	
	}
	
	
	
}
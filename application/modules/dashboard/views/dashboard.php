<section class="ct-dashboard">
	<div class="container">
		<?php 
			$title = $member->is_driver ? 'Driver Dashboard' : 'Shipper Dashboard';
		 ?>
		<h3><?php echo $title ?></h3>
		<p>Selamat datang, <b><?php echo $member->first_name ?></b> | 
			<a href="<?php echo site_url('login/logout') ?>">Logout</a></p>

		<div class="row">
			<div class="col-md-2">
				<div class="profile-picture">
				<?php if(isset($member->profpic)) : ?>
				
					<?php if($member->profpic == '' || !file_exists('./public/images/uploads/memberprofile/' . $member->profpic)) : ?>
					<img width="150" height="150" src="<?php echo base_url(); ?>public/images/uploads/memberprofile/anonymous.png">
					<?php else : ?>
					<img width="150" height="150" src="<?php echo base_url(); ?>public/images/uploads/memberprofile/<?php echo $member->profpic; ?>" alt="<?php echo $member->first_name ?>">
					<?php endif; ?>
				<?php else :?>
					<img width="150" height="150" src="<?php echo base_url(); ?>public/images/uploads/memberprofile/anonymous.png">
				<?php endif; ?>
				</div>
			</div>

			<div class="col-md-3">
				<ul class="list-unstyled">
					<li><b><?php echo $member->first_name." ".$member->middle_name." ".$member->last_name ?></b></li>
					<li><a href="<?php echo site_url('profile/edit/' . $this->auth->get_user_id()); ?>">Edit Profile</a></li>
				</ul>
				
				<?php if (!$member->is_driver): ?>
					<a href="<?php echo site_url('shipment/add') ?>" class="btn btn-primary">Posting Shipment Baru</a>
				<?php endif ?>
			</div>
		</div>
		
		<?php if($this->session->flashdata('success')) : ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $this->session->flashdata('success'); ?>
		</div>
		<?php endif; ?>


		<div class="dashboard-tabs">
			<?php
				if ($member->is_driver) {
				 	$this->load->view('dashboard_tabs_driver');
				 } else {
				 	$this->load->view('dashboard_tabs_shipper');
				 }
			 ?>
		</div>
	</div>
</section>
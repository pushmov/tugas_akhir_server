<ul class="nav nav-tabs">
  <li><a href="#message" data-toggle="tab">Pesan</a></li>
  <li class="active"><a href="#myBids" data-toggle="tab">Bid Aktif Saya</a></li>
  <li><a href="#notifications" data-toggle="tab">Notifikasi</a></li>
	<li><a href="#mycontract" data-toggle="tab">Pekerjaan Sekarang</a></li>
	<li><a href="#finished" data-toggle="tab">Pekerjaan Selesai</a></li>
	<li><a href="#myfeedback" data-toggle="tab">Feedback Saya</a></li>
	
</ul>

<div class="tab-content">
  <div class="tab-pane" id="message"></div>
  <div class="tab-pane active" id="myBids"></div>
  <div class="tab-pane" id="notifications"></div>
	<div class="tab-pane" id="mycontract"></div>
	<div class="tab-pane" id="finished"></div>
	<div class="tab-pane" id="myfeedback"></div>
</div>
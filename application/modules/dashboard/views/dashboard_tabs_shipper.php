<ul class="nav nav-tabs">
  <li class="active"><a href="#myShipment" data-toggle="tab">Shipment Saya</a></li>
  <li><a href="#message" data-toggle="tab">Pesan</a></li>
  <li><a href="#notifications" data-toggle="tab">Notifikasi</a></li>
	<li><a href="#mycontract" data-toggle="tab">Kontrak Sekarang</a></li>
	<li><a href="#finished" data-toggle="tab">Kontrak Selesai</a></li>
	<li><a href="#myfeedback" data-toggle="tab">Feedback Saya</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="myShipment"></div>
  <div class="tab-pane" id="message"></div>
  <div class="tab-pane" id="notifications"></div>
	<div class="tab-pane" id="mycontract"></div>
	<div class="tab-pane" id="finished"></div>
	<div class="tab-pane" id="myfeedback"></div>
</div>
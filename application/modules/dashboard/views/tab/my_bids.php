<ul class="my-bids list-group">
	<?php foreach ($bids as $row): ?>
		<li class="list-group-item">
			<p><b><?php echo $row->name ?></b></p>
			<p><?php echo $row->bid_submit_date ?></p>
			<p><?php echo $row->detail ?></p>
			<p><strong>Bid anda: <?php echo $row->bid_price ?></strong></p>

			<ul class="list-inline">
				<li><a href="<?php echo site_url('shipment/view/'.$row->shipment_id) ?>">Lihat Shipment</a></li>
				<li><a onclick="return confirm('Apakah anda yakin akan menghapus bid ini?')" href="<?php echo site_url('driver/cancel/' . $row->notification_id);?>">Batalkan Bid</a></li>
			</ul>
		</li>
	<?php endforeach ?>	
</ul>

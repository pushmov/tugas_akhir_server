<ul class="my-shipments list-group">
	<?php foreach ($contracts as $row): ?>
		<li class="list-group-item">
			<p><b><?php echo $row->name ?></b></p>
			<p><?php echo $row->date_added ?></p>
			<p><?php echo $row->detail ?></p>
			<br>
			<div>
				<p><b>Pemenang : <?php echo $row->driver->username; ?></b><span style="margin-left:50px">Harga : Rp. <?php echo number_format($row->bid_price, 2); ?></span></p>
				
				
				<?php if($row->driver_report == '') : ?>
				
					<?php if($row->is_shipper_confirmed) : ?>
						
						<?php if($row->is_admin_accepted) : ?>
						<p><i style="color:#47A447">Payment telah di acc admin</i></p>
						<div style="background-color:#CC0000;color:#fff;padding:3px 8px;display:inline-block">Status : Delivery on Progress</div>
						
						<div style="margin:15px 0">
							<a href="<?php echo site_url('driver/location/' . $row->driver->member_id . '/' . $row->shipment_id); ?>"><i class="fa fa-map-marker fa-lg" style="color:#CC0000;margin-right:5px;"></i>Lihat Posisi Driver Sekarang</a>
						</div>
						
						<?php else : ?>
						
						<div style="background-color:#CC0000;color:#fff;padding:3px 8px;display:inline-block">Status : Menunggu Konfirmasi Admin</div>
						<?php endif; ?>
						
					<?php else : ?>
						
						<?php if($row->invoice_status == 'R') : ?>
							<p>Rejected</p>
							<p><strong>Admin</strong> : <i><?php echo $row->status_message; ?></i></p>
						<?php endif; ?>
						
						<div>
							<a class="btn btn-primary" style="color:#fff;padding:3px 8px;display:inline-block" href="<?php echo site_url('shipper/payment/' . $row->notification_id); ?>">Konfirmasi Pembayaran Sekarang</a>
						</div>
					<?php endif; ?>
				<?php else : ?>
				<div style="margin-bottom:10px;">
						<a class="btn" href="javascript:void(0)" style="margin:0 3px;background-color:#037701;color:#fff;padding:3px 8px;display:inline-block;margin-">Status : Telah dikirim</a>
						<a data-notificationid="<?php echo $row->notification_id; ?>" data-toggle="modal" data-target="#report-admin" id="shipper-report-admin" class="btn shipper-report-admin" href="javascript:void(0)" style="margin:0 3px;background-color:#037701;color:#fff;padding:3px 8px;display:inline-block">Report ke Admin</a>
						
						<!--
						<a href="#" data-notificationid="<?php echo $row->notification_id; ?>" data-toggle="modal" data-target="#report-driver" id="shipper-report-diver" class="btn shipper-report-diver" href="javascript:void(0)" style="margin:0 3px;background-color:#037701;color:#fff;padding:3px 8px;display:inline-block">Terima Report</a>
						-->
				</div>
				<p style="font-style:italic">"<?php echo $row->driver_report; ?>"</p>
				<p>Tanggal : <?php echo date('d/M/Y', strtotime($row->driver_report_date)); ?></p>
				<?php endif; ?>
				
			</div>
			
		</li>
	<?php endforeach ?>	
</ul>

<script>
	$('.shipper-report-diver').click(function(){
		$('#notificationid').val($(this).attr('data-notificationid'));
	});
	
	$('.shipper-report-admin').click(function(){
		$('#notificationid-2').val($(this).attr('data-notificationid'));
	});
	
	
</script>

<div class="modal fade" id="report-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Report</h4>
      </div>
      <div class="modal-body">
    	<h5>Kirim report ke admin</h5>

        <form class="form-inline" role="form" action="<?php echo site_url('shipper/report_admin/') ?>" method="post">
					<input type="hidden" name="notificationid" id="notificationid-2" value="">
          <div class="form-group">
            <label class="sr-only" for="price">Pesan</label>
            <textarea name="message" style="border: 1px solid rgb(153, 153, 153); width: 548px; height: 185px;padding:15px;" placeholder="Tulis pesan disini..."></textarea>
          </div>
					<div style="margin-top:15px;">
						<button type="submit" class="btn btn-primary">Kirim</button>
					</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="report-driver" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Report ke Driver</h4>
      </div>
      <div class="modal-body">
    	<h5>Pesan : </h5>

        <form class="form-inline" role="form" action="<?php echo site_url('shipper/report/') ?>" method="post">
					<input type="hidden" name="notificationid" id="notificationid" value="">
          <div class="form-group">
            <label class="sr-only" for="price">Pesan</label>
            <textarea name="message" style="border: 1px solid rgb(153, 153, 153); width: 548px; height: 185px;padding:15px;" placeholder="Tulis pesan disini..."></textarea>
          </div>
					<div style="margin-top:15px;">
						<button type="submit" class="btn btn-primary">Kirim</button>
					</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
<ul class="my-shipments list-group">
	<?php foreach ($contracts as $row): ?>
		<li class="list-group-item">
			<p><a href="<?php echo site_url('shipment/view/'.$row->shipment_id) ?>"><b><?php echo $row->name ?></b></a></p>
			<p><?php echo $row->date_added ?></p>
			<p><?php echo $row->detail ?></p>
			<br>
			<div>
				<p><b>Harga : Rp. <?php echo number_format($row->bid_price, 2); ?></b></p>
				
				
				<?php if($row->is_payment_accepted) : ?>
				
				
					<?php if($row->driver_report == '') : ?>
					<p>Pekerjaan dapat dimulai</p>
					<a href="javascript:void(0)" class="btn" style="background-color:#CC0000;color:#fff;padding:3px 8px;display:inline-block">Status : On Progress</a>
					<a data-notificationid="<?php echo $row->notification_id; ?>" data-toggle="modal" data-target="#report-driver" id="shipper-report-diver" class="btn driver-report-shipper" href="javascript:void(0)" style="background-color:#037701;color:#fff;padding:3px 8px;display:inline-block">Kirim status selesai</a>
					
					<?php else : ?>
					<div style="margin-bottom:10px;">
						<a class="btn" href="javascript:void(0)" style="margin:0 3px;background-color:#037701;color:#fff;padding:3px 8px;display:inline-block;margin-">Status : Report telah dikirim. Menunggu shipper review</a>
					</div>
					<p style="font-style:italic">"<?php echo $row->driver_report; ?>"</p>
					<p>Report dikirim tanggal : <?php echo date('d/M/Y', strtotime($row->driver_report_date)); ?></p>
					<?php endif; ?>
				
				<?php else : ?>
					<a class="btn btn-primary" href="javascript:void(0)" style="margin:0 3px;color:#fff;padding:3px 8px;display:inline-block;margin-">Status : Menunggu konfirmasi admin</a>
				<?php endif; ?>
				
			</div>
			
		</li>
	<?php endforeach ?>	
</ul>

<script>
	$('.driver-report-shipper').click(function(){
		$('#notificationid').val($(this).attr('data-notificationid'));
	});
	
</script>


<div class="modal fade" id="report-driver" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Report ke Admin</h4>
      </div>
      <div class="modal-body">
    	<h5>Pesan : </h5>

        <form class="form-inline" role="form" action="<?php echo site_url('driver/report/') ?>" method="post">
					<input type="hidden" name="notificationid" id="notificationid" value="">
          <div class="form-group">
            <label class="sr-only" for="price">Pesan</label>
            <textarea name="message" style="border: 1px solid rgb(153, 153, 153); width: 548px; height: 185px;padding:15px;" placeholder="Tulis pesan disini..."></textarea>
          </div>
					<div style="margin-top:15px;">
						<button type="submit" class="btn btn-primary">Kirim</button>
					</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
<ul class="my-shipments list-group">
	<?php foreach ($feedbacks as $row): ?>
		<li class="list-group-item">
			<h4><a href="<?php echo site_url('shipment/view' . $row->shipment->shipment_id);?>"><?php echo $row->shipment->name ?></a></h4>
			<p><b><?php echo $row->title ?></b></p>
			<p>Rating : <?php echo $row->rating ?> dari 5.0</p>
			
			<div style="margin:15px 0">
			<?php for($i=1;$i<=floor($row->rating);$i++) :?>
				<img src="<?php echo base_url(); ?>public/images/star-1.png" >
			<?php endfor; ?>
			<?php if(5 - floor($row->rating) > 0) : ?>
			<?php for($j=1;$j<=(5 - floor($row->rating));$j++) :?>
				<img src="<?php echo base_url(); ?>public/images/star-1-disabled.png" >
			<?php endfor; ?>
			<?php endif; ?>
			</div>
			
			<p><?php echo $row->feedback_text ?></p>
			<br>
			<div>
				<p><b>Dikirim oleh : <?php echo $row->member->username; ?></b><span style="margin-left:50px">Tanggal <?php echo date('d/M/Y',strtotime($row->date_added)); ?></span></p>
				
			</div>
			
		</li>
	<?php endforeach ?>	
</ul>


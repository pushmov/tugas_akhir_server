<ul class="my-shipments list-group">
	<?php foreach ($contracts as $row): ?>
		<li class="list-group-item">
			<p><b><?php echo $row->name ?></b></p>
			<p><?php echo $row->date_added ?></p>
			<p><?php echo $row->detail ?></p>
			<br>
			<div>
				<p><b>Pemenang : <?php echo $row->driver->username; ?></b><span style="margin-left:50px">Harga : Rp. <?php echo number_format($row->bid_price, 2); ?></span></p>
				
				<div style="margin-bottom:10px;">
				<?php if((bool) $row->feedback_already_sent) : ?>
					<button class="btn btn-success">Feedback telah dikirim</button>
				<?php else : ?>
					<button class="btn btn-primary" onclick="window.location='<?php echo site_url('feedback/to_driver/' . $row->notification_id); ?>'">Kirim Feedback ke Driver</button>
				<?php endif; ?>
				
				</div>
			</div>
			
		</li>
	<?php endforeach ?>	
</ul>


<ul class="my-shipments list-group">
	<?php foreach ($contracts as $row): ?>
		<li class="list-group-item">
			<p><b><?php echo $row->name ?></b></p>
			<p><?php echo $row->date_added ?></p>
			<p><?php echo $row->detail ?></p>
			<br>
			<div>
				<p><b>Status : 
					<?php if((bool) $row->is_shipper_accept_report) : ?>
						<span style="color:#037701">Telah di review <?php echo ($row->is_completed) ? ' & dana telah dikirim' : ''; ?></span>
					<?php else : ?>
						<span style="color:#CC0000">Sedang di review</span>
					<?php endif; ?>
				</b></p>
				<?php if((bool) $row->is_shipper_accept_report) : ?>
				<!--
				<p style="font-style:italic">"<?php echo $row->shipper_report; ?>"</p>
				-->
				<span>Tanggal : <?php echo date('d/M/Y',strtotime($row->shipper_report_date)); ?></span>
				<?php else : ?>
				<p></p>
				<?php endif; ?>
				
				<div style="margin-bottom:10px;">
				<?php if((bool) $row->feedback_already_sent) : ?>
					<button class="btn btn-success">Feedback telah dikirim</button>
				<?php else : ?>
					<button class="btn btn-primary" onclick="window.location='<?php echo site_url('feedback/to_shipper/' . $row->notification_id); ?>'">Kirim Feedback ke Shipper</button>
				<?php endif; ?>
				
				</div>
			</div>
			
		</li>
	<?php endforeach ?>	
</ul>


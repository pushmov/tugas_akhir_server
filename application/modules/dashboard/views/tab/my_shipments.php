<?php $this->load->library('shipment_lib'); ?>
<ul class="my-shipments list-group">
	<?php foreach ($shipments as $row): ?>
		<li class="list-group-item">
			<p><b><?php echo $row->name ?></b></p>
			<p><b><?php echo ($this->shipment_lib->get_applied_bids($row->shipment_id)); ?> Bid</b></p>
			<p><?php echo $row->date_added ?></p>
			<p><?php echo $row->detail ?></p>
			<br>
			<ul class="list-inline">
				<li><a href="<?php echo site_url('shipment/view/'.$row->shipment_id) ?>">Detail</a></li>
				<li><a href="<?php echo site_url('shipment/edit/' . $row->shipment_id);?>">Edit</a></li>
				<li><a href="<?php echo site_url('shipment/delete/' . $row->shipment_id); ?>" onclick="return confirm('Apakah anda yakin akan menghapus shipment ini?')">Hapus</a></li>
			</ul>
		</li>
	<?php endforeach ?>	
</ul>

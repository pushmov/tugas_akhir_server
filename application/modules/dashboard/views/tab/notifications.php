<ul class="notifications list-group">
	<?php if ($this->auth->is_driver()): ?>
		<?php foreach ($notifications as $row): ?>
			<?php
				if ($row->status == 'ACC') {
					$message = 'Bid anda telah diterima!';
				} else {
					$message = "Bid anda telah ditolak";
				}
			 ?>
			<li class="list-group-item">
				<p><?php echo $row->notification_add_date ?></p>
				<p><?php echo $message ?></p>
				
				<ul class="list-inline">
					<li><a href="<?php echo site_url('shipment/view/'.$row->shipment_id) ?>">Lihat Bid</a></li>
				</ul>
			</li>
		<?php endforeach ?>	

	<?php else: ?>

		<?php foreach ($notifications as $row): ?>
			<?php 
				if (isset($row->is_new)){
					$style = "style='background-color:#f0f0f0'";
				} else {
					$style = '';
				}
			?>
			
			
			<?php if($row->status == 'A') : ?>
			<li class="list-group-item" <?php echo $style ?>>
				<p><?php echo $row->notification_add_date ?></p>
				<p>Seseorang memasang bid pada iklan anda</p>

				<ul class="list-inline">
					<li><a href="<?php echo site_url('shipment/view/'.$row->shipment_id) ?>">Lihat Bid</a></li>
					<li><a href="#">Lihat Driver</a></li>
				</ul>
			</li>
			<?php endif; ?>
		<?php endforeach ?>	

	<?php endif ?>
	
</ul>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver extends MX_Controller {
	
	var $base_view;

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('driver/driver_model');

		
	}
	
	public function register()
	{
		if($this->auth->is_logged_in()){
			redirect('/dashboard');
		}

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			$this->load->library('form_validation');

			$config = array(
					      	array("field" => "email",
					          "rules" => "required|trim|strip_tags|valid_email"),
					      	array("field" => "username",
					          "rules" => "required|trim"),
					      	array("field" => "pass1",
					          "rules" => "required|trim|matches[pass2]"),
					      	array("field" => "pass2",
					          "rules" => "required|trim"),
					      	array("field" => "address",
					          "rules" => "trim|strip_tags"),
					      	array("field" => "phone",
					          "rules" => "required")
			 			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run()) {

				$this->_save_driver_data();

				redirect(site_url('driver/register_complete'));
			} else {
				// TODO: Display Error Message
				
				echo 'Semua field harus diisi';exit();
			}

		} else {
			$data = array('page_content' => 'driver_register',
				  		  'active_nav' => 'register');

			$this->load->view($this->base_view, $data);
		}
	}


	public function register_complete()
	{
		$data = array('page_content' => 'driver_register_complete',
			  		  'active_nav' => 'register');

		$this->load->view($this->base_view, $data);
	}



	private function _save_driver_data()
	{
		// Get driver data
		$member_data = array('email' => $this->form_validation->set_value('email'), 
							 'username' => $this->form_validation->set_value('username'), 
							 'password' => $this->form_validation->set_value('pass1'), 
							 'address' => $this->form_validation->set_value('address'), 
							 'phone' => $this->form_validation->set_value('phone'),
							 'bid_quota' => 10
							);

		// Save driver
		$this->Appmodel->set_table('member');
		$insertid = $this->Appmodel->insert($member_data);
		
		$this->Appmodel->set_table('member_profile');
		$member_detail = array(
			'member_id' => $insertid,
			'bank' => $this->input->post('rekening'),
			'bank_account_number' => $this->input->post('nomor_rekening')
		);
		$this->Appmodel->insert($member_detail);
	}
	
	public function report()
	{
		
		if(!$this->input->post('message') || !$this->input->post('notificationid'))
		{
			redirect('/dashboard');
		}
		
		$message = $this->input->post('message');
		$notificationid = $this->input->post('notificationid');
		
		$data = array(
			'status' => 'C',
			'driver_report' => $message,
			'driver_report_date' => date('Y-m-d H:i:s')
		);
		
		$this->Appmodel->set_table('notification');
		$this->Appmodel->update($data, array('notification_id' => $notificationid));
		
		$this->session->set_flashdata('success', 'Report telah dikirim ke shipper.');
		redirect('/dashboard');
		
	}
	
	function cancel($notification_id=NULL){
		
		if(!isset($notification_id)){
			redirect('dashboard');
		}
		
		$data = array(
			'status' => 'O',
			'bid_price' => NULL,
			'bid_submit_date' => NULL
		);
		
		$this->Appmodel->set_table('notification');
		$this->Appmodel->update($data, array('notification_id' => $notification_id));
		
		$this->session->set_flashdata('success', 'Bid anda telah di cancel.');
		redirect('/dashboard');
	}
	
	function location($id=NULL, $shipmentid=NULL){
		
		if(!isset($id) || !isset($shipmentid))
		{
			redirect('/dashboard');
		}
		
		$this->Appmodel->set_table('member');
		$row_driver = $this->Appmodel->fetch_row(NULL, array('member_id' => $id));
		
		if(empty($row_driver))
		{
			redirect('/dashboard');
		}
		
		$this->Appmodel->set_table('shipper_invoice');
		$row_acc = $this->Appmodel->fetch_row(NULL, array('shipment_id' => $shipmentid, 'driver_id' => $id));
		
		$date_end = $row_acc->invoice_date_paid;
		$date_start = date('Y-m-d H:i:s');
		
		$driver_location = $this->driver_model->get_location_driver($id, $date_start, $date_end);
		
		$latitude = (empty($driver_location)) ? 0.0000 : $driver_location->latitude;
		$longitude = (empty($driver_location)) ? 0.0000 : $driver_location->longitude;
		$last_seen = (empty($driver_location)) ? 'N/A' : $driver_location->date_created;
		
		
		$data = array('page_content' => 'location',
			  		  'active_nav' => '', 'latitude' => $latitude, 'longitude' => $longitude, 'last_seen' => $last_seen, 'driver' => $row_driver);
							
		$this->load->view('maps_single_marker', $data);
		
	}
	
}
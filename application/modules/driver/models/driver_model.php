<?php

class Driver_model extends AppModel {
	
	function get_location_driver($driverid, $start_date, $end_date){
		
		return $this->db->where(array('date_created >=' => $start_date, 'date_created <=' => $end_date, 'user_id' => $driverid))
			->order_by('date_created', 'DESC')
			->limit(1)
			->get('tracking')
			->row();
		
	}
	
		
}
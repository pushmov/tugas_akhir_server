<section class="ct-dashboard">
	<div class="container">
		<h3>Register Driver</h3>
		<div class="row">
		<div class="col-md-8">
			<form class="form-horizontal" role="form" method="POST" action="">
				<h4>Informasi Akun</h4>
				<fieldset>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Email<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				      		<input name="email" id="email-check" type="email" class="form-control required">
									<i class="fa fa-check-circle fa-lg" id="notif" style="position:absolute;right:25px;top:10px;display:none"></i>
									<span id="email-used" style="display:none;right:0;color:#ff0000">Email telah digunakan</span>
				    	</div>
				  	</div>

					<div class="form-group">
				    	<label class="col-sm-2 control-label">Username<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				      		<input name="username" type="text" class="form-control required">
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Password<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				      		<input name="pass1" type="password" class="form-control required">
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Ketik Ulang Password<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				      		<input name="pass2" type="password" class="form-control required">
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Alamat Lengkap<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				    		<textarea name="address" id="alamat" class="form-control required" cols="30" rows="5"></textarea>
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Telepon<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				      		<input name="phone" type="text" class="form-control required">
				    	</div>
				  	</div>

					<div class="form-group">
				    	<label class="col-sm-2 control-label">Rekening Anda<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				    		<select name="rekening" class="form-control">
					    		<option value="BCA">BCA</option>
									<option value="Mandiri">Mandiri</option>
									<option value="BNI">BNI</option>
				    		</select>
				    	</div>
				  	</div>
					
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Masukan No Rekening<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10">
				      		<input name="nomor_rekening" type="text" class="form-control required">
				    	</div>
				  	</div>
					
				  	<div class="form-group">
			  	    	<div class="col-sm-offset-2 col-sm-10">
				  	      	<div class="checkbox">
				  	        	<label><input type="checkbox" class="toc_checkbox" id="toc_checkbox"> Saya setuju dengan <a href="#">Terms &amp; Condition</a></label>
				  	      	</div>
			  	      	</div>
			  	    </div>
							
						<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10" id="notification" style="display:none">
					      <span style="color:#ff0000">Field dengan tanda * harus diisi</span>
					    </div>
						</div>

			  	    <div class="col-sm-offset-2 col-sm-10">
			  	    	<button class="btn btn-primary" id="btn-register">Register</button>
			  	    </div>

			  	  </div>
				</fieldset>
			  	
			</form>
		</div>
		</div>
	</div>
</section>

<script>
	$('#email-check').change(function(){
		$('#btn-register').show();
		$('#email-used').hide();
		$.get('<?php echo site_url('ajax/checkemail/'); ?>' + '/' + $(this).val(), function(data){
			console.log(data);
			var obj = $.parseJSON( data );
			if(obj.success == 'success'){
				$('#notif').removeClass('fa-check-circle').addClass('fa-ban').css({'color' : '#ff0000'}).show();
				$('#email-used').show();
				$('#btn-register').hide();
			} else {
				$('#notif').removeClass('fa-ban').addClass('fa-check-circle').css({'color' : '#2D8E00'}).show();
				$('#btn-register').show();
			}
		});
	});
	
	$('#btn-register').click(function(){
		
		if(!$('#toc_checkbox').is(':checked')){
			alert('Anda harus menyetujui TOS');
			return false;
		}
		
		var ret = true;
		
		$('.required').each(function(){
			
			if($(this).val() == ''){
				$('#notification').show();
				ret = false;
				return ret;
			}
			
		});
		
		return ret;
	});
</script>

<style>
	.holder{position:relative}
	i.fa-ban{display:none}
</style>
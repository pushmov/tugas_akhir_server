<section class="shipment">
	<div class="container">
		<h3>Registrasi Selesai!</h3>
		<div class="row">
			<div class="col-md-8">
				<p>Silahkan <a href="<?php echo site_url('login') ?>">Login</a> untuk menggunakan layanan kami.</p>

				<a href="<?php echo site_url() ?>" class="btn btn-primary">Kembali Ke Menu Utama</a>
			</div>
		</div>
	</div>
</section>

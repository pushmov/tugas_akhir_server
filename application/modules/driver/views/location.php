
<span id="base_url" alt="<?php echo base_url(); ?>"></span>
<section class="ct-find">
	<div class="container">
			
			<div style="margin:15px 0 10px 0">
				<h3>Posisi Driver <a href="<?php echo site_url('profile/index/' . $driver->member_id); ?>"><?php echo $driver->username; ?></h3>
				<span>Last seen : <?php echo date('d/M/Y H:i:s', strtotime($last_seen)); ?></span>
			</div>
			
			<div id="map-canvas" style="margin-top:15px;margin-bottom:15px;width: 100%;height: 700px;"></div>
	</div>
</section>

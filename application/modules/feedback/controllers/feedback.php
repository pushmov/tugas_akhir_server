<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends CI_Controller {
	
	var $feedback_view;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('feedback/feedback_model');
		
		$this->feedback_view = $this->config->item('feedback_view');
		
		if(!$this->auth->is_logged_in()){
			redirect('/login');
		}
	}
	
	public function to_driver($notification_id){
		
		$data = array('active_nav' => '', 'page_content' => 'feedback');
		$data['page_title'] = 'Kirim testimoni ke driver : ';
		
		$to_driver_row = $this->feedback_model->get_data($notification_id);
		
		
		
		$data['detail'] = $to_driver_row;
		$data['source'] = 'to_driver';
		$data['destination'] = 'to_driver_send';
		
		$this->load->view($this->feedback_view, $data);
		
	}
	
	public function to_shipper($notificationid){
	
		$data = array('active_nav' => '', 'page_content' => 'feedback');
		$data['page_title'] = 'Kirim testimoni ke shipper : ';
		
		$to_driver_row = $this->feedback_model->get_data($notificationid);
		
		$data['detail'] = $to_driver_row;
		$data['source'] = 'to_shipper';
		$data['destination'] = 'to_shipper_send';
		
		$this->load->view($this->feedback_view, $data);
	}
	
	public function to_driver_send($notification_id){
		
		if(!$this->input->post('content') || !$this->input->post('title'))
		{
			redirect('/feedback/' . $this->input->post('source') . '/' . $notification_id);
		}
		
		$this->Appmodel->set_table('notification');
		$row_notification = $this->Appmodel->fetch_row(NULL, array('notification_id' => $notification_id));
		
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$rating = $this->input->post('rating');
		
		$data = array(
			'title' => $title,
			'feedback_text' => $content,
			'rating' => $rating,
			'from_member_id' => $this->auth->get_user_id(),
			'recipient_id' => $row_notification->member_id,
			'shipment_id' => $row_notification->shipment_id
		);
		
		$this->Appmodel->set_table('feedback');
		$this->Appmodel->insert($data);
		
		$this->session->set_flashdata('success', 'Feedback telah terkirim ke driver');
		redirect('/dashboard');
		
	}
	
	public function to_shipper_send($notification_id){
		
		if(!$this->input->post('content') || !$this->input->post('title'))
		{
			redirect('/feedback/' . $this->input->post('source') . '/' . $notification_id);
		}
		
		$this->Appmodel->set_table('notification');
		$row_notification = $this->Appmodel->fetch_row(NULL, array('notification_id' => $notification_id));
		
		$this->Appmodel->set_table('shipment');
		$row_shipment = $this->Appmodel->fetch_row('shipment_id, member_id', array('shipment_id' => $row_notification->shipment_id));
		
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$rating = $this->input->post('rating');
		
		$data = array(
			'title' => $title,
			'feedback_text' => $content,
			'rating' => $rating,
			'from_member_id' => $this->auth->get_user_id(),
			'recipient_id' => $row_shipment->member_id,
			'shipment_id' => $row_shipment->shipment_id
		);
		
		$this->Appmodel->set_table('feedback');
		$this->Appmodel->insert($data);
		
		$this->session->set_flashdata('success', 'Feedback telah terkirim ke shipper');
		redirect('/dashboard');
		
	}
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends Appmodel {

	function __construct() {
		parent::__construct();
	}
	
	function get_data($notificationid){
		
		return $this->db->where(array('notification_id' => $notificationid, 'status' => 'F'))
			->join('member', "notification.member_id = member.member_id")
			->join('shipment', "notification.shipment_id = shipment.shipment_id")
			->get('notification')
			->row();
	}
	
	function get_data_driver($notificationid){
		
	}
	
	
}
<section class="ct-feedback">
		<div class="container">
				
				<h2><?php echo $page_title; ?></h2>
				
				<?php if($source == 'to_driver') : ?>
				<h5>Nama Driver : <strong><?php echo $detail->username; ?></strong></h5>
				<?php endif; ?>
				
				<h5>Shipment : <?php echo $detail->name; ?></h5>
				<h5>Tanggal Selesai : <?php echo date('d/M/Y',strtotime($detail->shipper_report_date)); ?></h5>
				
				<div class="alert alert-danger alert-dismissable" id="alert" style="display:none">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p style="color:#cc0000">Semua field harus diisi...</p>
				</div>
				
				<div style="padding:45px 0">
					<form method="post" id="feedback" name="feedback" action="<?php echo site_url('feedback/'.$destination.'/'.$detail->notification_id); ?>">
						<input type="hidden" name="source" value="<?php echo $source; ?>">
						<fieldset>
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Judul</label>
									<div class="col-sm-10">
											<input name="title" type="text" class="form-control required">
									</div>
							</div>
							
							<div class="form-group clearfix">	
								<label class="col-sm-2 control-label">Rating</label>
								<div class="input select rating-f col-sm-10">
										<select id="example-f" name="rating">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
										</select>
								</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Pesan</label>
									<div class="col-sm-10">
											<textarea name="content" class="form-control required" style="height:219px" placeholder="pesan..."></textarea>
									</div>
							</div>
							
							<input type="submit" id="submit" name="submit" class="btn btn-primary" value="KIRIM">
						</fieldset>
					</form>
				</div>
		</div>
</section>


<script>
	$('#submit').click(function(){
		$('.required').css({'border' : '1px solid #CCCCCC'});
		$('#alert').hide();
		var is_valid = true;
			$('.required').each(function(){
				if($(this).val() == '')
				{
					$(this).css({'border' : '1px solid #cc0000'});
					$('#alert').show('fast');
					is_valid = false;
				}
			});
			
			return is_valid;
	});
	
</script>
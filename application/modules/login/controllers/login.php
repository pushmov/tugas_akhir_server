<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {
	
	var $base_view;

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		
		// if($this->session->userdata('global_session')){
			// redirect('/dashboard');
		// }
		

		$this->base_view = $this->config->item('base_view');
	}
	
	public function index(){
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

		$data = array('page_content' => 'login',
					  'active_nav' => 'login');

		$this->load->view($this->base_view, $data);
	}
	
	public function verify(){
		
		$request = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		);
		
		if(!$this->input->post('password') || !$this->input->post('email')){
			$this->session->set_flashdata('message','Semua field harus diisi');
			redirect('/login');
		}
		
		$this->Appmodel->set_table('member');
		$row = $this->Appmodel->fetch_row(NULL,array('email' => $request['email'], 'password' => $request['password']));
		
		if(empty($row)){
			$this->session->set_flashdata('message', 'Email ' . $request['email'] . ' belum terdaftar atau password salah. Silahkan daftar terlebih dahulu.');
			redirect('/login');
		}
		
		
		$sess_data = array(
			'userid' => $row->member_id,
			'email' => $row->email,
			'type' => ($row->is_driver) ? 'driver' : 'shipper'
		);
		$this->session->set_userdata('global_session',$sess_data);
		redirect('/dashboard');
		
	}
	
	public function logout(){
		
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		$this->session->set_flashdata('message', 'Anda telah logout');
		redirect('/login');
		
	}
	
	

}
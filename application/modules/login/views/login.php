<section class="ct-login">
	<div class="container">
		
		<?php if($this->session->flashdata('message')) : ?>
			<div class="alert alert-danger">
			  <a href="#" class="alert-link"><?php echo $this->session->flashdata('message'); ?></a>
			</div>
		<?php endif; ?>
			<fieldset>
				<h3>Login Area</h3>
				<form class="form-horizontal" role="form" action="<?php echo site_url('login/verify'); ?>" method="POST" name="form-login">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-4">
							<input type="email" class="form-control" id="inputEmail3" name="email" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox"> Remember me
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">Sign in</button>
						</div>
					</div>
			</form>

			</fieldset>
	</div>
</section>

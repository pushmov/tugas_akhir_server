<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Appmodel');
		
	}

	public function add($shipment_id)
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			// Check if price is null
			if ($this->input->post('price') == '') {
				$this->session->set_flashdata('error', 'Mohon isi kolom harga');
				redirect(site_url('shipment/view/'.$shipment_id));
			}

			// Check if shipment exists
			$this->Appmodel->set_table('shipment');
			$where = array('shipment_id' => $shipment_id);
			$shipment = $this->Appmodel->fetch_row(NULL, $where);

			if (!$shipment) {
				return show_404();
			}

			// Check if user has not placed a bid on this shipment
			$this->Appmodel->set_table('notification');
			$notification_data = array('shipment_id' => $shipment_id, 'member_id' => $this->auth->get_user_id(), 'status' => 'A');
			$is_exist = $this->Appmodel->fetch_row(NULL, $notification_data);
			if ($is_exist) {
				// Bid exist, set message
				$this->session->set_flashdata('error', 'Anda telah memasang bid pada shipment ini.');
				redirect(site_url('shipment/view/'.$shipment_id));
			}

			// Add to database
			$notification_data['bid_price'] = $this->input->post('price');
			$notification_data['bid_submit_date'] = date('Y-m-d H:i:s');
			$notification_data['status'] = 'A';
			
			$check_row = $this->Appmodel->fetch_row(NULL, array('shipment_id' => $shipment_id, 'member_id' => $this->auth->get_user_id(), 'status' => 'O'));
			if(empty($check_row)){
				$this->Appmodel->insert($notification_data);
			} else {
				$this->Appmodel->update($notification_data, array('notification_id' => $check_row->notification_id));
			}

			// Redirect to shipment detail
			$this->session->set_flashdata('success', 'Bid anda berhasil dipasang');
			redirect(site_url('shipment/view/'.$shipment_id));
		}
	}

	public function choose($notification_id)
	{
		// Check if parameter is null
		if (!$notification_id) {
			return show_404();
		}

		// Get notification object
		$this->Appmodel->set_table('notification');
		$where = array('notification_id' => $notification_id);
		$notification = $this->Appmodel->fetch_row(NULL, $where);

		// Check if notification exists
		if (!$notification) {
			return show_404();
		}

		// Check if shipment is users
		$this->Appmodel->set_table('shipment');
		$where = array('shipment_id' => $notification->shipment_id);
		$shipment = $this->Appmodel->fetch_row(NULL, $where);
		if (!$shipment || $shipment->member_id != $this->auth->get_user_id()) {
			return show_404();
		}
		
		//delete open bid notification
		$this->Appmodel->set_table('notification');
		$where = array('shipment_id' => $notification->shipment_id, 'status' => 'O');
		$this->Appmodel->delete($where);

		// Set other notification as rejected
		
		$where = array('shipment_id' => $notification->shipment_id);
		$data = array('status' => 'R'); 
		$shipment = $this->Appmodel->update($data, $where);

		// Set notification as accepted
		$where = array('notification_id' => $notification_id);
		$data = array('status' => 'ACC'); 
		$shipment = $this->Appmodel->update($data, $where);
		
		//shipment locked
		$where = array('shipment_id' => $notification->shipment_id);
		$data = array('is_accept_driver' => 1);
		$this->Appmodel->set_table('shipment');
		$this->Appmodel->update($data, $where);
		
		//create invoice data for admin
		$this->Appmodel->set_table('shipper_invoice');
		$data = array('shipment_id' => $notification->shipment_id, 'shipper_id' => $this->auth->get_user_id(),'driver_id' => $notification->member_id,'invoice_number' => $this->generateRandomString());
		$this->Appmodel->insert($data);
		
		// Set flash data & redirect
		$this->session->set_flashdata('success', 'Anda telah memilih driver.');
		redirect(site_url('shipment/view/'. $notification->shipment_id));

	}
	
	function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
	}
	
	

}

/* End of file notification.php */
/* Location: ./application/controllers/notification.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	var $base_view;
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Appmodel');
		$this->load->model('profile_model');
		$this->base_view = $this->config->item('base_view');
	}
	
	private function _calculate_rating($memberid){
		
		$result = $this->profile_model->my_feedbacks($memberid);
		
		$rating = 0;
		$total = 0;
		
		if(!empty($result))
		{
		
			foreach($result as $row)
			{
				$rating += (int) $row->rating;
				$total++;
			}
		
		}
		
		return number_format(round(($rating / $total),1) , 1);
		
	}
	
	private function _calculate_spesifik_rating($member_id){
		
		$curr_category = $this->profile_model->get_current_category();
		
		$my_overall = array();
		
		foreach($curr_category as $id => $val)
		{
			
			$enabled = $this->profile_model->calculate_spesifik($member_id, $id);
			
			$my_overall[$val] = array(
				'enabled' => $enabled['avg'],
				'disabled' => 5 - $enabled['avg'],
				'reviews' => $enabled['reviews']
			);
			
		}
		
		return $my_overall;
	}

	public function index($member_id=NULL)
	{	
		if (is_null($member_id)) {
			return show_404();
		}

		// get member data
		$member = $this->profile_model->get_profile($member_id);
		
		$detail = $this->profile_model->get_profile_detail($member_id);
		
		$latest = $this->profile_model->get_latest_shipments($member_id);

		$data = array('member' => $member,
						'detail' => $detail,
						'rating_enabled' => $this->_calculate_rating($member_id),
						'rating_disabled' => (5 - $this->_calculate_rating($member_id)),
						'latest' => $latest,
					  'page_content' => 'view_profile',
						'overall_rating' => $this->_calculate_spesifik_rating($member_id),
					  'active_nav' => NULL);
		
		$this->load->view($this->base_view, $data);
	}
	
	function changepic($memberid){
		
		$this->db->where('member_id', $memberid)
			->set('profpic', NULL)
			->update('member');
		
		redirect('/profile/edit/' . $memberid);
		
	}
	
	function edit($id=NULL){
		
		if(!$this->auth->is_logged_in()){
			redirect('/login');
		}
		
		if(!isset($id)){
			redirect('dashboard');
		}
		
		$row_member = $this->profile_model->get_profile($id);
		
		if($row_member->member_id != $this->auth->get_user_id()){
			redirect('dashboard');
		}
		
		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			
			$member_data = array(
				'first_name' => $_POST['fname'],
				'middle_name' => $_POST['mname'],
				'last_name' => $_POST['lname'],
				'date_of_birth' => $_POST['dob'],
				'address' => $_POST['address'],
				'city' => $_POST['city'],
				'zip_code' => $_POST['zip_code'],
				'state_code' => $_POST['province_from'],
				'phone' => $_POST['phone']
			);
			
			$member_profile = array(
				'experience' => $_POST['experience'],
				'car_type' => $_POST['car_type'],
				'insurance_company' => $_POST['insurance'],
				'bank' => $_POST['rekening'],
				'bank_account_number' => $_POST['nomor_rekening']
			);
			
			$request['photo'] = (!empty($_FILES['profpic']['tmp_name'])) ? time() .'-'.$this->auth->get_user_id() : '';
			$photo = $request['photo'];

			/** upload configuration */
			$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
			$config['max_size'] = 1000000;
			$config['upload_path'] = './public/images/uploads/memberprofile/';

			if($photo != '')
			{
				$config['file_name'] = $photo;
				$this->load->library('upload',$config);

				if(!$this->upload->do_upload('profpic'))
				{
					echo $this->upload->display_error();
					exit();
				}
				else
				{
					$data = $this->upload->data();
					$photo = $data['orig_name'];

					$this->Appmodel->set_table('member');
					$this->Appmodel->update(array('profpic' => $photo), array('member_id' => $id));
					
				}
			}
			
			$this->Appmodel->set_table('member');
			$this->Appmodel->update($member_data, array('member_id' => $id));
			
			$this->Appmodel->set_table('member_profile');
			$this->Appmodel->update($member_profile, array('member_id' => $id));
			
			$this->session->set_flashdata('success', 'Profile berhasil di update. <a href="'.site_url('profile/index/' .$id).'">Lihat profile sekarang</a>');
			redirect('profile/edit/' . $id);
			
		}
		
		
		$row_detail = $this->profile_model->get_profile_detail($id);
		
		$data = array('member' => $row_member,
						'detail' => $row_detail,
					  'page_content' => 'edit_profile',
					  'active_nav' => NULL);
						
		$this->Appmodel->set_table('province_code');
		$data['provinces'] = $this->Appmodel->fetch_rows();
						
		$this->load->view($this->base_view, $data);
		
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends Appmodel {
	
	
	function get_profile($id){
		return $this->db->where('member_id', $id)
			->join('province_code', "member.state_code = province_code.province_code")
			->get('member')
			->row();
	}
	
	function get_profile_detail($id){
		return $this->db->where('member_id', $id)
			->get('member_profile')
			->row();
	
	}
	
	function my_feedbacks($id){
		
		return $this->db->select('shipment.name, shipment.type, feedback.*')
			->where('recipient_id', $id)
			->join('shipment', "shipment.shipment_id = feedback.shipment_id")
			->get('feedback')
			->result();
	
	}
	
	function get_latest_shipments($id){
		return $this->db->where('status', 'F')
			->where('notification.member_id', $id)
			->join('shipment', "shipment.shipment_id = notification.shipment_id")
			->join('feedback', "feedback.shipment_id = notification.shipment_id AND notification.member_id = feedback.recipient_id")
			->get('notification')
			->result();
	}
	
	function get_current_category(){
		$return = array();
		
		$query = $this->db->order_by('type', 'ASC')
			->get('shipment_type')
			->result();
		
		if(!empty($query)){
			foreach($query as $row){
				
				$return[$row->type] = $row->shipment_type_name;
			
			}
		}
		
		return $return;
	}
	
	function calculate_spesifik($memberid, $type){
		
		$total_points = $this->db->where(array('recipient_id' => $memberid, 'shipment.type' => $type))
			->join('shipment', "shipment.shipment_id = feedback.shipment_id")
			->join('shipment_type', "shipment.type = shipment_type.type")
			->get('feedback')
			->result();
		
		$acc_total= 0;
		$avg_type['avg'] = 0;
		foreach($total_points as $row){
			$acc_total += $row->rating;
		}
		
		$avg_type['avg'] = (!empty($total_points)) ? round($acc_total / count($total_points), 1) : 0;
		$avg_type['reviews'] = count($total_points);
		
		return $avg_type;
	}

}


<section class="shipment">
	<div class="container">
		<h3>Edit My Profile</h3>
		
		<?php if($this->session->flashdata('success')) : ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $this->session->flashdata('success'); ?>
		</div>
		<?php endif; ?>
		
		
		<div class="row">
		<div class="col-md-8">
			<form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('profile/edit/' . $member->member_id); ?>" enctype="multipart/form-data">
				
				<fieldset>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-10 clearfix">
								<div class="pull-left" style="margin-right:15px;">
									<?php if($member->profpic != '') : ?>
									<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/<?php echo $member->profpic; ?>" width="100" height="100">
									<?php else : ?>
									<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/anonymous.png" width="100" height="100">
									<?php endif; ?>
								</div>
								<div>
									<h5><?php echo $member->username; ?></h5>
									<input type="file" name="profpic" id="profpic" >
								</div>
							</div>
							
						</div>
				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Nama Depan</label>
				    	<div class="col-sm-10 holder">
				      		<input name="fname" type="text" class="form-control" value="<?php echo $member->first_name; ?>">
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Nama Tengah</label>
				    	<div class="col-sm-10 holder">
				      		<input name="mname" type="text" class="form-control" value="<?php echo $member->middle_name; ?>">
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Nama Belakang</label>
				    	<div class="col-sm-10 holder">
				      		<input name="lname" type="text" class="form-control" value="<?php echo $member->last_name; ?>">
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">D.O.B</label>
				    	<div class="col-sm-10 holder">
				      		<input name="dob" placeholder="YYYY-MM-DD" type="text" class="date-field form-control" value="<?php echo $member->date_of_birth; ?>">
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Alamat<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="address" cols="30" rows="5" class="form-control"><?php echo $member->address; ?></textarea>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Provinsi</label>
				    	<div class="col-sm-10 holder">
				      	<select name="province_from" class="form-control" id="province_from">
					    		<?php foreach ($provinces as $province): ?>
					    			<option value="<?php echo $province->province_code ?>"><?php echo $province->province_name ?></option>
					    		<?php endforeach ?>
				    		</select>
								
								<script>
									$('#province_from').val('<?php echo $member->state_code; ?>');
								</script>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Kota</label>
				    	<div class="col-sm-10 holder">
				      		<input name="city" type="text" class="form-control" value="<?php echo $member->city; ?>">
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Kode Pos</label>
				    	<div class="col-sm-10 holder">
				      		<input name="zip_code" type="text" class="form-control" value="<?php echo $member->zip_code; ?>">
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">No. Telpon</label>
				    	<div class="col-sm-10 holder">
				      		<input name="phone" type="text" class="form-control" value="<?php echo $member->phone; ?>">
				    	</div>
				  	</div>
						
					</fieldset>
						<?php if($this->auth->is_driver()) : ?>
					
					<fieldset>
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Pengalaman</label>
				    	<div class="col-sm-10 holder">
				      	<textarea name="experience" cols="30" rows="5" class="form-control required"><?php echo $detail->experience; ?></textarea>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Tipe Kendaraan</label>
				    	<div class="col-sm-10 holder">
				      	<textarea name="car_type" cols="30" rows="5" class="form-control required"><?php echo $detail->car_type; ?></textarea>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Asuransi</label>
				    	<div class="col-sm-10 holder">
				      	<textarea name="insurance" cols="30" rows="5" class="form-control required"><?php echo $detail->insurance_company; ?></textarea>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Rekening</label>
				    	<div class="col-sm-10 holder">
								<select name="rekening" id="rekening" class="form-control">
									<option value="BCA">BCA</option>
									<option value="Mandiri">Mandiri</option>
									<option value="BNI">BNI</option>
								</select>
								<script>
									$('#rekening').val('<?php echo $detail->bank; ?>');
								</script>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Nomer Rekening</label>
				    	<div class="col-sm-10 holder">
				      		<input name="nomor_rekening" type="text" class="form-control" value="<?php echo $detail->bank_account_number; ?>">
				    	</div>
				  	</div>
					</fieldset>
						
						<?php endif; ?>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10" id="notification" style="display:none">
					      <span style="color:#ff0000">Field dengan tanda * harus diisi</span>
					    </div>
					</div>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-primary" id="submit">Update</button>
					    </div>
					</div>
				
			  	
			</form>
		</div>
		</div>
	</div>
</section>

<script>
	$('#email-check').change(function(){
		$.get('<?php echo site_url('ajax/checkemail/'); ?>' + '/' + $(this).val(), function(data){
			console.log(data);
			var obj = $.parseJSON( data );
			if(obj.success == 'success'){
				$('#notif').removeClass('fa-check-circle').addClass('fa-ban').css({'color' : '#ff0000'}).show();
			} else {
				$('#notif').removeClass('fa-ban').addClass('fa-check-circle').css({'color' : '#2D8E00'}).show();
			}
		});
	});
	
	$('#submit').click(function(){
		var isvalid = true;
		$('.required').each(function(){
			var value = $(this).val();
			
			if(value == ''){
				isvalid = false;
				console.log();
				$('#notification').show();
				return isvalid;
			}
		});
		
		return isvalid;
	})
</script>

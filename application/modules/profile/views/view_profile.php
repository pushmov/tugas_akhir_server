

<section class="shipment">
	<div class="container">
		<h3>Profile - <?php echo $member->username; ?></h3>
		
		
		<div class="row">
		<div class="col-md-12">
				<div class="clearfix">
					<div class="col-sm-4 clearfix" style="margin:10px 25px">
						<div class="pull-left" style="margin-right:15px;">
							<?php if($member->profpic != '') : ?>
							<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/<?php echo $member->profpic; ?>" width="100" height="100">
							<?php else : ?>
							<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/anonymous.png" width="100" height="100">
							<?php endif; ?>
						</div>
						<div>
							<h5 style="font-weight:bold;font-size:16px;">Rating </h5>
							<p><?php echo $rating_enabled; ?> from 5.0</p>
							
							<?php for($i=1;$i<=$rating_enabled;$i++) : ?>
							<img src="<?php echo base_url(); ?>public/images/star-1.png" >
							<?php endfor; ?>
							<?php for($i=1;$i<=$rating_disabled;$i++) : ?>
							<img src="<?php echo base_url(); ?>public/images/star-1-disabled.png" >
							<?php endfor; ?>
							
						</div>
					</div>
					
							
				</div>
				
				<h5 style="margin-left:25px;font-weight:bold;font-size:22px">Informasi Akun</h5>
				<div class="profile" style="margin:10px 25px;border-radius:4px;border:1px solid #eaeaea">
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Nama Depan</label>
						<div class="col-sm-10 holder">
							<?php echo $member->first_name . ' ' . $member->middle_name . ' ' . $member->last_name; ?>
						</div>
					</div>
							
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Tanggal Lahir</label>
						<div class="col-sm-10 holder">
							<?php echo $member->date_of_birth; ?>
						</div>
					</div>

					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Alamat</label>
						<div class="col-sm-10 holder">
							<?php echo $member->address; ?>
						</div>
					</div>
					
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Provinsi</label>
						<div class="col-sm-10 holder">
							<?php echo $member->province_name; ?>
						</div>
					</div>
					
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Kota</label>
						<div class="col-sm-10 holder">
							<?php echo $member->city; ?>
						</div>
					</div>
					
					<?php if($this->auth->is_driver()) : ?>
					
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Pengalaman</label>
						<div class="col-sm-10 holder">
							<?php echo $detail->experience; ?>
						</div>
					</div>
					
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Tipe Kendaraan</label>
						<div class="col-sm-10 holder">
							<?php echo $detail->car_type; ?>
						</div>
					</div>
					
					<div class="profile-item clearfix">
						<label class="col-sm-2 control-label">Asuransi</label>
						<div class="col-sm-10 holder">
							<?php echo $detail->insurance_company; ?>
						</div>
					</div>
					
					<?php endif; ?>
					
					
				</div>
				
				<h5 style="margin-top:25px;margin-left:25px;font-weight:bold;font-size:22px">Statistik Rating ( Total Rating : <?php echo $rating_enabled; ?> )</h5>
				
					<div class="profile" style="margin:10px 25px;border-radius:4px;border:1px solid #eaeaea">
						<?php foreach($overall_rating as $index => $row) : ?>
						<div class="profile-item clearfix">
							<label class="col-sm-2 control-label">
								<?php echo $index; ?>
								<h5 style="font-weight:normal;font-size:14px;"><?php echo round($row['enabled']) . ' from 5'; ?> (<?php echo $row['reviews'] . ' feedback';?>)</h5>
							</label>
							<div class="col-sm-10 holder">
								
								<?php for($i=1;$i<=round($row['enabled']);$i++) : ?>
								<img src="<?php echo base_url(); ?>public/images/star-1.png" >
								<?php endfor; ?>
								<?php for($i=1;$i<=floor($row['disabled']);$i++) : ?>
								<img src="<?php echo base_url(); ?>public/images/star-1-disabled.png" >
								<?php endfor; ?>
								
								
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				
				
				<h5 style="margin-top:25px;margin-left:25px;font-weight:bold;font-size:22px">Pekerjaan Terakhir</h5>
				
				<?php if(!empty($latest)) : ?>
					<?php foreach($latest as $row) : ?>
					<div class="profile" style="margin:10px 25px;border-radius:4px;border:1px solid #eaeaea">
						<div class="profile-item clearfix">
							<label class="col-sm-2 control-label">
								<a href="<?php echo site_url('shipment/view/' . $row->shipment_id); ?>"><?php echo $row->name; ?></a>
							</label>
							<div class="col-sm-10 holder">
								<span><?php echo date('d-M-Y', strtotime($row->driver_report_date)); ?></span>
								<p><?php echo $row->detail; ?></p>
							</div>
						</div>
						
						<div class="profile-item clearfix">
							<label class="col-sm-2 control-label">Feedback</label>
							<div class="col-sm-10 holder">
								
								<?php for($i=1;$i<=round($row->rating);$i++) : ?>
								<img src="<?php echo base_url(); ?>public/images/star-1.png" >
								<?php endfor; ?>
								<?php for($i=1;$i<=5-round($row->rating);$i++) : ?>
								<img src="<?php echo base_url(); ?>public/images/star-1-disabled.png" >
								<?php endfor; ?>
								
								<h3 style="margin:5px 0;font-size:16px;font-weight:bold"><?php echo $row->title; ?></h3>
								<p style="margin:0"><?php echo $row->feedback_text; ?></p>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				<?php else : ?>
				<h3>Data tidak ditemukan</h3>
				<?php endif; ?>
			  	
			</form>
		</div>
		</div>
	</div>
</section>
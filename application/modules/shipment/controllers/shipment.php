<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipment extends MX_Controller {

	var $base_view;
			
	function __construct()
	{
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->library('shipment_lib');
		$this->load->model('shipment/shipment_model');

		$this->base_view = $this->config->item('base_view');
	}

	public function find()
	{
		$table_name = 'shipment';

		// List Query
		$this->Appmodel->set_table($table_name);
		$select = ('shipment_id, name, detail, pickup_city, deliver_city, date_added, pickup_max_date, type, sub_type');
		$where = NULL;

		//Pagination
		$this->load->library('pagination');
		$pagination_config = array('base_url' => site_url('shipment/find'),
		                           'total_rows' => $this->Appmodel->count($table_name, $where),
		                           'per_page' => 10,
		                           'uri_segment' => 3);

		$this->pagination->initialize($pagination_config);

		$page = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) - 1 : 0;
		$limit = $pagination_config['per_page'];
		$offset = $page * $limit;
		//

		// Get entries
		$order = "date_added DESC";
		$where = array('shipment.is_accept_driver' => 0);
		$shipments = $this->Appmodel->fetch_rows($select, $where, $limit, $offset, $order);

		$data = array('shipments' => $shipments,
					  'pagination' => $this->pagination->create_links(),
					  'page_content' => 'list',
			  		  'active_nav' => 'shipment');

		$this->load->view($this->base_view, $data);
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			$this->load->library('form_validation');

			$config = array(
					      	array("field" => "shipment_name",
					          "rules" => "required|trim|strip_tags"),
					      	array("field" => "detail",
					          "rules" => "required|trim"),
					      	array("field" => "price_offered",
					          "rules" => ""),
					      	array("field" => "max_date",
					          "rules" => "")
					 			);

			if (!$this->auth->is_logged_in()) {
				$config_member = array(
									array("field" => "email",
							          "rules" => "required|trim|strip_tags|valid_email"),
							      	array("field" => "username",
							          "rules" => "required|trim"),
							      	array("field" => "pass1",
							          "rules" => "required|trim|matches[pass2]"),
							      	array("field" => "pass2",
							          "rules" => "required|trim")
							      );

				$config = array_merge($config, $config_member);
			}

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run()) {

				$shipment_id = $this->_save_shipment_data();

				redirect(site_url('shipment/add_complete/'.$shipment_id));
			}

			
		} else {
			$data = array('page_content' => 'add',
						  'active_nav' => 'home');

			$this->Appmodel->set_table('shipment_type');
			$data['kategori'] = $this->Appmodel->fetch_rows();

			$this->Appmodel->set_table('province_code');
			$data['provinces'] = $this->Appmodel->fetch_rows();

			$this->load->view($this->base_view, $data);	
		}

	}

	public function add_complete($shipment_id)
	{
		$data = array('page_content' => 'add_complete',
					  			'active_nav' => 'home',
					  			'shipment_id' => $shipment_id);

		$this->load->view($this->base_view, $data);
	}

	public function view($shipment_id=NULL)
	{
		if (is_null($shipment_id)){
			return show_404();
		}

		//Get shipment data
		$this->Appmodel->set_table('shipment');
		$where = array('shipment_id' => $shipment_id);
		$shipment = $this->Appmodel->fetch_row(NULL, $where);

		$data = array('shipment' => $shipment,
									'page_content' => 'view',
									'active_nav' => NULL);
		
		$data['pictures'] = $this->shipment_model->get_shipment_pictures($shipment_id);
		
		$bidders = $this->shipment_model->get_bidder_list($shipment_id);
		$data['bids'] = $bidders;

		$this->load->view($this->base_view, $data);
	}

	public function curl_post_async($url, $params){
			foreach ($params as $key => &$val) {
				if (is_array($val)) $val = implode(',', $val);
					$post_params[] = $key.'='.urlencode($val);
			}
			$post_string = implode('&', $post_params);

			$parts=parse_url($url);
			$fp = fsockopen($parts['host'],
					isset($parts['port'])?$parts['port']:80,
					$errno, $errstr, 30);

			$out = "POST ".$parts['path']." HTTP/1.1\r\n";
			$out.= "Host: ".$parts['host']."\r\n";
			$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
			$out.= "Content-Length: ".strlen($post_string)."\r\n";
			$out.= "Connection: Close\r\n\r\n";
			if (isset($post_string)) $out.= $post_string;

			fwrite($fp, $out);
			fclose($fp);
	}
	
	private function _get_lat_lon($kode, $reg2, $reg3, $local, $zip){
		
		$row_province = $this->shipment_model->get_name_province($kode);
		
		$row = $this->shipment_model->get_location($row_province->province_name_en, $reg2, $reg3, $local, $zip);
		
		$return = array(
			'latitude' => $row->latitude,
			'longitude' => $row->longitude
		);
		
		return $return;
		
	}

	private function _save_shipment_data()
	{
		// Get id from session if user is logged in
		if ($this->auth->is_logged_in()) {
			$member_id = $this->auth->get_user_id();
		} else {
			// Register member
			$member_id = $this->_save_shipper_data();
		}
		
		$pickup_location = $this->_get_lat_lon(
			$_POST['pickup_province_code'], 
			$_POST['pickup_city'], 
			$_POST['pickup_kecamatan'], 
			$_POST['pickup_kelurahan'],
			$_POST['kodepos_asal']
		);
		
		
		
		$deliver_location = $this->_get_lat_lon(
			$_POST['deliver_province_code'], 
			$_POST['deliver_city'], 
			$_POST['deliver_kecamatan'], 
			$_POST['deliver_kelurahan'],
			$_POST['kodepos_tujuan']
		);
		
		// Get shipment data
		$shipment_data = array('member_id' => $member_id,
			'name' => $this->form_validation->set_value('shipment_name'), 
			'type' => $_POST['type'],
			'sub_type' => $_POST['subtype'],
			'detail' => $_POST['detail'],
			'price_offered' => $_POST['price_offered'],
			'pickup_address' => $_POST['pickup_address'],
			'pickup_city' => $_POST['pickup_city'],
			'pickup_province_code' => $_POST['pickup_province_code'],
			'pickup_zip_code' => $_POST['kodepos_asal'],
			'pickup_max_date' => $this->form_validation->set_value('max_date'),
			'pickup_latitude' => $pickup_location['latitude'],
			'pickup_longitude' => $pickup_location['longitude'],
			'deliver_address' => $_POST['deliver_address'],
			'deliver_city' => $_POST['deliver_city'],
			'deliver_province_code' => $_POST['deliver_province_code'],
			'deliver_zip_code' => $_POST['kodepos_tujuan'],
			'deliver_max_date' => $this->form_validation->set_value('max_date'),
			'deliver_latitude' => $deliver_location['latitude'],
			'deliver_longitude' => $deliver_location['longitude']
		);

		// Save shipment
		
		$this->Appmodel->set_table('shipment');
		
		$shipmentid = $this->Appmodel->insert($shipment_data);
		
		$request['photo'] = (!empty($_FILES['picture']['tmp_name'])) ? time() .'-'.$shipmentid : '';
		$photo = $request['photo'];
		
		$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
		$config['max_size'] = 1000000;
		$config['upload_path'] = './public/images/uploads/shipmentpicture/';
		

		if($photo != '')
		{
			$config['file_name'] = $photo;
			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('picture'))
			{
				echo $this->upload->display_error();
				exit();
			}
			else
			{
				$data = $this->upload->data();
				$photo = $data['orig_name'];

				$this->Appmodel->set_table('shipment_picture');
				$this->Appmodel->insert(array('shipment_id' => $shipmentid, 'shipment_picture' => $photo));
				
			}
		}
		
		$data['memberid'] = $shipmentid;
		$this->silentpost($shipmentid);
		
		
		return $shipmentid;
	}
	
	private function silentpost($shipment_id){
	
		$response = array();

		//dummy

		$this->Appmodel->set_table('shipment');
		$shipment_detail = $this->Appmodel->fetch_row(NULL,array('shipment_id' => $shipment_id));

		if(empty($shipment_detail)){
			$response['status'] = 'EMPTY';
			$response['message'] = 'Shipment not found';
			$this->response($response, 200);
			exit();
		}

		$start_state_code = $shipment_detail->pickup_province_code;
		$end_state_code = $shipment_detail->deliver_province_code;

		$start_zip_code = $shipment_detail->pickup_zip_code;
		$end_zip_code = $shipment_detail->deliver_zip_code;

		$this->Appmodel->set_table('member_route');
		$routes = $this->Appmodel->custom_rows(NULL,"FIND_IN_SET( '$start_state_code' , `state_list` ) 
			OR FIND_IN_SET('$end_state_code', `state_list`) 
			OR FIND_IN_SET('$start_zip_code', `start_zip_code`) 
			OR FIND_IN_SET('$end_zip_code', `end_zip_code`) 
			");

		$member_list = array(); 
		foreach($routes as $route){
			//member yg setting route nya cocok dengan shipment skrg
			$member_list[] = $route->member_id;
		}
		

		$this->Appmodel->set_table('notification');
		$uniq = array_unique($member_list);
		foreach($uniq as $key => $val){
			 
			$notification_row = array(
				'status' => 'O',
				'member_id' => $val,
				'shipment_id' => $shipment_id
			);
			$this->Appmodel->insert($notification_row);
			
		}
		
		return;
	}

	private function _save_shipper_data()
	{
		// Get driver data
		$member_data = array('email' => $this->form_validation->set_value('email'), 
							 'username' => $this->form_validation->set_value('username'), 
							 'password' => $this->form_validation->set_value('pass1'), 
							 'is_driver' => false,
							);

		// Save driver
		$this->Appmodel->set_table('member');
		return $this->Appmodel->insert($member_data);
	}
	
	private function _post_data($shipment_id){
		
		$shipmentid = $this->input->post('id');
		
		$pickup_location = $this->_get_lat_lon(
			$this->input->post('pickup_province_code'), 
			$this->input->post('pickup_city'), 
			$this->input->post('pickup_kecamatan'), 
			$this->input->post('pickup_kelurahan'),
			$this->input->post('kodepos_asal')
		);
		
		
		
		$deliver_location = $this->_get_lat_lon(
			$this->input->post('deliver_province_code'), 
			$this->input->post('deliver_city'), 
			$this->input->post('deliver_kecamatan'), 
			$this->input->post('deliver_kelurahan'),
			$this->input->post('kodepos_tujuan')
		);
		
		$shipment_data = array(
			'name' => $this->input->post('shipment_name'), 
			'type' => $this->input->post('type'),
			'detail' => $this->input->post('detail'),
			'price_offered' => $this->input->post('price_offered'),
			'pickup_address' => $this->input->post('pickup_address'),
			'pickup_city' => $this->input->post('pickup_city'),
			'pickup_province_code' => $this->input->post('pickup_province_code'),
			'pickup_zip_code' => $this->input->post('kodepos_asal'),
			'pickup_max_date' => $this->input->post('max_date'),
			'pickup_latitude' => $pickup_location['latitude'],
			'pickup_longitude' => $pickup_location['longitude'],
			'deliver_address' => $this->input->post('deliver_address'),
			'deliver_city' => $this->input->post('deliver_city'),
			'deliver_province_code' => $this->input->post('deliver_province_code'),
			'deliver_zip_code' => $this->input->post('kodepos_tujuan'),
			'deliver_max_date' => $this->input->post('max_date'),
			'deliver_latitude' => $deliver_location['latitude'],
			'deliver_longitude' => $deliver_location['longitude']
		);
		
		$this->Appmodel->set_table('shipment');
		$this->Appmodel->update($shipment_data,array('shipment_id' => $shipmentid));
		
		$request['photo'] = (!empty($_FILES['picture']['tmp_name'])) ? time() .'-'.$shipmentid : '';
		$photo = $request['photo'];

		/** upload configuration */
		$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
		$config['max_size'] = 1000000;
		$config['upload_path'] = './public/images/uploads/shipmentpicture/';

		if($photo != '')
		{
			$config['file_name'] = $photo;
			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('picture'))
			{
				echo $this->upload->display_error();
				exit();
			}
			else
			{
				$data = $this->upload->data();
				$photo = $data['orig_name'];

				$this->Appmodel->set_table('shipment_picture');
				$this->Appmodel->insert(array('shipment_id' => $shipment_id, 'shipment_picture' => $photo));
				
			}
		}
		
		redirect('/dashboard');
		
	}
	
	function edit($shipmentid=NULL){
		
		if(!isset($shipmentid)){
			redirect(site_url('dashboard'));
		}
		
		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			$this->_post_data($shipmentid);
			print_r($_REQUEST);exit();
		}
		
		$data = array('page_content' => 'edit',
			'active_nav' => 'home');
			
		$this->Appmodel->set_table('shipment_type');
		$data['kategori'] = $this->Appmodel->fetch_rows();

		$this->Appmodel->set_table('province_code');
		$data['provinces'] = $this->Appmodel->fetch_rows();
		
		$row_detail = $this->shipment_model->get_shipment_detail($shipmentid);
		$data['shipment'] = $row_detail;
		
		$pictures = $this->shipment_model->get_shipment_pictures_2($shipmentid);
		$data['pictures'] = $pictures;
		
		$this->load->view($this->base_view, $data);	
		
	}
	
	function change_picture($picid=NULL){
		
		if(!isset($picid)){
			redirect('dashboard');
		}
		
		$this->Appmodel->set_table('shipment_picture');
		$row = $this->Appmodel->fetch_row(NULL, array('picture_id' => $picid));
		
		$this->Appmodel->delete(array('picture_id' => $picid));
		
		redirect(site_url('shipment/edit/' . $row->shipment_id));
		
	}
	
	function delete($shipmentid){
		
		if(!isset($shipmentid)){
			redirect(site_url('dashboard'));
		}
		
		$this->shipment_model->drop_shipment($shipmentid);
		
		redirect('dashboard');
	}
	
	function maps(){
		$data = array('page_content' => 'maps',
			  		  'active_nav' => 'shipment');
							
							
		//default
		$shipment_map = array();
		
		$this->Appmodel->set_table('shipment');
		$shipments = $this->shipment_model->get_shipment_maps();
		
		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			$return = $this->_maps_submit();
			$shipments = $return['shipments'];
			$data['params'] = $return['params'];
			$data['selected_filter'] = $this->input->post('filter-control');
			
		}
		else
		{
			$data['selected_filter'] = '';
			$data['params'] = array();
		}
		
		$total_shipments = sizeof($shipments);
		
		$pickup_center_lat = 0.0;
		$pickup_center_lon = 0.0;
		
		foreach($shipments as $shipment){
			
			$shipment_map[] = array(
				'<a target="_blank" href="'.site_url('shipment/view/' . $shipment->shipment_id).'">' . $shipment->name .'</a>' ,
				$shipment->pickup_latitude,
				$shipment->pickup_longitude,
				$shipment->shipment_id
			);
		}
		
		$data['maps_center']['latitude'] = -3.099336;
		$data['maps_center']['longitude'] = 114.207564;
		
		$this->Appmodel->set_table('shipment_type');
		$data['kategori'] = $this->Appmodel->fetch_rows();
		
		$data['shipments'] = $shipment_map;
		$this->load->view('maps_layout', $data);	
	}
	
	function _maps_submit(){
		
		$filter = $this->input->post('filter-control');
		
		switch($filter)
		{
			
			case 'asal' :
				
				$params = array(
					'kota_asal' => $this->input->post('kota_asal'),
					'kode_pos_asal' => $this->input->post('kode_pos_asal')
				);
				
				$shipments = $this->shipment_model->get_maps_submit_asal($params);
				break;
				
			case 'tujuan': 
				$params = array(
					'kota_tujuan' => $this->input->post('kota_tujuan'),
					'kode_pos_tujuan' => $this->input->post('kode_pos_tujuan')
				);
				
				$shipments = $this->shipment_model->get_maps_submit_tujuan($params);
				break;
			case 'kategori':
			
				$params = array();
				
				foreach($this->input->post('kategory') as $key => $cat)
				{
					$params[] = $key;
				}
				
				$shipments = $this->shipment_model->get_maps_submit_kategori($params);
				break;
				
			case 'lokasi':
			
				$params = array(
					'address' => $this->input->post('alamat_sekarang'),
					'city' => $this->input->post('kota_sekarang'),
					'postal_code' => $this->input->post('postal_code'),
					'radius_sekarang' => $this->input->post('radius_sekarang')
				);
				
				$my_location = $this->_find_my_position($params);
				
				$location = array(
					'latitude' => $my_location['latitude'],
					'longitude' => $my_location['longitude'],
					'radius' => $params['radius_sekarang']
				);
				
				$shipments = $this->shipment_model->get_maps_submit_radius($location);
				break;
		}
		
		$return['shipments'] = $shipments;
		$return['params'] = $params;
		
		return $return;
		
	}
	
	function _find_my_position($params){
		
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($params['address'])."&components=country:ID|city=".$params['city']."|postal_code:".$params['postal_code']."&sensor=false";
		$data = @file_get_contents($url);
		$jsondata = json_decode($data,true);

		if($jsondata['status'] != 'OK')
		{
			
			$position['latitude'] = '0.000000';
			$position['longitude'] = '0.000000';

		} else {

			$location = $jsondata['results'][0]['geometry']['location'];
			$position['latitude'] = $location['lat'];
			$position['longitude'] = $location['lng'];

		}
		
		return $position;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */



<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipment_model extends CI_Model {
  
  var $table = 'order_address';

	function __construct() {
		parent::__construct();
	}
	
	function get_bidder_list($shipment_id){
		
		return $this->db->where(array('shipment_id' => $shipment_id, 'status' => 'A'))
			->join('member', "notification.member_id = member.member_id")
			->get('notification')
			->result();
		
	}
	
	function get_list($offset=0, $limit=0){
		return $this->db->where("pickup_max_date >= NOW()", NULL, FALSE)
			->order_by('date_added', "DESC")
			->join('notification', "notification.status = 'O' OR notification.status = 'A'")
			->get('shipment', $offset, $limit)
			->result();
	}
	
	function get_shipment_pictures($shipmentid){
		return $this->db->where('shipment_id', $shipmentid)
			->order_by('date_uploaded DESC')
			->limit(1)
			->get('shipment_picture')
			->result();
	}
	
	function get_location($reg1, $reg2, $reg3, $local, $zip){
		return $this->db->select('latitude, longitude')
			->where(array('region1' => $reg1, 'region2' => $reg2, 'region3' => $reg3, 'locality' => $local, 'postcode' => $zip))
			->get('postal_code')
			->row();
	}
	
	function get_name_province($kode){
		return $this->db->where('province_code', $kode)
			->get('province_code')
			->row();
	}
	
	function get_shipment_detail($id){
		return $this->db->where('shipment_id', $id)
			->get('shipment')
			->row();
	}
	
	function get_shipment_pictures_2($id){
		return $this->db->where('shipment_id', $id)
			->get('shipment_picture')
			->result();
	}
	
	function drop_shipment($id){
		
		$this->db->where('shipment_id', $id)
			->delete('notification');
			
		$this->db->where('shipment_id', $id)
			->delete('shipment_picture');
			
		$this->db->where('shipment_id', $id)
			->delete('shipment');
		
	}
	
	function get_shipment_maps(){
		return $this->db->select('shipment_id, name, detail, pickup_latitude, pickup_longitude, deliver_latitude, deliver_longitude, type')
			->where('is_accept_driver', 0)
			->get('shipment')
			->result();
	}
	
	function get_maps_submit_asal($params){
		
		$kota = $params['kota_asal'];
		$kodepos = $params['kode_pos_asal'];
		
		return $this->db->select('shipment_id, name, detail, pickup_latitude, pickup_longitude, deliver_latitude, deliver_longitude, type')
			->where('pickup_city', $kota)
			->where('pickup_zip_code', $kodepos)
			->get('shipment')
			->result();
	
	}
	
	function get_maps_submit_tujuan($params){
		
		$kota = $params['kota_tujuan'];
		$kodepos = $params['kode_pos_tujuan'];
		
		return $this->db->select('shipment_id, name, detail, pickup_latitude, pickup_longitude, deliver_latitude, deliver_longitude, type')
			->where('deliver_city', $kota)
			->where('deliver_zip_code', $kodepos)
			->get('shipment')
			->result();
		
	}
	
	function get_maps_submit_kategori($params){
		
		$type = join(',', $params);
		
		return $this->db->select('shipment_id, name, detail, pickup_latitude, pickup_longitude, deliver_latitude, deliver_longitude, type')
			->where("type	IN ($type)",NULL, FALSE)
			->get('shipment')
			->result();
	}
	
	function get_maps_submit_radius($params){
	
		//3959 : earth radius
		//note, this is in kilometer
		
		$latitude = $params['latitude'];
		$longitude = $params['longitude'];
		$radius = $params['radius'];
		$earth_radius = 6371;
		
		return $this->db->query("
			SELECT shipment_id,name,type,detail,pickup_city,deliver_city,pickup_latitude, pickup_longitude, deliver_latitude, deliver_longitude, date_added, ( $earth_radius * ACOS( COS( RADIANS($latitude) ) * COS( RADIANS( pickup_latitude ) ) 
				* COS( RADIANS( pickup_longitude ) - RADIANS($longitude) ) + SIN( RADIANS($latitude) ) * SIN(RADIANS(pickup_latitude)) ) ) AS distance 
				FROM shipment WHERE is_accept_driver = 0
				HAVING distance < $radius 
				ORDER BY distance ASC;
		")->result();
		
	}
	
	

}
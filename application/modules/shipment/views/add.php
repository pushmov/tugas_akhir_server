
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>public/js/autosuggest/ajax.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>public/js/autosuggest/tools.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>public/js/autosuggest/autosuggest.js"></script>
<style type="text/css">
<!--
ul.searchList{
	z-index:2;
}
.searchList {
	margin: 0px;
	padding: 0px;
	list-style-type: none;
	position: absolute;
	width: 97%;
	height: 160px;
	overflow-y:auto;
	overflow:-moz-auto-vertical
}
.wrapSearch {
}
#input {
	width: 220px;
	padding: 2px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
.searchList li {
	display: block;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #D6D6D6;
	width: 98%;
	background-color: #F9F9F9;
}
.searchList li a{
	display: block;
	color: #006;
	text-decoration: none;
	background-color: #F9F9F9;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 8px;
}
.searchList li a:hover{
	color: #FFF;
	background-color: #555;
}
.listWrap {
	visibility: hidden;
}
.holder{position:relative}
i.fa-ban{display:none}

-->
</style>
<span id="base_url" alt="<?php echo base_url(); ?>"></span>

<section class="shipment">
	<div class="container">
		<h3>Posting Shipment</h3>
		<div class="row">
		<div class="col-md-8">
			<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
				<?php if (!$this->auth->is_logged_in()): ?>
					<h4>Informasi Akun</h4>
					<fieldset>
						<div class="form-group">
					    	<label class="col-sm-2 control-label">Email<span style="color:#ff0000">*</span></label>
					    	<div class="col-sm-10 holder">
					      		<input name="email" type="email" class="form-control required" id="email-check">
										<i class="fa fa-check-circle fa-lg" id="notif" style="position:absolute;right:25px;top:10px;display:none"></i>
										<span id="email-used" style="display:none;right:0;color:#ff0000">Email telah digunakan</span>
					    	</div>
					  	</div>

						<div class="form-group">
					    	<label class="col-sm-2 control-label">Username<span style="color:#ff0000">*</span></label>
					    	<div class="col-sm-10 holder">
					      		<input name="username" type="text" class="form-control required">
										<i class="fa fa-ban fa-lg" style="position:absolute;right:25px;top:10px;color:#ff0000"></i>
					    	</div>
					  	</div>

					  	<div class="form-group">
					    	<label class="col-sm-2 control-label">Password<span style="color:#ff0000">*</span></label>
					    	<div class="col-sm-10 holder">
					      		<input name="pass1" type="password" class="form-control required">
										<i class="fa fa-ban fa-lg" style="position:absolute;right:25px;top:10px;color:#ff0000"></i>
					    	</div>
					  	</div>

					  	<div class="form-group">
					    	<label class="col-sm-2 control-label">Ketik Ulang Password<span style="color:#ff0000">*</span></label>
					    	<div class="col-sm-10 holder">
					      		<input name="pass2" type="password" class="form-control required">
										<i class="fa fa-ban fa-lg" style="position:absolute;right:25px;top:10px;color:#ff0000"></i>
					    	</div>
					  	</div>

				  		<!-- <p class="text-center">Sudah terdaftar? <a href="<?php echo site_url('login') ?>">login disini</a></p> -->
					</fieldset>
				<?php endif ?>
				
				<h4>Informasi Shipment</h4>
				
				<fieldset>
				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Nama Shipment<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				      		<input name="shipment_name" type="text" class="form-control required">
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Kategori</label>
				    	<div class="col-sm-10">
				    		<select name="type" id="chained-parent" class="form-control">
									<option value="">--</option>
					    		<?php
					    			foreach ($kategori as $kat) {
					    				echo "<option value='".$kat->type."''>".$kat->shipment_type_name."</option>";
					    			 } 
					    		?>
				    		</select>
				    	</div>
				  	</div>
						
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Kategori Spesifik</label>
				    	<div class="col-sm-10">
				    		<select name="subtype" id="chained-child" class="form-control">
					    		<option value="">--</option>
				    		</select>
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Keterangan<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="detail" cols="30" rows="5" class="form-control required"></textarea>
				    	</div>
				  	</div>

				  	

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Harga yang ditawarkan</label>
				  		<div class="col-sm-10">
				  			<input type="text" name="price_offered" class="form-control number-field">
				  		</div>
				  	</div>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Tanggal Expired<span style="color:#ff0000">*</span></label>
				  		<div class="col-sm-10 holder">
				  			<input type="text" name="max_date" class="form-control date-field required">
				  		</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Alamat Pengambilan Barang<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="pickup_address" cols="30" rows="3" class="form-control required"></textarea>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Provinsi</label>
				    	<div class="col-sm-10">
				    		<select name="pickup_province_code" class="form-control" id="pickup">
					    		<?php foreach ($provinces as $province): ?>
					    			<option value="<?php echo $province->province_code ?>"><?php echo $province->province_name ?></option>
					    		<?php endforeach ?>
				    		</select>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kota/Kabupaten<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
								<input type="text" class="form-control number-field required" name="pickup_city" id="pickup_city" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap1', 'searchList1', event, '0', 'kodepos_asal', 'pickup');" placeholder="Ketikkan kota/Kabupaten Asal...">
								<div class="listWrap" id="listWrap1">
									<ul class="searchList" id="searchList1">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kecamatan</label>
				    	<div class="col-sm-10">
				    		<input type="text" class="form-control number-field" name="pickup_kecamatan" id="pickup_kecamatan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap2', 'searchList2', event, '1', 'kodepos_asal', 'pickup');" placeholder="Ketikkan kecamatan Asal...">
								<div class="listWrap" id="listWrap2">
									<ul class="searchList" id="searchList2">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kelurahan</label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field" name="pickup_kelurahan" id="pickup_kelurahan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap3', 'searchList3', event, '2', 'kodepos_asal', 'pickup');" placeholder="Ketikkan kelurahan Asal...">
								<div class="listWrap" id="listWrap3">
									<ul class="searchList" id="searchList3">
									</ul>
								</div>
				    	</div>
				  	</div>
						
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kode Pos<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field required" name="kodepos_asal" id="kodepos_asal" value="">
				    	</div>
				  	</div>
					<br></br>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Alamat Tujuan<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="deliver_address" cols="30" rows="3" class="form-control required"></textarea>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Provinsi</label>
				    	<div class="col-sm-10">
				    		<select name="deliver_province_code" class="form-control" id="deliver">
					    		<?php foreach ($provinces as $province): ?>
					    			<option value="<?php echo $province->province_code ?>"><?php echo $province->province_name ?></option>
					    		<?php endforeach ?>
				    		</select>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kota/Kabupaten<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field required" name="deliver_city" id="deliver_city" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap11', 'searchList11', event, '0', 'kodepos_tujuan', 'deliver');" placeholder="Ketikkan kota/Kabupaten tujuan...">
								<div class="listWrap" id="listWrap11">
									<ul class="searchList" id="searchList11">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kecamatan</label>
				    	<div class="col-sm-10">
				    		<input type="text" class="form-control number-field" name="deliver_kecamatan" id="deliver_kecamatan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap12', 'searchList12', event, '1', 'kodepos_tujuan', 'deliver');" placeholder="Ketikkan kecamatan tujuan...">
								<div class="listWrap" id="listWrap12">
									<ul class="searchList" id="searchList12">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kelurahan</label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field" name="deliver_kelurahan" id="deliver_kelurahan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap13', 'searchList13', event, '2', 'kodepos_tujuan', 'deliver');" placeholder="Ketikkan kelurahan tujuan...">
								<div class="listWrap" id="listWrap13">
									<ul class="searchList" id="searchList13">
									</ul>
								</div>
				    	</div>
				    	
				  	</div>
						
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kode Pos<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field required" name="kodepos_tujuan" id="kodepos_tujuan" value="">
				    	</div>
				  	</div>
					<br></br>
				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Upload Foto</label>
				    	<div class="col-sm-10">
				    		<input type="file" name="picture" id="picture">
				    	</div>
				  	</div>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10" id="notification" style="display:none">
					      <span style="color:#ff0000">Field dengan tanda * harus diisi</span>
					    </div>
					</div>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-primary" id="submit">Submit</button>
					    </div>
					</div>
				</fieldset>
			  	
			</form>
		</div>
		</div>
	</div>
</section>

<script src="<?php echo base_url(); ?>public/js/jquery.chained.remote.js"></script>
<script>
	$("#chained-child").remoteChained("#chained-parent", "<?php echo site_url('ajax/chain/'); ?>");
	
	$('#email-check').change(function(){
		$.get('<?php echo site_url('ajax/checkemail/'); ?>' + '/' + $(this).val(), function(data){
			console.log(data);
			$('#email-used').hide();
			var obj = $.parseJSON( data );
			if(obj.success == 'success'){
				$('#notif').removeClass('fa-check-circle').addClass('fa-ban').css({'color' : '#ff0000'}).show();
				$('#email-used').show();
				$('#submit').hide();
			} else {
				$('#notif').removeClass('fa-ban').addClass('fa-check-circle').css({'color' : '#2D8E00'}).show();
				$('#submit').show();
				$('#email-used').hide();
			}
		});
	});
	
	$('#submit').click(function(){
		$('#email-used').hide();
		var isvalid = true;
		$('.required').each(function(){
			var value = $(this).val();
			
			if(value == ''){
				isvalid = false;
				console.log();
				$('#notification').show();
				return isvalid;
			}
		});
		
		return isvalid;
	})
</script>

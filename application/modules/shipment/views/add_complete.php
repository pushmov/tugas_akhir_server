<section class="shipment">
	<div class="container">
		<h3>Posting Shipment Selesai!</h3>
		<div class="row">
			<div class="col-md-8">
				<p>Terima kasih telah menggunakan layanan kami</p>
				<p><a href="<?php echo site_url('shipment/view/'.$shipment_id) ?>">Lihat info shipment anda</a></p>

				<a href="<?php echo site_url('shipment/add') ?>" class="btn btn-primary">Posting Shipment Lainnya</a>
				<a href="<?php echo site_url() ?>" class="btn btn-primary">Kembali Ke Menu Utama</a>
			</div>
		</div>
	</div>
</section>

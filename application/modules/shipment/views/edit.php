
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>public/js/autosuggest/ajax.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>public/js/autosuggest/tools.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() ?>public/js/autosuggest/autosuggest.js"></script>
<style type="text/css">
<!--
ul.searchList{
	z-index:2;
}
.searchList {
	margin: 0px;
	padding: 0px;
	list-style-type: none;
	position: absolute;
	width: 97%;
	height: 160px;
	overflow-y:auto;
	overflow:-moz-auto-vertical
}
.wrapSearch {
}
#input {
	width: 220px;
	padding: 2px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
.searchList li {
	display: block;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #D6D6D6;
	width: 98%;
	background-color: #F9F9F9;
}
.searchList li a{
	display: block;
	color: #006;
	text-decoration: none;
	background-color: #F9F9F9;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 8px;
}
.searchList li a:hover{
	color: #FFF;
	background-color: #555;
}
.listWrap {
	visibility: hidden;
}
.holder{position:relative}
i.fa-ban{display:none}

-->
</style>
<span id="base_url" alt="<?php echo base_url(); ?>"></span>

<section class="shipment">
	<div class="container">
		<h3>Edit Shipment</h3>
		<div class="row">
		<div class="col-md-8">
			<form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('shipment/edit/' . $shipment->shipment_id); ?>" enctype="multipart/form-data">
				
				<h4>Informasi Shipment</h4>
				
				<fieldset>
				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Nama Shipment<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				      		<input name="shipment_name" type="text" class="form-control required" value="<?php echo $shipment->name; ?>">
									<input name="source" type="hidden" class="form-control required" value="edit">
									<input name="id" type="hidden" class="form-control required" value="<?php echo $shipment->shipment_id; ?>">
									
									
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Keterangan<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="detail" cols="30" rows="5" class="form-control required"><?php echo $shipment->detail; ?></textarea>
				    	</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Kategori</label>
				    	<div class="col-sm-10">
				    		<select name="type" class="form-control" id="type">
					    		<?php
					    			foreach ($kategori as $kat) {
					    				echo "<option value='".$kat->type."''>".$kat->shipment_type_name."</option>";
					    			 } 
					    		?>
				    		</select>
				    	</div>
				  	</div>
						<script>
							$('#type').val('<?php echo $shipment->type; ?>');
						</script>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Harga yang ditawarkan</label>
				  		<div class="col-sm-10">
				  			<input type="text" name="price_offered" class="form-control number-field" value="<?php echo $shipment->price_offered; ?>">
				  		</div>
				  	</div>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Tanggal Expired<span style="color:#ff0000">*</span></label>
				  		<div class="col-sm-10 holder">
				  			<input type="text" name="max_date" class="form-control date-field required" value="<?php echo date('Y-m-d', strtotime($shipment->pickup_max_date)); ?>">
				  		</div>
				  	</div>

				  	<div class="form-group">
				    	<label class="col-sm-2 control-label">Alamat Pengambilan Barang<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="pickup_address" cols="30" rows="3" class="form-control required"><?php echo $shipment->pickup_address; ?></textarea>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Provinsi</label>
				    	<div class="col-sm-10">
				    		<select name="pickup_province_code" class="form-control" id="pickup">
					    		<?php foreach ($provinces as $province): ?>
					    			<option value="<?php echo $province->province_code ?>"><?php echo $province->province_name ?></option>
					    		<?php endforeach ?>
				    		</select>
								
								<script>
									$('#pickup').val('<?php echo $shipment->pickup_province_code; ?>');
								</script>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kota/Kabupaten<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
								<input type="text" class="form-control number-field required" name="pickup_city" id="pickup_city" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap1', 'searchList1', event, '0', 'kodepos_asal', 'pickup');" placeholder="Ketikkan kota/Kabupaten Asal..." value="<?php echo $shipment->pickup_city; ?>">
								<div class="listWrap" id="listWrap1">
									<ul class="searchList" id="searchList1">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kecamatan</label>
				    	<div class="col-sm-10">
				    		<input type="text" class="form-control number-field" name="pickup_kecamatan" id="pickup_kecamatan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap2', 'searchList2', event, '1', 'kodepos_asal', 'pickup');" placeholder="Ketikkan kecamatan Asal..." value="">
								<div class="listWrap" id="listWrap2">
									<ul class="searchList" id="searchList2">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kelurahan</label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field" name="pickup_kelurahan" id="pickup_kelurahan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap3', 'searchList3', event, '2', 'kodepos_asal', 'pickup');" placeholder="Ketikkan kelurahan Asal..." >
								<div class="listWrap" id="listWrap3">
									<ul class="searchList" id="searchList3">
									</ul>
								</div>
				    	</div>
				  	</div>
						
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kode Pos<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field required" name="kodepos_asal" id="kodepos_asal" value="<?php echo $shipment->pickup_zip_code; ?>">
				    	</div>
				  	</div>
					<br></br>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Alamat Tujuan<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<textarea name="deliver_address" cols="30" rows="3" class="form-control required"><?php echo $shipment->deliver_address; ?></textarea>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Provinsi</label>
				    	<div class="col-sm-10">
				    		<select name="deliver_province_code" class="form-control" id="deliver">
					    		<?php foreach ($provinces as $province): ?>
					    			<option value="<?php echo $province->province_code ?>"><?php echo $province->province_name ?></option>
					    		<?php endforeach ?>
				    		</select>
								<script>
									$('#deliver').val('<?php echo $shipment->deliver_province_code; ?>');
								</script>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kota/Kabupaten<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field required" name="deliver_city" id="deliver_city" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap11', 'searchList11', event, '0', 'kodepos_tujuan', 'deliver');" placeholder="Ketikkan kota/Kabupaten tujuan..." value="<?php echo $shipment->pickup_city; ?>">
								<div class="listWrap" id="listWrap11">
									<ul class="searchList" id="searchList11">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kecamatan</label>
				    	<div class="col-sm-10">
				    		<input type="text" class="form-control number-field" name="deliver_kecamatan" id="deliver_kecamatan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap12', 'searchList12', event, '1', 'kodepos_tujuan', 'deliver');" placeholder="Ketikkan kecamatan tujuan...">
								<div class="listWrap" id="listWrap12">
									<ul class="searchList" id="searchList12">
									</ul>
								</div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kelurahan</label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field" name="deliver_kelurahan" id="deliver_kelurahan" autocomplete="off" onkeyup="autoSuggestNew(this.id, 'listWrap13', 'searchList13', event, '2', 'kodepos_tujuan', 'deliver');" placeholder="Ketikkan kelurahan tujuan...">
								<div class="listWrap" id="listWrap13">
									<ul class="searchList" id="searchList13">
									</ul>
								</div>
				    	</div>
				    	
				  	</div>
						
					<div class="form-group">
				    	<label class="col-sm-2 control-label">Kode Pos<span style="color:#ff0000">*</span></label>
				    	<div class="col-sm-10 holder">
				    		<input type="text" class="form-control number-field required" name="kodepos_tujuan" id="kodepos_tujuan" value="<?php echo $shipment->deliver_zip_code; ?>">
				    	</div>
				  	</div>
					<br></br>
				  	
						<?php if(empty($pictures)) : ?>
						<div class="form-group">
				    	<label class="col-sm-2 control-label">Upload Foto</label>
				    	<div class="col-sm-10">
				    		<input type="file" name="picture" >
				    	</div>
				  	</div>
						
						<?php else : ?>
						
						<div class="form-group">
							<?php foreach($pictures as $pic) : ?>
							
							<label class="col-sm-2 control-label">Upload Foto</label>
							<div class="col-sm-10" style="margin:10px 0">
								<img src="<?php echo base_url(); ?>public/images/uploads/shipmentpicture/<?php echo $pic->shipment_picture; ?>" width="250" height="250">
								<div><a href="<?php echo site_url('shipment/change_picture/' . $pic->picture_id); ?>">Change Pic</a></div>
							</div>
							<?php endforeach; ?>
				    	
				  	</div>
						
						<?php endif; ?>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10" id="notification" style="display:none">
					      <span style="color:#ff0000">Field dengan tanda * harus diisi</span>
					    </div>
					</div>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-primary" id="submit">Submit</button>
					    </div>
					</div>
				</fieldset>
			  	
			</form>
		</div>
		</div>
	</div>
</section>

<script>
	$('#email-check').change(function(){
		$.get('<?php echo site_url('ajax/checkemail/'); ?>' + '/' + $(this).val(), function(data){
			console.log(data);
			var obj = $.parseJSON( data );
			if(obj.success == 'success'){
				$('#notif').removeClass('fa-check-circle').addClass('fa-ban').css({'color' : '#ff0000'}).show();
			} else {
				$('#notif').removeClass('fa-ban').addClass('fa-check-circle').css({'color' : '#2D8E00'}).show();
			}
		});
	});
	
	$('#submit').click(function(){
		var isvalid = true;
		$('.required').each(function(){
			var value = $(this).val();
			
			if(value == ''){
				isvalid = false;
				console.log();
				$('#notification').show();
				return isvalid;
			}
		});
		
		return isvalid;
	})
</script>

<section class="ct-find">
	<div class="container">
			
			<div class="clearfix">
				<div class="pull-left">
					<h3>Daftar Paket Tersedia :</h3>
					
					<!--
					<div>
						Urutkan berdasarkan : 
						<select name="sort">
							<option>Tanggal Posting</option>
							<option>Tanggal Expired</option>
							<option>Jumlah Bid</option>
						</select>
					</div>
					-->
				</div>
				<div class="pull-right">
					<a href="<?php echo site_url('shipment/maps/');?>"><i class="fa fa-map-marker fa-lg" style="margin-right:10px;color:#D94D43"></i>View in Maps</a>
				</div>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nama</th>
						<th>Kategori</th>
						<th>Asal</th>
						<th>Tujuan</th>
						<th>Diposting</th>
						<th>Kadaluwarsa</th>
						<th>Aksi</th>
						<th>Bid</th>
					</tr>
				</thead>
				<tbody>

					<?php foreach ($shipments as $row):?>
						<tr>
							<td><?php echo $row->shipment_id ?></td>
							<td>
								<h5><a href="<?php echo site_url('shipment/view/'.$row->shipment_id); ?>"><strong><?php echo $row->name ?></strong></a></h5>
								
							</td>
							<td>
								<h6><?php echo isset($this->shipment_lib->get_type_name($row->type)->shipment_type_name) ? $this->shipment_lib->get_type_name($row->type)->shipment_type_name : 'tidak tersedia'; ?></h6>
								<h6><?php echo isset($this->shipment_lib->get_sub_type_name($row->sub_type)->sub_name) ? $this->shipment_lib->get_sub_type_name($row->sub_type)->sub_name : '-'; ?></h6>
							</td>
							<td><?php echo $row->pickup_city ?></td>
							<td><?php echo $row->deliver_city ?></td>
							<td><?php echo $row->date_added ?></td>
							<td><?php echo $row->pickup_max_date ?></td>
							<td><a href="<?php echo site_url('shipment/view/'.$row->shipment_id) ?>" class="btn btn-success">detail</a></td>
							<td><?php echo $this->shipment_lib->get_bid_count($row->shipment_id) ?></td>
						</tr>
					<?php endforeach ?>
					
				</tbody>
			</table>

			<nav class="page">
				<?php echo $pagination ?>
			</nav>
															
	</div>
</section>
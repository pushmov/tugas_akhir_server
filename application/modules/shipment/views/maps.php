
<span id="base_url" alt="<?php echo base_url(); ?>"></span>
<section class="ct-find">
	<div class="container">
			
			
			
			
			<div class="clearfix">
				<div class="pull-left">
					<h3>Cari Shipment :</h3>
				</div>
				<div class="pull-right">
					<a href="<?php echo site_url('shipment/find/');?>"><i class="fa fa-list fa-lg" style="margin-right:10px;color:#D94D43"></i>View in List</a>
				</div>
			</div>
			
			<div class="row clearfix maps-wrapper">
				<form action="<?php echo site_url('shipment/maps/'); ?>" method="POST" name="" >
					<div class="col-md-3">
						<h3><input type="radio" value="asal" alt="asal" <?php echo ($selected_filter == 'asal') ? 'checked' : ''; ?> name="filter-control" style="margin-left:8px;"> Berdasarkan Tempat Asal</h3>
						<fieldset class="asal">
							<!--
							<div class="form-group">
								<input type="text" name="address_asal" class="form-control number-field" value="" placeholder="address...">
							</div>
							-->
							
							<div class="form-group">
								<input type="text" name="kota_asal" class="form-control number-field" value="<?php echo isset($params['kota_asal']) ? $params['kota_asal'] : ''; ?>" placeholder="kota/kabupaten...">
							</div>
							
							<div class="form-group">
								<input type="text" name="kode_pos_asal" class="form-control number-field" value="<?php echo isset($params['kode_pos_asal']) ? $params['kode_pos_asal'] : ''; ?>" placeholder="kode pos...">
							</div>
							
						</fieldset>
					</div>
					<div class="col-md-3">
						<h3><input type="radio" value="tujuan" alt="tujuan" <?php echo ($selected_filter == 'tujuan') ? 'checked' : ''; ?> name="filter-control" style="margin-left:8px;"> Berdasarkan Tempat Tujuan</h3>
						<fieldset class="tujuan">
							<!--
							<div class="form-group">
								<input type="text" name="address_tujuan" class="form-control number-field" value="" placeholder="address...">
							</div>
							-->
							
							<div class="form-group">
								<input type="text" name="kota_tujuan" class="form-control number-field" value="<?php echo isset($params['kota_tujuan']) ? $params['kota_tujuan'] : ''; ?>" placeholder="kota/kabupaten...">
							</div>
							
							<div class="form-group">
								<input type="text" name="kode_pos_tujuan" class="form-control number-field" value="<?php echo isset($params['kota_tujuan']) ? $params['kode_pos_tujuan'] : ''; ?>" placeholder="kode pos...">
							</div>
							
						</fieldset>
					</div>
					<div class="col-md-3">
						<h3><input type="radio" <?php echo ($selected_filter == 'kategori') ? 'checked' : ''; ?> name="filter-control" alt="kategori" value="kategori" style="margin-left:8px;"> Berdasarkan Kategori</h3>
						<fieldset class="kategori">
							<div class="clearfix">
								<div>
									<?php foreach($kategori as $kat ) : ?>
									<div class="form-group">
										<input type="checkbox" <?php echo (in_array($kat->type, $params)) ? 'checked' : ''; ?> name="kategory[<?php echo $kat->type; ?>]" ><span style="margin-left:9px;"><?php echo $kat->shipment_type_name; ?></span>
									</div>
									<?php endforeach; ?>
								</div>
								
								
							</div>
						</fieldset>
					</div>
					<div class="col-md-3">
						<h3><input type="radio" <?php echo ($selected_filter == 'lokasi') ? 'checked' : ''; ?> name="filter-control" alt="lokasi" value="lokasi" style="margin-left:8px;"> Berdasarkan Lokasi Saat ini</h3>
						<fieldset class="lokasi">
							<div class="form-group">
								<input type="text" name="alamat_sekarang" class="form-control number-field" value="<?php echo isset($params['address']) ? $params['address'] : '' ;?>" placeholder="alamat...">
							</div>
							
							<div class="form-group">
								<input type="text" name="kota_sekarang" class="form-control number-field" value="<?php echo isset($params['city']) ? $params['city'] : '' ;?>" placeholder="kota/kabupaten...">
							</div>
							
							<div class="form-group">
								<input type="text" name="postal_code" class="form-control number-field" value="<?php echo isset($params['postal_code']) ? $params['postal_code'] : '' ;?>" placeholder="kodepos...">
							</div>
							
							<div class="form-group">
								<select name="radius_sekarang" class="form-control" id="radius_sekarang">
									<option value="">Radius...</option>
									<option value="5">5 km</option>
									<option value="10">10 km</option>
									<option value="15">15 km</option>
								</select>
								
								<?php if(isset($params['radius_sekarang'])) :?>
								<script>
									$('#radius_sekarang').val('<?php echo $params['radius_sekarang']; ?>');
								</script>
								<?php endif; ?>
							</div>
						</fieldset>
					</div>
					
					<div class="col-sm-12" style="text-align:center">
						<button class="btn btn-primary"><i class="fa fa-search" style="margin-right:5px;color:#fff"></i>CARI</button>
					</div>
				</form>
			</div>
			
			
			<div id="map-canvas" style="margin-top:15px;margin-bottom:15px;width: 100%;height: 700px;"></div>
	</div>
</section>

<script>
		$('input[type="radio"]').on('ifClicked',function(){
			
			$('select, input[type="checkbox"], input[type="text"]').prop('disabled', true);
			
			var form_target = $(this).attr('alt');
			$('fieldset.' + form_target + ' input[type="text"]').prop('disabled', false);
			$('fieldset.' + form_target + ' select').prop('disabled', false);
			$('fieldset.' + form_target + ' input[type="checkbox"]').prop('disabled', false);
			$('fieldset.' + form_target + ' input[type="checkbox"]').removeProp('disabled');
			
			$('.icheckbox_square-blue ').removeClass('checked');
			
		});
		
		$(document).ready(function(){
			
			
			$('input[type="radio"]').each(function(){
				var form_target = $(this).attr('alt');
				
				if($(this).is(':checked')){
					$('fieldset.' + form_target + ' input[type="text"]').prop('disabled', false);
					$('fieldset.' + form_target + ' select').prop('disabled', false);
					$('fieldset.' + form_target + ' input[type="checkbox"]').prop('disabled', false);
					$('fieldset.' + form_target + ' input[type="checkbox"]').removeProp('disabled');
				}
				else
				{
					$('select,input[type="text"],input[type="checkbox"]').prop('disabled', true);
					$('.icheckbox_square-blue ').removeClass('checked');
				}
			});
		});
</script>

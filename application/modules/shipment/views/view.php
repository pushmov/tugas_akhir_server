<section class="ct-find-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger">
					  <a href="#" class="alert-link"><?php echo $this->session->flashdata('error'); ?></a>
					</div>
				<?php endif; ?>
				<?php if($this->session->flashdata('success')) : ?>
					<div class="alert alert-success">
					  <a href="#" class="alert-link"><?php echo $this->session->flashdata('success'); ?></a>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="col-md-6">
				<h3 class="strong"><?php echo $shipment->name ?></h3>
				<h4><?php echo $this->shipment_lib->get_type_name($shipment->type)->shipment_type_name ?> &raquo; <?php echo isset($this->shipment_lib->get_sub_type_name($shipment->sub_type)->sub_name) ? $this->shipment_lib->get_sub_type_name($shipment->sub_type)->sub_name : '-';?></h4>
				
				<div><span>Tanggal Posting : <?php echo $shipment->date_added ?></span></div>
				<div><span>Tanggal Expired : <?php echo $shipment->pickup_max_date ?></span></div>
				<div><span>Harga Yang Ditawarkan : <?php echo ($shipment->price_offered == '') ? 'Fleksibel' : 'Rp. ' . number_format($shipment->price_offered) ?></span></div>

				<h4 class="strong">Deskripsi:</h4>
				<p><?php echo $shipment->detail ?></p>
				
				<div class="gallery" style="margin:10px 0;min-height:250px">
				<?php if(!empty($pictures)) : ?>
				
				<?php foreach($pictures as $pic) : ?>
				
					<div><img src="<?php echo base_url(); ?>public/images/uploads/shipmentpicture/<?php echo $pic->shipment_picture; ?>" width="250" height="250"></div>
					
				<?php endforeach; ?>
				
				<?php endif; ?>
				</div>
				
				<?php if ($this->auth->is_logged_in() && $this->auth->is_driver()): ?>
					<?php $bid = $this->auth->user_bid($shipment->shipment_id) ?>
					<?php if ($bid): ?>
						<div class="well">
							<h4 style="position:relative">Bid Anda: <b>Rp. <?php echo $bid->bid_price ?></b>
								<a onclick="return confirm('Apakah anda yakin akan menghapus bid ini?')" href="<?php echo site_url('driver/cancel/' . $bid->notification_id); ?>" style="color:#ff0000;right:5px;top:0;position:absolute">Cancel Bid<i style="margin-left:5px;color:#ff0000" class="fa fa-times-circle fa-lg"></i></a>
							</h4>
						</div>
					<?php else: ?>
						<a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-lg btn-primary">
							Bid Sekarang
						</a>
					<?php endif ?>
				<?php endif ?>	

				<?php if ($this->shipment_lib->is_closed($shipment->shipment_id)): ?>
					<div class="well"><h4 class="strong">Iklan ini sudah ditutup</h4></div>
					
					<h4 class="strong">Pemenang : </h4>
					
					<ul class="list-unstyled">
						<?php $winner = $this->shipment_lib->get_winner($shipment->shipment_id);?>
						
						<li class="row" style="border-bottom:1px solid #E3E3E3">
							<div class="col-md-8" style="margin-top:25px;">
								<div class="clearfix">
									<div class="pull-left" style="margin-right:15px;">
										<?php if($winner->profpic != '') : ?>
										<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/<?php echo $winner->profpic?>" width="60" height="60">
										<?php else : ?>
										<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/anonymous.png" width="60" height="60">
										<?php endif; ?>
									</div>
									
									<div>
										<h5><a href="<?php echo site_url('profile/index/' . $winner->member_id) ?>"><?php echo $winner->username ?></a></h5>
										<p>Harga: <b>Rp. <?php echo $winner->bid_price ?></b></p>
									</div>
								</div>
							</div>
								
						</li>
						
					</ul>
				<?php else: ?>
					<div class="bidder-list">
						<h4 class="strong">Daftar Bidder:</h4>
						<ul class="list-unstyled">
							<?php $bids = $this->shipment_lib->get_bids($shipment->shipment_id);?>
							<?php foreach ($bids as $bid): ?>
							<li class="row" style="border-bottom:1px solid #E3E3E3">
								<div class="col-md-8" style="margin-top:25px;">
									<div class="clearfix">
										<div class="pull-left" style="margin-right:15px;">
											<?php if($bid->profpic != '') : ?>
											<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/<?php echo $bid->profpic?>" width="60" height="60">
											<?php else : ?>
											<img src="<?php echo base_url(); ?>public/images/uploads/memberprofile/anonymous.png" width="60" height="60">
											<?php endif; ?>
										</div>
										<div>
											<h5><a href="<?php echo site_url('profile/index/' . $bid->member_id) ?>"><?php echo $bid->username ?></a></h5>
											<?php if($this->auth->get_user_id() == $bid->member_id || !$this->auth->is_driver()) : ?>
											<p>Harga: <b>Rp. <?php echo $bid->bid_price ?></b></p>
											<?php else : ?>
											<p>Tanggal Bid : <?php echo date('d-M-Y', strtotime($bid->bid_submit_date)); ?></p>
											<?php endif; ?>
										</div>
									</div>
								</div>
								

								<?php if ($shipment->member_id == $this->auth->get_user_id()): ?>
									<div class="col-md-4 text-right">
										<br>
										<a href="<?php echo site_url('notification/choose/'.$bid->notification_id) ?>" class="btn btn-primary" onclick="return confirm('Apakah anda yakin akan memilih driver ini?')">Pilih</a>
									</div>
								<?php endif ?>
								
							</li>
							<?php endforeach ?>
						</ul>
					</div>	
				<?php endif ?>
					
			</div>
			
			<div class="col-md-6">
				<h4 class="strong">Map</h4>
				<style>
					#map_canvas {
						width: 100%;
						height: 400px;
					}
				</style>
				
				<script src="https://www.mapquestapi.com/sdk/js/v7.0.s/mqa.toolkit.js?key=Fmjtd%7Cluua2d0r20%2Cb2%3Do5-hf7w9"></script>
				<script language="javascript">
					MQA.EventUtil.observe(window, 'load', function() {
					
					window.map = new MQA.TileMap(
						document.getElementById('map_canvas'),
						10,
						{lat:-2.635789, lng:114.859314}, 
						'map');
					
					MQA.withModule('largezoom', function() {
									map.addControl(
										new MQA.LargeZoom(),
										new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5))
									);
								});
					
					MQA.withModule('directions', function() {
							map.addRoute([
									{latLng: {lat: '<?php echo $shipment->pickup_latitude; ?>', lng: '<?php echo $shipment->pickup_longitude; ?>'}},
									{latLng: {lat: '<?php echo $shipment->deliver_latitude; ?>', lng: '<?php echo $shipment->deliver_longitude; ?>'}}
								], 
								{ribbonOptions:{draggable:true, draggablepoi:true}}
								
								);	
					});
					});
				</script>
				<script type="text/javascript" src="<?php echo base_url() ?>public/plugin/mapquest/hostname.js"></script>
				<script type="text/javascript" src="<?php echo base_url() ?>public/plugin/mapquest/omniunih.js"></script>
				<script type="text/javascript" src="<?php echo base_url() ?>public/plugin/mapquest/cdata.js"></script>
				
				<div id="map_canvas"></div>
				
				
				<script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyBwgS5XTE8U6GMPrNFAfYnmdSZHTIVvniE" type="text/javascript"></script>
				<script type="text/javascript" src="https://www.google.com/jsapi?key=AIzaSyBwgS5XTE8U6GMPrNFAfYnmdSZHTIVvniE"></script>
				<script type="text/javascript">
					//<![CDATA[
					google.load("maps", "2");
					var gdir;
					function load() {
						if (GBrowserIsCompatible()) {
							gdir = new google.maps.Directions();
							google.maps.Event.addListener(gdir, "load", handleLoad);
							gdir.load("from: <?php echo $shipment->pickup_latitude; ?>,<?php echo $shipment->pickup_longitude; ?> to: <?php echo $shipment->deliver_latitude;?>,<?php echo $shipment->deliver_longitude;?>", {getSteps: true});
						}
					}
					function handleLoad() {
						var totalMiles = gdir.getDistance().html;
						var totalMilesInt = totalMiles.split("&nbsp;");
						
						$('#totalMiles').html(totalMiles);
					}
					
					window.onload = load;
					window.onunload = google.maps.Unload;
					//]]>
				</script>
				
				<?php 
					$start_addr = $shipment->pickup_address.", ".$shipment->pickup_city.", ";
					$start_addr .= $shipment->pickup_province_code.", ".$shipment->pickup_zip_code;

					$end_addr = $shipment->deliver_address.", ".$shipment->deliver_city.", ";
					$end_addr .= $shipment->deliver_province_code.", ".$shipment->deliver_zip_code;
				 ?>
				<h5>Start (A) 	: <?php echo $start_addr ?></h5>
				<h5>End (B) 	: <?php echo $end_addr ?></h5>
				<h5>Estimated Numbers of Miles 	:<span id="totalMiles" style="color:#FFD95A;font-size:22px;font-weight:bold"></span></h5>
			</div>
			
				<div class="col-md-12">
				</div>
		</div>
		
	</div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Pasang Bid</h4>
      </div>
      <div class="modal-body">
    	<h5>Masukkan harga yang anda tawarkan untuk shipment ini</h5>

        <form class="form-inline" role="form" action="<?php echo site_url('notification/add/'.$shipment->shipment_id) ?>" method="post">
          <div class="form-group">
            <label class="sr-only" for="price">Email address</label>
            <input type="text" name="price" class="form-control" id="price" placeholder="100000">
          </div>
          <button type="submit" class="btn btn-primary">Pasang Bid</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
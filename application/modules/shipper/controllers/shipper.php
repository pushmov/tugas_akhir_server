<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipper extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('shipper_model');
		$this->base_view = $this->config->item('base_view');
		
		if(!$this->auth->is_logged_in()){
			redirect('/login');
		}
	}
	
	public function report()
	{
		
		if(!$this->input->post('notificationid'))
		{
			redirect('/dashboard');
		}
		
		$message = $this->input->post('message');
		$notificationid = $this->input->post('notificationid');
		
		$update_data = array(
			'status' => 'F',
			'is_shipper_accept_report' => 1,
			'shipper_report' => $message,
			'shipper_report_date' => date('Y-m-d H:i:s')
		);
		$this->Appmodel->set_table('notification');
		$this->Appmodel->update($update_data, array('notification_id' => $notificationid));
		
		$this->session->set_flashdata('success', 'Report diterima. Shipment telah dikonfirmasi');
		redirect('/dashboard');
	}
	
	public function report_admin()
	{
		if(!$this->input->post('notificationid'))
		{
			redirect('/dashboard');
		}
		
		$message = $this->input->post('message');
		$notificationid = $this->input->post('notificationid');
		
		$update_data = array(
			'status' => 'F',
			'is_shipper_accept_report' => 1,
			'shipper_report' => $message,
			'shipper_report_date' => date('Y-m-d H:i:s')
		);
		$this->Appmodel->set_table('notification');
		$this->Appmodel->update($update_data, array('notification_id' => $notificationid));
		
		$this->session->set_flashdata('success', 'Report diterima. Shipment telah dikonfirmasi');
		
		redirect('dashboard');
	}
	
	function payment($notification_id){
		
		$data = array(
			'page_content' => 'payment',
			'active_nav' => '');
			
		
		
		$payment_data = $this->shipper_model->get_payment_data($notification_id);
		$data['payment'] = $payment_data;
		
		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			
			$this->_post_data($payment_data->id);
		}
		
		$this->load->view($this->base_view, $data);
	}
	
	private function _post_data($id){
		
		$request = array(
			'invoice_method' => $this->input->post('metode'),
			'invoice_transferred' => $this->input->post('jumlah_dana'),
			'invoice_date_paid' => $this->input->post('tanggal_bayar'),
			'invoice_account_name' => $this->input->post('nama_pengirim'),
			'invoice_account_number' => $this->input->post('nomor_rekening_pengirim'),
			'invoice_status' => NULL,
			'status_message' => NULL
		);
		
		$this->Appmodel->set_table('shipper_invoice');
		$this->Appmodel->update($request, array('id' => $id));
		
		$this->session->set_flashdata('success', 'Data telah tersimpan. Pembayaran segera di review oleh admin.');
		redirect('dashboard');
	}

	
}
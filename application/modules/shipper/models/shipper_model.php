
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipper_model extends Appmodel {

	function __construct() {
		parent::__construct();
	}
	
	function get_payment_data($nid){
		return $this->db->where('notification_id', $nid)
			->join('shipment', "shipment.shipment_id = notification.shipment_id")
			->join('member', "notification.member_id = member.member_id")
			->join('member_profile', "member.member_id = member_profile.member_id")
			->join('shipper_invoice', "notification.member_id = shipper_invoice.driver_id AND notification.shipment_id = shipper_invoice.shipment_id AND shipment.member_id = shipper_invoice.shipper_id")
			->get('notification')
			->row();
	}
	
}
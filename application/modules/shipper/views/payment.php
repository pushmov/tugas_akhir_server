<section class="ct-feedback">
		<div class="container">
				
				<h2>Konfirmasi Dana</h2>
				
				<h5>Driver Bid : <strong>Rp. <?php echo number_format($payment->bid_price); ?></strong></h5>
				<h5>Preffered Price : <strong><?php echo ($payment->price_offered != '') ? 'Rp. '.number_format($payment->price_offered) : 'Fleksibel'; ?></strong></h5>
				
				
				<div class="alert alert-danger alert-dismissable" id="alert" style="display:none">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p style="color:#cc0000">Semua field harus diisi...</p>
				</div>
				
				<h4 style="font-size:14px">Semua field dengan tanda <span style="color:#ff0000">*</span> harus diisi</h4>
				
				<div style="padding:45px 0">
					<form method="post" id="feedback" name="feedback" action="<?php echo site_url('shipper/payment/' . $payment->notification_id); ?>">
						<fieldset>
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Metode Pembayaran : </label>
									<div class="col-sm-10">
										<select name="metode" id="metode">
											<option>ATM</option>
											<option>Internet Banking</option>
											<option>SMS</option>
										</select>
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Jumlah Dana<span style="color:#ff0000">*</span> : </label>
									<div class="col-sm-10">
											<input name="jumlah_dana" type="text" class="form-control required" value="">
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Tanggal Bayar<span style="color:#ff0000">*</span> : </label>
									<div class="col-sm-10">
											<input name="tanggal_bayar" type="text" class="form-control required date-field" value="">
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Nama Pemegang Rekening<span style="color:#ff0000">*</span> : </label>
									<div class="col-sm-10">
											<input name="nama_pengirim" type="text" class="form-control required" value="">
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Nomor Rekening<span style="color:#ff0000">*</span>: </label>
									<div class="col-sm-10">
											<input name="nomor_rekening_pengirim" type="text" class="form-control required" value="">
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Penerima : </label>
									<div class="col-sm-10">
											<strong><?php echo $payment->first_name . ' ' . $payment->last_name; ?></strong>
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Nomor Rekening Penerima : </label>
									<div class="col-sm-10">
											<strong><?php echo $payment->bank_account_number; ?></strong>
									</div>
							</div>
							
							<div class="form-group clearfix">
									<label class="col-sm-2 control-label">Nama Shipment : </label>
									<div class="col-sm-10">
											<a href="<?php echo site_url('shipment/view/' . $payment->shipment_id ); ?>"><strong><?php echo $payment->name; ?></a></strong>
									</div>
							</div>
							
							<input type="submit" id="submit" name="submit" class="btn btn-primary" value="KONFIRMASI">
						</fieldset>
					</form>
				</div>
		</div>
</section>


<script>
	$('#submit').click(function(){
		$('.required').css({'border' : '1px solid #CCCCCC'});
		$('#alert').hide();
		var is_valid = true;
			$('.required').each(function(){
				if($(this).val() == '')
				{
					$(this).css({'border' : '1px solid #cc0000'});
					$('#alert').show('fast');
					is_valid = false;
				}
			});
			
			return is_valid;
	});
	
</script>
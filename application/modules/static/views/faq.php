<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  
		
	<title>Online Shipper - FAQ</title>
	
	<link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css' />
	<link href="<?php echo base_url(); ?>public/css/styles.css" rel="stylesheet"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.min.css" >
	
	
	
	
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>public/js/icheck.js"></script>
	<link href="<?php echo base_url(); ?>public/css/polaris/polaris.css" rel="stylesheet" >
	
	<link href="<?php echo base_url(); ?>public/css/square/blue.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>public/js/icheck.js"></script>
	<!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="<?php echo base_url(); ?>js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
	
	<script>
		$(document).ready(function(){
			  $('input').iCheck({
			    checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
			    increaseArea: '10%' // optional
			  });
			});
	</script>
	
</head>
<body>
	<header class="navbar navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
        </button>
				<div class="brand-wrapper">
					<a href="#"><img src="<?php echo base_url(); ?>public/images/logo.png" alt="logo" ></a>
				</div>
      </div>
			<div class="collapse navbar-collapse navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo base_url(); ?>">HOME</a></li>
					<li><a href="<?php echo base_url(); ?>find/">CARI PAKET</a></li>
					<li><a href="<?php echo base_url(); ?>register/">DAFTAR SEKARANG</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>faq/">FAQ</a></li>
					<li><a href="<?php echo base_url(); ?>kontak/">KONTAK</a></li>
					<li><a href="<?php echo base_url(); ?>login/">LOGIN</a></li>
        </ul>
      </div>
    </div>
	</header>
	
	
	
	<section class="ct-login">
		<div class="container">
			
			<h3>FAQ</h3>
			<h4 class="strong">Apakah Online-Shipper itu?</h4>
			Online-shipper dapat menghubungkan orang - orang yang ingin menyampaikan barang ke suatu tempat (shipper) dengan orang yang bersedia mengantar nya (driver). Anda bisa mengirimkan apa saja, mulai dari dokumen, paket, peralatan rumah tangga, bahkan sampai hewan peliharaan! Segera daftar sekarang untuk menikmati semua fitur yang tersedia. Jika anda berencana bepergian ke suatu tempat, ini bisa menjadi dana tambahan anda untuk membayar biaya perjalanan anda. 
			<h4 class="strong">Apakah saya bisa benar - benar menghemat uang dengan menggunakan Online-Shipper?</h4>
			Tentu saja! anda bisa menghemat dalam biaya pengiriman barang. Karena jika dibandingkan dengan jasa ekspedisi tradisional, Online-Shipper lebih fleksibel dan efisien karena anda bisa mengirimkan berbagai jenis barang yang anda inginkan dibandingkan dengan jaaa ekspedisi tradisional yang mempunyai keterbatasan dalam jenis barang dan tarif jika disangkutpautkan dengan ukuran dan berat barang. Di ekspedisi tradisional, jika barang semakin besar dan berat maka tarifnya akan melambung tinggi. Tetapi di Online-Shipper seberapapun ukuran dan berat barang, tarif bisa ditentukan dari kesepakatan antara shipper dan driver sehingga tarif yang tercipta bisa menjadi lebih murah dibandingkan menggunakan ekspedisi tradisional.
			<h4 class="strong">Apakah saya bisa benar - benar menghasilkan uang dari Online-Shipper?</h4>
			
		</div>
	</section>
	
	<footer>
		<div class="container">
			<p>&copy; 2014 Team Tugas Akhir Sistem Komputer</p>
		</div>
	</footer>
	
	<script>
		
		$(window).scroll(function(e){parallax();});
		function parallax(){var scrolled = $(window).scrollTop();$('.intro-banner').css('top',-(scrolled*0.2)+'px');}
	</script>
</body>
</html>
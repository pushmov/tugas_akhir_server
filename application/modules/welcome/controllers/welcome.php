<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MX_Controller {

	var $base_view;
			
	function __construct()
	{
		parent::__construct();

		$this->base_view = $this->config->item('base_view');
		
		$this->load->model('welcome/welcome_model');
	}


	public function index()
	{
		
		if($this->auth->is_driver()){
			redirect('dashboard');
		}
		
		$data = array('page_content' => 'home',
					  'active_nav' => 'home');
						
		$latest_shipment = $this->welcome_model->get_latest_shipment();
		
		$data['latest_shipment'] = $latest_shipment;
		
		$this->load->view($this->base_view, $data);
	}

	public function sendemail(){
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'rizki.a.aprilian@gmail.com',
			'smtp_pass' => '',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		// Set to, from, message, etc.

		$result = $this->email->send();
		
		print_r($result);
		exit();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome_model extends CI_Model {
  
  var $table = 'order_address';

	function __construct() {
		parent::__construct();
	}
	
	function get_latest_shipment(){
		
		return $this->db->where("(notification.status = 'O' OR notification.status = 'A') AND unix_timestamp(shipment.deliver_max_date) > " . time(), NULL, FALSE)
			->join('notification', "notification.shipment_id = shipment.shipment_id")
			->join('shipment_type', "shipment_type.type = shipment.type")
			->order_by('shipment.date_added', "DESC")
			->limit(4)
			->get('shipment')
			->result();
		
	}
	

}
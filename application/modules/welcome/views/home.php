<section class="intro-banner">
	<div class="container">
		<a class="btn btn-primary" href="<?php echo site_url('shipment/add') ?>">POSTING IKLAN SEKARANG</a>
	</div>
</section>

<section class="latest-shipment">
	<div class="container" style="text-align:center">
		<h3>Postingan terbaru</h3>
		<div class="row">
			
			<?php if(!empty($latest_shipment)) : ?>
			<?php foreach($latest_shipment as $shipment) : ?>
				<div class="col-md-3 latest-shipment-box">
					<div class="latest-wrapper">
						<h3><a href="<?php echo base_url(); ?>shipment/view/<?php echo $shipment->shipment_id; ?>"><?php echo $shipment->shipment_type_name; ?></a></h3>
						<div><img height="100" width="250" src="<?php echo base_url(); ?>public/uploads/shipment/d.JPG"></div>
						<span>Dispoting : <?php echo date('d F Y',strtotime($shipment->date_added)); ?></span>
						<h4><a href="<?php echo base_url(); ?>shipment/view/<?php echo $shipment->shipment_id; ?>"><?php echo $shipment->name; ?></a></h4>
						<p><?php echo $shipment->detail; ?></p>
					</div>	
					<button class="btn btn-primary" onclick="window.location='<?php echo base_url(); ?>shipment/view/<?php echo $shipment->shipment_id; ?>'">BID SEKARANG</button>
				</div>
			<?php endforeach; ?>
			<?php else : ?>
			<div>No record found</div>
			<?php endif; ?>
			
		</div>
	</div>
</section>

<section class="ct-about">
	<div class="container">
		<div class="row">
			<p>Online-shipper dapat menghubungkan orang - orang yang ingin menyampaikan barang ke suatu tempat (shipper) dengan orang yang bersedia mengantar nya (driver). Anda bisa mengirimkan apa saja, mulai dari dokumen, paket, peralatan rumah tangga, bahkan sampai hewan peliharaan! Segera daftar sekarang untuk menikmati semua fitur yang tersedia. Jika anda berencana bepergian ke suatu tempat, ini bisa menjadi dana tambahan anda untuk membayar biaya perjalanan anda</p>
		</div>
	</div>
</section>

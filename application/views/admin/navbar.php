<header class="navbar navbar-fixed-top navbar-inverse" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
    </button>
			
  </div>
		<div class="collapse navbar-collapse navbar-left">
			<ul class="nav navbar-nav">
				
				<?php if($this->session->userdata('admin')) : ?>
				<li <?php echo ($active == 'request') ? 'class="active"' : ''; ?>>
					<a href="<?php echo site_url('admin/request'); ?>">REQUEST ACC</a>
				</li>
				<li <?php echo ($active == 'report') ? 'class="active"' : ''; ?>>
					<a href="<?php echo site_url('admin/report'); ?>">REPORT</a>
				</li>
				<li <?php echo ($active == 'sub') ? 'class="active"' : ''; ?>>
					<a href="<?php echo site_url('admin/subtype'); ?>">SUB TYPE</a>
				</li>
				<li>
					<a href="<?php echo site_url('admin/logout'); ?>">LOGOUT</a>
				</li>
				<?php else : ?>
				<li class="active">
					<a href="<?php echo site_url('admin/logout'); ?>">LOGIN</a>
				</li>
				<?php endif; ?>
				
    </ul>
  </div>
</div>
</header>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>
    
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url(); ?>public/css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.min.css" >
    <link href="<?php echo base_url(); ?>public/css/polaris/polaris.css" rel="stylesheet" >
    <link href="<?php echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" >
		
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php echo base_url(); ?>js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    
  </head>
  
    <body>
    <!--[if lt IE 8]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
		
    <?php $this->load->view('admin/navbar'); ?>
        
    <?php $this->load->view($page_content); ?>
        
    <?php $this->load->view('admin/footer'); ?>

    
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/jquery-ui.js"></script>
    

  </body>
</html>
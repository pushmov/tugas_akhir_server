		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script>

			function initialize() {
				var mapOptions = {
					zoom: 6,
					center: new google.maps.LatLng(parseFloat('<?php echo $maps_center['latitude']; ?>'), parseFloat('<?php echo $maps_center['longitude']; ?>'))
				};
				
				var locations = <?php echo json_encode($shipments); ?>;

				var map = new google.maps.Map(document.getElementById('map-canvas'),
					mapOptions);
					
				var infowindow = new google.maps.InfoWindow();
				
				var marker, i;

				for (i = 0; i < locations.length; i++) {
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						icon : $('#base_url').attr('alt') + 'public/images/small-package.png',
						map: map
					});

					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
			 
			}
		</script>
		
		
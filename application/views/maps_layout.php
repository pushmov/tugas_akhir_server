<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Online Shipper</title>
    
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url(); ?>public/css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.min.css" >
    <link href="<?php echo base_url(); ?>public/css/polaris/polaris.css" rel="stylesheet" >
    <link href="<?php echo base_url(); ?>public/css/jquery-ui.css" rel="stylesheet" >
		
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php echo base_url(); ?>js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
		
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		
		<?php $this->load->view('maps_header'); ?>
		
		
		<link href="<?php echo base_url(); ?>public/plugin/icheck/skins/all.css" rel="stylesheet">
		<script src="<?php echo base_url(); ?>public/plugin/icheck/icheck.js"></script>
		<script>
			$(document).ready(function(){
				var callbacks_list = $('.demo-callbacks ul');
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%'
				});
			});
		</script>
    
  </head>
  
    <body <?php if ($active_nav == 'home') { echo 'class="homepage"';} ?> onload="initialize()">
    <!--[if lt IE 8]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php $this->load->view('navbar'); ?>
        
    <?php $this->load->view($page_content); ?>
        
    <?php $this->load->view('footer'); ?>

    
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/jquery-ui.js"></script>
    
    <?php if (isset($extra_js)) {
      foreach ($extra_js as $file) {
        echo "<script src='".base_url($file)."'></script>";
      }
    } ?>

    <script>
      $(window).scroll(function(e){parallax();});
      function parallax(){var scrolled = $(window).scrollTop();$('.intro-banner').css('top',-(scrolled*0.2)+'px');}
      $(function () {$(".date-field").datepicker({dateFormat: 'yy-mm-dd'});})
    </script>


    <!--

    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>-->
  </body>
</html>
<header class="navbar navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
    </button>
			<div class="brand-wrapper">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/images/logo.png" alt="logo" ></a>
			</div>
  </div>
		<div class="collapse navbar-collapse navbar-right">
			<ul class="nav navbar-nav">
				<li <?php if ($active_nav == 'home') {echo 'class="active"';} ?>>
					<a href="<?php echo base_url();?>" id="home">HOME</a>
				</li>

				<?php if($this->auth->is_driver() || !$this->auth->is_logged_in()) : ?>
				<li <?php if ($active_nav == 'shipment') {echo 'class="active"';} ?>>
					<a href="<?php echo site_url('shipment/find'); ?>">CARI PAKET</a>
				</li>
				<?php endif; ?>

				<?php if (!$this->auth->is_logged_in()): ?>
					<li <?php if ($active_nav == 'register') {echo 'class="active"';} ?>>
						<a href="<?php echo site_url('driver/register'); ?>">DAFTAR SEKARANG</a>
					</li>
				<?php endif ?>

				<li <?php if ($active_nav == 'faq') {echo 'class="active"';} ?>>
					<a href="<?php echo site_url('faq'); ?>">FAQ</a>
				</li>

				<li <?php if ($active_nav == 'contact') {echo 'class="active"';} ?>>
					<a href="<?php echo site_url('kontak'); ?>">KONTAK</a>
				</li>

				<?php if ($this->auth->is_logged_in()): ?>
					<li <?php if ($active_nav == 'dashboard') {echo 'class="active"';} ?>>
						<a href="<?php echo site_url('dashboard'); ?>">DASHBOARD</a>
					</li>
				<?php else: ?>
					<li <?php if ($active_nav == 'login') {echo 'class="active"';} ?>>
						<a href="<?php echo site_url('login'); ?>">LOGIN</a>
					</li>
				<?php endif ?>
    </ul>
  </div>
</div>
</header>
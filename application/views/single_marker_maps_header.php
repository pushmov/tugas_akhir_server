		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script>
			function initialize(){
				var myLatlng = new google.maps.LatLng(parseFloat('<?php echo $latitude; ?>'),parseFloat('<?php echo $longitude; ?>'));
				var mapOptions = {
					zoom: 18,
					center: myLatlng
				}
				var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

				var marker = new google.maps.Marker({
						position: myLatlng,
						title:"<?php echo $driver->username; ?>"
				});

				// To add the marker to the map, call setMap();
				marker.setMap(map);
			}
		</script>
		
		
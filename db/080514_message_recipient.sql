ALTER TABLE `message_recipient` CHANGE `message_id` `message_recipient_id` INT( 20 ) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `message_recipient` ADD `message_id` INT( 20 ) NOT NULL AFTER `message_recipient_id` ;
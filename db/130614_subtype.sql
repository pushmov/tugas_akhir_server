CREATE TABLE IF NOT EXISTS `shipment_sub_type` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `sub_name` varchar(255) NOT NULL,
  PRIMARY KEY (`sub_id`)
);

INSERT INTO `shipment_sub_type` (`sub_id`, `parent_id`, `sub_name`) VALUES
(2, 1, 'Anjing'),
(3, 1, 'Kucing'),
(4, 1, 'Asu'),
(5, 1, 'Bebek'),
(6, 1, 'Ayam');
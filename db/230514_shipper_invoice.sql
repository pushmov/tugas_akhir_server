CREATE TABLE IF NOT EXISTS `shipper_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipper_id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `invoice_number` varchar(40) NOT NULL,
  `invoice_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_date_paid` datetime DEFAULT NULL,
  `invoice_method` varchar(255) DEFAULT NULL,
  `is_accepted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

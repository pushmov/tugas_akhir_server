ALTER TABLE `notification` ADD `driver_report` TEXT NULL DEFAULT NULL ;
ALTER TABLE `notification` ADD `driver_report_date` DATE NULL DEFAULT NULL ;
ALTER TABLE `notification` ADD `is_shipper_accept_report` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `notification` CHANGE `is_shipper_accept_report` `is_shipper_accept_report` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT '0=shipper blm accept report dari driver, 1=shipper sudh menerima report dan konfirmasi bahwa barang sudah sampai + asumsi payment sudah dilakukan';
ALTER TABLE `notification` CHANGE `status` `status` VARCHAR( 3 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'O' COMMENT 'A=applied, ACC=accepted, R=rejected, O=open, C=completed, F=finished';
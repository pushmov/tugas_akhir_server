ALTER TABLE `notification` ADD `shipper_report` TEXT NULL DEFAULT NULL ;
ALTER TABLE `notification` ADD `shipper_report_date` DATETIME NULL DEFAULT NULL ;


ALTER TABLE `feedback` CHANGE `member_id` `from_member_id` INT( 20 ) NULL DEFAULT NULL ;
ALTER TABLE `feedback` ADD `shipment_id` INT( 11 ) NOT NULL ;
ALTER TABLE `feedback` ADD `recipient_title` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `recipient_id` ,
ADD `recipient_rating` FLOAT NULL DEFAULT NULL AFTER `recipient_title` ;

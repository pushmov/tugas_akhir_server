ALTER TABLE `member` ADD `is_admin` TINYINT( 1 ) NOT NULL DEFAULT '0';

INSERT INTO `member` (`member_id`, `email`, `username`, `password`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `address`, `city`, `state_code`, `zip_code`, `phone`, `ip_address`, `registration_date_time`, `is_driver`, `is_banned`, `activation_status`, `activation_code`, `subscribe_newsletter`, `subscribe_push_notification`, `profpic`, `bid_quota`, `verification_level`, `is_admin`) VALUES
(59, 'admin@admin.com', 'admin', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-29 08:18:02', 0, 0, 0, NULL, 1, 1, NULL, 5, 1, 1);


ALTER TABLE `shipper_invoice` ADD `invoice_transferred` VARCHAR( 255 ) NULL DEFAULT NULL ;
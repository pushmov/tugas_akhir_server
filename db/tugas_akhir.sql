-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2014 at 01:33 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tugas_akhir`
--

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `feedback_id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `feedback_text` text,
  `rating` float DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `member_id` int(20) DEFAULT NULL,
  `recipient_id` int(20) DEFAULT NULL,
  `recipient_reply` text,
  `recipient_reply_date` datetime DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `middle_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address` tinytext,
  `city` varchar(255) DEFAULT NULL,
  `state_code` varchar(2) DEFAULT NULL,
  `zip_code` varchar(5) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `registration_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_driver` tinyint(1) NOT NULL DEFAULT '1',
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `activation_status` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(40) DEFAULT NULL,
  `subscribe_newsletter` tinyint(1) NOT NULL DEFAULT '1',
  `subscribe_push_notification` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `email`, `username`, `password`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `address`, `city`, `state_code`, `zip_code`, `phone`, `ip_address`, `registration_date_time`, `is_driver`, `is_banned`, `activation_status`, `activation_code`, `subscribe_newsletter`, `subscribe_push_notification`) VALUES
(1, 'rizki.a.aprilian@gmail.com', 'rizkiaprilian', '12345', 'Rizki', 'A', 'Aprilian', NULL, NULL, NULL, NULL, NULL, '+625720002060', NULL, '2013-12-15 08:57:37', 1, 0, 1, 'd41d8cd98f00b204e9804b34f8a4985c4', 1, 1),
(2, 'rizki.test@gmail.com', 'rizki.test', '12345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6285720002060', '192.168.56.101', '2014-01-04 08:10:56', 0, 0, 1, '940cd9db3bda983669a2d775902e790a4e3557b9', 1, 1),
(3, 'rizki.test.2@gmail.com', 'rizki.test.2', 'password', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '+6285720002060', '192.168.56.101', '2014-01-04 10:41:21', 0, 0, 1, '69ee1602b20471b638749664f5e5b92f6df6f11d', 1, 1),
(17, 'jsajk@sajhjc.com', 'jsajk', 'asi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9283923', '192.168.56.101', '2014-01-04 17:27:46', 0, 0, 1, '9a58c9ecd15643d50877a9eb724d015f1da470f8', 1, 1),
(18, 'ipulfellaz@gmail.com', 'ipulfellaz', '12345', 'Syaiful', 'Anwar', 'Musyadad', '1990-05-02', 'Jl Sukabirus no. 64', 'Bandung', 'JB', '40257', '08984256326', NULL, '2014-02-07 16:04:38', 1, 0, 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_photo`
--

CREATE TABLE IF NOT EXISTS `member_photo` (
  `photo_id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) DEFAULT NULL,
  `photo_file` varchar(255) DEFAULT NULL,
  `add_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_profile`
--

CREATE TABLE IF NOT EXISTS `member_profile` (
  `member_id` int(20) NOT NULL,
  `experience` text,
  `car_type` tinytext,
  `insurance_company` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member_route`
--

CREATE TABLE IF NOT EXISTS `member_route` (
  `route_id` int(20) NOT NULL AUTO_INCREMENT,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `member_id` int(20) DEFAULT NULL,
  `route_name` varchar(255) DEFAULT NULL,
  `start_city` varchar(255) DEFAULT NULL,
  `start_state_code` varchar(5) DEFAULT NULL,
  `start_zip_code` varchar(5) DEFAULT NULL,
  `start_radious` int(5) DEFAULT NULL,
  `start_zip_code_list` text,
  `end_city` varchar(255) DEFAULT NULL,
  `end_state_code` varchar(5) DEFAULT NULL,
  `end_zip_code` varchar(5) DEFAULT NULL,
  `end_radious` int(5) DEFAULT NULL,
  `end_zip_code_list` text,
  `state_list` text,
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_setting`
--

CREATE TABLE IF NOT EXISTS `member_setting` (
  `member_id` int(20) NOT NULL,
  `category_setting` text,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `message_id` int(20) NOT NULL AUTO_INCREMENT,
  `sent_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) DEFAULT NULL,
  `message_text` text,
  `member_id` int(20) DEFAULT NULL,
  `sender_keep` tinyint(1) NOT NULL DEFAULT '1',
  `reply_to_message_id` int(20) DEFAULT NULL,
  `thread_root_message_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_recipient`
--

CREATE TABLE IF NOT EXISTS `message_recipient` (
  `message_id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) DEFAULT NULL,
  `recipient_keep` int(1) NOT NULL DEFAULT '1',
  `recipient_read_date` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `notification_id` int(20) NOT NULL AUTO_INCREMENT,
  `notification_add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(3) DEFAULT 'O',
  `member_id` int(20) DEFAULT NULL,
  `shipment_id` int(20) DEFAULT NULL,
  `bid_price` varchar(50) DEFAULT NULL,
  `notification_read_date` datetime DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `province_code`
--

CREATE TABLE IF NOT EXISTS `province_code` (
  `province_id` int(10) NOT NULL AUTO_INCREMENT,
  `province_code` char(2) NOT NULL,
  `province_code_iso` char(5) NOT NULL,
  `province_name` varchar(255) NOT NULL,
  `province_name_en` varchar(255) NOT NULL,
  `country_code` varchar(3) DEFAULT NULL,
  `country_name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `province_code`
--

INSERT INTO `province_code` (`province_id`, `province_code`, `province_code_iso`, `province_name`, `province_name_en`, `country_code`, `country_name`) VALUES
(1, 'BT', 'ID-BT', 'Banten', 'Banten', 'ID', 'Indonesia'),
(2, 'JB', 'ID-JB', 'Jawa Barat', 'West Java', 'ID', 'Indonesia'),
(3, 'JT', 'ID-JT', 'Jawa Tengah', 'Central Java', 'ID', 'Indonesia'),
(4, 'JI', 'ID-JI', 'Jawa Timur', 'East Java', 'ID', 'Indonesia'),
(5, 'JK', 'ID-JK', 'DKI Jakarta', 'Jakarta', 'ID', 'Indonesia'),
(6, 'YO', 'ID-YO', 'DI Yogyakarta', 'Yogyakarta', 'ID', 'Indonesia'),
(7, 'AL', 'US-AL', 'Alabama', 'Alabama', 'US', 'United States'),
(8, 'AK', 'US-AK', 'Alaska', 'Alaska', 'US', 'United States'),
(9, 'AZ', 'US-AZ', 'Arizona', 'Arizona', 'US', 'United States'),
(10, 'AR', 'US-AR', 'Arkansas', 'Arkansas', 'US', 'United States'),
(11, 'CA', 'US-CA', 'California', 'California', 'US', 'United States'),
(12, 'CO', 'US-CO', 'Colorado', 'Colorado', 'US', 'United States'),
(13, 'CT', 'US-CT', 'Connecticut', 'Connecticut', 'US', 'United States'),
(14, 'DE', 'US-DE', 'Delaware', 'Delaware', 'US', 'United States'),
(15, 'DC', 'US-DC', 'District of Columbia', 'District of Columbia', 'US', 'United States'),
(16, 'FL', 'US-FL', 'Florida', 'Florida', 'US', 'United States'),
(17, 'GA', 'US-GA', 'Georgia', 'Georgia', 'US', 'United States'),
(18, 'HI', 'US-HI', 'Hawaii', 'Hawaii', 'US', 'United States'),
(19, 'ID', 'US-ID', 'Idaho', 'Idaho', 'US', 'United States'),
(20, 'IL', 'US-IL', 'Illinois', 'Illinois', 'US', 'United States'),
(21, 'IN', 'US-IN', 'Indiana', 'Indiana', 'US', 'United States'),
(22, 'IA', 'US-IA', 'Iowa', 'Iowa', 'US', 'United States'),
(23, 'KS', 'US-KS', 'Kansas', 'Kansas', 'US', 'United States'),
(24, 'KY', 'US-KY', 'Kentucky', 'Kentucky', 'US', 'United States'),
(25, 'LA', 'US-LA', 'Louisiana', 'Louisiana', 'US', 'United States'),
(26, 'ME', 'US-ME', 'Maine', 'Maine', 'US', 'United States'),
(27, 'MD', 'US-MD', 'Maryland', 'Maryland', 'US', 'United States'),
(28, 'RI', 'US-RI', 'Rhode Island', 'Rhode Island', 'US', 'United States'),
(29, 'MA', 'US-MA', 'Massachusetts', 'Massachusetts', 'US', 'United States'),
(30, 'MI', 'US-MI', 'Michigan', 'Michigan', 'US', 'United States'),
(31, 'MN', 'US-MN', 'Minnesota', 'Minnesota', 'US', 'United States'),
(32, 'MS', 'US-MS', 'Mississippi', 'Mississippi', 'US', 'United States'),
(33, 'MO', 'US-MO', 'Missouri', 'Missouri', 'US', 'United States'),
(34, 'MT', 'US-MT', 'Montana', 'Montana', 'US', 'United States'),
(35, 'NE', 'US-NE', 'Nebraska', 'Nebraska', 'US', 'United States'),
(36, 'NV', 'US-NV', 'Nevada', 'Nevada', 'US', 'United States'),
(37, 'NH', 'US-NH', 'New Hampshire', 'New Hampshire', 'US', 'United States'),
(38, 'NJ', 'US-NJ', 'New Jersey', 'New Jersey', 'US', 'United States'),
(39, 'NM', 'US-NM', 'New Mexico', 'New Mexico', 'US', 'United States'),
(40, 'NY', 'US-NY', 'New York', 'New York', 'US', 'United States'),
(41, 'NC', 'US-NC', 'North Carolina', 'North Carolina', 'US', 'United States'),
(42, 'ND', 'US-ND', 'North Dakota', 'North Dakota', 'US', 'United States'),
(43, 'OH', 'US-OH', 'Ohio', 'Ohio', 'US', 'United States'),
(44, 'OK', 'US-OK', 'Oklahoma', 'Oklahoma', 'US', 'United States'),
(45, 'OR', 'US-OR', 'Oregon', 'Oregon', 'US', 'United States'),
(46, 'PA', 'US-PA', 'Pennsylvania', 'Pennsylvania', 'US', 'United States'),
(47, 'SC', 'US-SC', 'South Carolina', 'South Carolina', 'US', 'United States'),
(48, 'SD', 'US-SD', 'South Dakota', 'South Dakota', 'US', 'United States'),
(49, 'TN', 'US-TN', 'Tennessee', 'Tennessee', 'US', 'United States'),
(50, 'TX', 'US-TX', 'Texas', 'Texas', 'US', 'United States'),
(51, 'UT', 'US-UT', 'Utah', 'Utah', 'US', 'United States'),
(52, 'VT', 'US-VT', 'Vermont', 'Vermont', 'US', 'United States'),
(53, 'VA', 'US-VA', 'Virginia', 'Virginia', 'US', 'United States'),
(54, 'WA', 'US-WA', 'Washington', 'Washington', 'US', 'United States'),
(55, 'WV', 'US-WV', 'West Virginia', 'West Virginia', 'US', 'United States'),
(56, 'WI', 'US-WI', 'Wisconsin', 'Wisconsin', 'US', 'United States'),
(57, 'WY', 'US-WY', 'Wyoming', 'Wyoming', 'US', 'United States');

-- --------------------------------------------------------

--
-- Table structure for table `shipment`
--

CREATE TABLE IF NOT EXISTS `shipment` (
  `shipment_id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `detail` text,
  `pickup_address` tinytext,
  `pickup_city` varchar(255) DEFAULT NULL,
  `pickup_province_code` varchar(2) DEFAULT NULL,
  `pickup_zip_code` varchar(5) DEFAULT NULL,
  `pickup_max_date` datetime DEFAULT NULL,
  `pickup_latitude` decimal(12,6) DEFAULT NULL,
  `pickup_longitude` decimal(12,6) DEFAULT NULL,
  `deliver_address` tinytext,
  `deliver_city` varchar(255) DEFAULT NULL,
  `deliver_province_code` varchar(2) DEFAULT NULL,
  `deliver_zip_code` varchar(5) DEFAULT NULL,
  `deliver_max_date` datetime DEFAULT NULL,
  `deliver_latitude` decimal(12,6) DEFAULT NULL,
  `deliver_longitude` decimal(12,6) DEFAULT NULL,
  `price_offered` varchar(50) DEFAULT NULL,
  `total_distance` float DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`shipment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `shipment`
--

INSERT INTO `shipment` (`shipment_id`, `member_id`, `name`, `type`, `detail`, `pickup_address`, `pickup_city`, `pickup_province_code`, `pickup_zip_code`, `pickup_max_date`, `pickup_latitude`, `pickup_longitude`, `deliver_address`, `deliver_city`, `deliver_province_code`, `deliver_zip_code`, `deliver_max_date`, `deliver_latitude`, `deliver_longitude`, `price_offered`, `total_distance`, `date_added`) VALUES
(1, 2, 'Kulkas ukuran standar', 6, '1 kulkas ukuran standar, masih dibungkus dengan kardus nya', '429 E. 5th Street', 'Bandung', 'JB', '40266', '2014-06-16 21:31:55', NULL, NULL, '1239 e. florida st.', 'Cirebon', 'JB', '40278', '2014-06-16 21:32:43', NULL, NULL, NULL, NULL, '2013-12-16 14:32:52'),
(2, 2, 'Sugarglider', 4, 'I am looking for a 1995 16 ''x80'' mobile home transported from Black River Falls WI Wisconsin to Lubbock Texas.', 'N8041 US H-12', 'Jakarta', 'JK', '40266', '2014-06-16 21:31:55', NULL, NULL, '6117 9th st.', 'Tegal', 'JT', '52192', '2014-06-16 21:32:43', NULL, NULL, NULL, NULL, '2013-12-17 08:18:47'),
(3, 2, 'Harley to Alaska', 4, 'Unboxed, will need tie downs or enclosed Motorcycle Trailer. Excellent condition and the bike runs. ', '721 Alexandria Ave', 'Carthage', 'NY', '13619', '2014-06-16 21:31:55', '43.984484', '-75.612353', '10960 Hazel Avenue', 'Wasilla', 'AK', '99654', '2014-06-16 21:32:43', '61.483549', '-149.720360', NULL, NULL, '2013-12-21 05:03:32'),
(4, 2, 'xterra 07', 4, 'Job is for one person, Military moving and I don''t want to drive 3 days with my 5 year old. Please be reasonable in the price. I have drove this trip 4 times and I just don''t feel like doing it again. With driving it 4 times I know what it cost to take the trip. ', '15315 Berry Valley Rd Se yelm, wa 98597', 'Yelm', 'WA', '98597', '2014-06-16 21:31:55', '46.945344', '-122.617724', '2047 Mills Ave cincinnati, oh 45212', 'Norwood', 'OH', '45212', '2014-06-16 21:32:43', '39.158632', '-84.459490', NULL, NULL, '2013-12-21 05:05:05'),
(5, 2, 'Symba the Sharpei', 4, 'a 60lbs male sharpei that does not like crates and does not sit well in the front seat of a car/truck', '115 Auburn St', 'Cranston', 'RI', '02910', '2014-06-16 21:31:55', '41.776543', '-71.430510', '302 Locust Creek Blvd', 'Louisville', 'KY', '40245', '2014-06-16 21:32:43', '38.238392', '-85.423201', NULL, NULL, '2013-12-21 05:06:17'),
(6, 2, 'Antique drafting table', 4, 'Could come apart in 2 peices', '61 wilmot st', 'Watertown', 'MA', '02472', '2014-06-16 21:31:55', '42.375026', '-71.201522', '4239 SE 122nd Ave', 'Portland', 'OR', '97236', '2014-06-16 21:32:43', '45.491423', '-122.538814', NULL, NULL, '2013-12-21 05:07:17'),
(7, 2, '2 Cats for Transport to Austin, TX', 2, 'Two domestic short haired cats, both female (sisters), 10 lbs each, declawed, and spayed for immediate transport to Austin, TX from Spring Valley, OH.', '2565 Riverbend Dr. Spring Valley, OH 45370', 'Spring Valley', 'OH', '45370', '2014-06-16 21:31:55', '39.625184', '-84.035820', '401 Little Texas Lane, Austin, TX 78745', 'Austin', 'TX', '78745', '2014-06-16 21:32:43', '30.200200', '-97.772437', NULL, NULL, '2013-12-21 05:08:40'),
(8, 2, 'Jade Nacole back to Vegas', 4, 'items are in boxes. 2 five drawer dressers, 1 queen size bed + 1 TWIN SIZE MATTRESS. 1 big tv. 1 flatscreen tv. 1 small kiddie tv. toybox. 1 bike + 1 kiddie motorbike. 1 trunk (chest). 1 statue of a knight ', '801 anchorage street, wilmington DE 19805', 'wilmington', 'DE', '19805', '2014-06-16 21:31:55', '39.736708', '-75.566897', '27940 solamint rd ', 'canyon country', 'CA', '91387', '2014-06-16 21:32:43', '34.419161', '-118.451145', NULL, NULL, '2013-12-21 05:09:57'),
(9, 2, 'Furniture to David in Ark', 4, '3 wooden kitchen chairs, one dresser, small desk, bookshelf with cabinet, one bin and similar sized box.  See pics.  stuff on shelves etc. will not ship. Transporter to load.', '9828 Balmoral Circle, Charlotte, NC 28210', 'Charlotte', 'NC', '28210', '2014-06-16 21:31:55', '35.102838', '-80.871213', '900 Madden Rd., C1, Jacksonville, AR 72076', 'Jacksonville', 'AR', '72076', '2014-06-16 21:32:43', '34.890161', '-92.099774', NULL, NULL, '2013-12-21 05:11:50'),
(10, 2, 'Bronx "The Bomber"', 4, 'Special care, stops for feeding and potty.', '114 Oxford Court', 'Benton', 'KY', '42025', '2014-06-16 21:31:55', '36.845826', '-88.355089', '4722 Meadow St #1509', 'Dallas', 'TX', '75215', '2014-06-16 21:32:43', '32.757331', '-96.747259', NULL, NULL, '2013-12-21 05:13:21'),
(11, 2, '1972 nova', 4, 'This is a show car so I will pay for protected shipping.', '3138 Sunshine Dr', 'North Fort Myers', 'FL', '33903', '2014-06-16 21:31:55', '26.705582', '-81.902612', '1080 Washington St', 'Freeland', 'MI', '48623', '2014-06-16 21:32:43', '43.524440', '-84.110537', NULL, NULL, '2013-12-21 05:14:46'),
(12, 2, 'Sleeper couch to upstate NY', 4, 'Sleeper couch weigh approx 100lbs. Measurements:\r\nLength: 80 in\r\nHeight: 35 in\r\nWidth: 34 in\r\n\r\nBack is removable. May need help to unload in NY depending on delivery date/time. ', '383 Westridge Circle', 'Phoenixville', 'PA', '19460', '2014-06-16 21:31:55', '40.136484', '-75.535900', '3574 State Route 5', 'Frankfort', 'NY', '13340', '2014-06-16 21:32:43', '43.055725', '-75.077702', NULL, NULL, '2013-12-21 05:15:59'),
(13, 2, 'Bella Anderson', 6, 'She will be in a kennel size 36Lx26Wx26H.  5 month old puppy and we need her moved to our new home.  Do you offer military discount?', '622 Westwind Dr', 'Rapid City', 'SD', '57702', '2014-06-16 21:31:55', '44.080174', '-103.295312', '6023 Hagaru Dr', 'Tarawa Terrace', 'NC', '28543', '2014-06-16 21:32:43', '34.737414', '-77.381472', NULL, NULL, '2013-12-21 05:17:54'),
(14, 2, 'axle to San Antonio', 4, 'This is an axle. It weighs about 175 lbs. The dimensions are about 81L x 25W x 18H \r\n', '95 W 1100 N ', 'North Salt Lake', 'UT', '84054', '2014-06-16 21:31:55', '40.860712', '-111.914572', '2730 Trinity Ridge', 'San Antonio', 'TX', '78261', '2014-06-16 21:32:43', '29.701178', '-98.443595', NULL, NULL, '2013-12-21 05:19:27'),
(15, 2, '2 rattan chairs', 4, 'light weight \r\n37" high x 35" deep x 26" wide\r\n', '3632 Lake Avenue', 'Wilmette', 'IL', '60091', '2014-06-16 21:31:55', '42.083195', '-87.766269', '3014 Benvenue Avenue', 'Berkeley', 'CA', '94705', '2014-06-16 21:32:43', '37.855217', '-122.254216', NULL, NULL, '2013-12-21 05:20:31'),
(16, 2, 'Loader to Summersville', 4, '12ton track loader. Approx 250 miles to destination. Someone will load and unload equipment at each location. 10 ft tall to top cab. Trailer will need to be drop deck or tilt. It will be loaded from ground', '10001 runyan hills lane', 'Chattanooga', 'TN', '37363', '2014-06-16 21:31:55', '35.219434', '-85.038974', '274 Montgomery mill rd', 'Summersville', 'KY', '42782', '2014-06-16 21:32:43', '37.354994', '-85.612647', NULL, NULL, '2013-12-21 05:22:02'),
(17, 2, 'Honda CRF 450x', 4, 'Its a 280 pound well used dirtbike including one or two boxes of parts.  It is clean but rough and scratched.', '65 Isleib Road', 'Marlborough', 'CT', '06447', '2014-06-16 21:31:55', '41.666930', '-72.481980', '985 Deborah Street', 'Upland', 'CA', '91784', '2014-06-16 21:32:43', '34.142575', '-117.665374', NULL, NULL, '2013-12-21 05:23:08'),
(18, 2, 'Ride to Michigan with Pet', 4, 'I am a 49 yr old female looking to ride with someone to Detroit with my son''s dog (golden retriever).  I would need to leave on a Wed/thursday and would like to drive straight through.  I an willing to help drive and pay for all the gas/food to get there.  However I will be flying back home to Florida. My desired date would be to leave on Feb 14 early morning and arrive 24-26 hours later.', '4917 Purdue Drive', 'Boynton Beach', 'FL', '33436', '2014-06-16 21:31:55', '26.548084', '-80.120579', 'Unknown', 'St. Clair Shores', 'MI', '48082', '2014-06-16 21:32:43', '42.524825', '-82.885091', NULL, NULL, '2013-12-21 05:24:55'),
(19, 2, 'Pair of Upholstered Side Chairs', 4, 'Two matching upholstered side chairs.', 'Metrolina Expo', 'Charlotte', 'NC', '28202', '2014-06-16 21:31:55', '35.232678', '-80.846082', '157 1/2 Wentworth St. Apt A.', 'Charleston', 'SC', '29401', '2014-06-16 21:32:43', '32.781321', '-79.935880', NULL, NULL, '2013-12-21 05:26:06'),
(20, 2, 'Brass Bar Cart', 4, 'brass bar cart with two glass shelves needs to be picked up.', '5114 Avignon Court', 'Orlando', 'FL', '32810', '2014-06-16 21:31:55', '28.617017', '-81.422404', '157 1/2 Wentworth St. Apt A.', 'Charleston', 'SC', '29401', '2014-06-16 21:32:43', '32.781321', '-79.935880', NULL, NULL, '2013-12-21 05:27:23'),
(21, 2, 'Test 1', 4, 'aa', '10960 West Hazel Avenue, ', 'Wasilla', 'Al', '99654', '1970-01-01 07:00:00', NULL, NULL, '10960 West Hazel Avenue, ', '10960 West Hazel Avenue, ', 'Al', '99654', '1970-01-01 07:00:00', NULL, NULL, '1200', 0, '2014-01-04 08:10:56'),
(22, 3, 'Household, Vehicle, and Cat all moving East', 4, 'tv, ottoman, rocking chair, crib, ironing board, dresser, hamper, microwave, sml trunk, changing table, musical instrument, instep stroller, framed picture, 2 small boxes, 5 medium boxes, 7 large boxes, baby gate, baby hiking pack, pack and play, rubbermaid 4-drawer, storage bin, 3-20 gallon storage totes, infant swing, cordless floor sweeper, swiffer mop. Vehicle is 2003 suzuki XL-7 and one cat with all shots and cargo box.\n\nI can load and unload this myself.', '13008 86th Ave E', 'Puyallup', 'WA', '98373', '1970-01-01 07:00:00', '47.138229', '-122.315265', '2003 Jack Fords Drive', '2003 Jack Fords Drive', 'NC', '28303', '1970-01-01 07:00:00', '35.093837', '-78.962762', '', 0, '2014-01-04 10:41:22'),
(36, 17, 'asjkas', 5, 'asajks', '10960 West Hazel Avenue, ', 'Wasilla', 'Al', '99654', '2014-04-05 00:27:46', '-7.702036', '139.528000', '10960 West Hazel Avenue, ', '10960 West Hazel Avenue, ', 'Al', '99654', '2014-04-05 00:27:46', '-7.702036', '139.528000', '', 0, '2014-01-04 17:27:48');

-- --------------------------------------------------------

--
-- Table structure for table `shipment_picture`
--

CREATE TABLE IF NOT EXISTS `shipment_picture` (
  `picture_id` int(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` int(20) NOT NULL,
  `shipment_picture` varchar(255) NOT NULL,
  `date_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`picture_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `shipment_picture`
--

INSERT INTO `shipment_picture` (`picture_id`, `shipment_id`, `shipment_picture`, `date_uploaded`) VALUES
(1, 36, '1388856808-36.jpg', '2014-01-04 17:33:29');

-- --------------------------------------------------------

--
-- Table structure for table `shipment_type`
--

CREATE TABLE IF NOT EXISTS `shipment_type` (
  `type` int(5) NOT NULL AUTO_INCREMENT,
  `shipment_type_name` varchar(255) NOT NULL,
  `icon_name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `shipment_type`
--

INSERT INTO `shipment_type` (`type`, `shipment_type_name`, `icon_name`, `icon`) VALUES
(1, 'Hewan Peliharaan', 'ikon_hewan.png', 'ikon_hewan'),
(2, 'Barang Berat', 'ikon_barangberat.png', 'ikon_barangberat'),
(3, 'Barang Mudah Pecah', 'ikon_mudahpecah.png', 'ikon_mudahpecah'),
(4, 'Kendaraan', 'ikon_kendaraan.png', 'ikon_kendaraan'),
(5, 'Furniture', 'ikon_apartment.png', 'ikon_apartment'),
(6, 'Peralatan Rumah Tangga', 'ikon_alatrumahtangga.png', 'ikon_alatrumahtangga'),
(7, 'Lainnya', 'ikon_other.png', 'ikon_other');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

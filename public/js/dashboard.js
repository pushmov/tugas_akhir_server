function getTabContent (target) {
	var tabUrl, url;

	tabUrl = {
		'#myShipment': 'dashboard/my_shipments/',
		'#myBids': 'dashboard/my_bids/',
		'#notifications': 'dashboard/notifications/',
		'#mycontract': 'dashboard/curr_contracts/',
		'#finished': 'dashboard/finished_contracts/',
		'#myfeedback' : 'dashboard/my_feedback'
	};

	url = tabUrl[target];

	if (url) {
		$.ajax(url).success(function (data) {
			$(target).empty().append(data);
		})
	};
}

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr('href');
  getTabContent(target);
})

$(function () {
	// Load initial tab
	var initialTarget = $(".tab-pane.active").attr('id');
	getTabContent("#"+initialTarget);
})
/*<![CDATA[*/
      /* Disable Omniture optional features */
      s_265.trackDownloadLinks=false
      s_265.trackExternalLinks=false
      s_265.trackInlineStats=false
      s_265.linkLeaveQueryString=false
      s_265.trackFormList=false
      s_265.trackPageName=false
      s_265.useCommerce=false

      s_265.pfxID = "map";
      s_265.server = "";
      s_265.channel = "mq.mq";
      s_265.pageType = "";
      s_265.linkInternalFilters = "javascript:,mapquest.com,mqcdn.com";
      s_265.prop1 = s_265.pfxID + " : " + "MQPlatformAPI";
      s_265.prop2 = s_265.pfxID + " : " + "platformapi";
      s_265.prop20 = "none";
      s_265.prop21 = null;
      s_265.pageName = s_265.pfxID + " : " +getPageName();
      s_265.prop12 = window.location.protocol + "//" +getHostName()+"/"+getURLFileName();
      s_265.mmxgo = true;
      //s_265.mmxtitle="MQDevnet";
      
      if((!s_265.prop16) || (s_265.prop16 == "")){s_265.prop16 = s_265.getQueryParam('cid');}
      var s = s_gi(s_account);
      if (s) {
         s.linkTrackVars = "None";
         s.linkTrackEvents= "None";
         s.t();
      }
   /*]]>*/
function displayNarrative(data){
		if(data.route){
			var legs = data.route.legs, html = '', i = 0, j = 0, trek, maneuver;
			html += '<table><tbody>'
			for (; i < legs.length; i++) {
					for (j = 0; j < legs[i].maneuvers.length; j++) {
							maneuver = legs[i].maneuvers[j];
							html += '<tr>';
							html += '<td>&nbsp;';
							if (maneuver.iconUrl) {
									html += '<img src="' + maneuver.iconUrl + '">  '; 
							} 
							for (k = 0; k < maneuver.signs.length; k++) {
									var sign = maneuver.signs[k];
									if (sign && sign.url) {
											html += '<img src="' + sign.url + '">  '; 
									}
							}
							html += '</td><td>' + maneuver.narrative + '</td>'
							html += '</tr>';
					}
			}
			html += '</tbody></table>';
			document.getElementById('narrative').innerHTML = html;
		}
	}	
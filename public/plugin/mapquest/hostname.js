function getHostName()  {
	     var url = window.location.href;
	     var noprotocol = url.split('//')[1];
	     var host = noprotocol .split('/')[0]
	     return host;
}
function getURLFileName() {
   var subDir = window.location.href.replace((window.location.protocol + "//" + getHostName()+"/"),"");
   subDir=(subDir.indexOf("?")>=0)?subDir.substring(0, subDir.indexOf("?")):subDir;
   return subDir.toLowerCase();
}
function getPageName()  {
   var pageName=getURLFileName().replace(/\//g,".");
   return "devnet."+((pageName)?pageName:"home");
}
var s_accountCL = 'aolwpmq';
var s_account = s_accountCL + ",aolsvc";